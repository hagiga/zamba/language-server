CREATE TABLE IF NOT EXISTS zamba.files
(
 rowid       integer             PRIMARY KEY AUTOINCREMENT,
 uri         text                NOT NULL UNIQUE,
 namespace   text,                                      -- maybe there is no namespace
 time_mod    integer             NOT NULL
 -- one can not allow empty namespaces,
 -- but needs to enable a null-value beforehand.
 -- so one issue zamba has is not supporting multiple namespaces within a file.
 --, CONSTRAINT oto_uri_ns UNIQUE (uri, namespace)         -- there is only one namespace for each uri.
);

CREATE TABLE IF NOT EXISTS zamba.member
(
 ref        integer             NOT NULL REFERENCES files(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 name       text                NOT NULL,
 type       text,
 ins_text   text,                                      -- needs to be handled in code
 start      integer             NOT NULL,              -- in bytes
 end        integer             NOT NULL,
 start_body integer             NOT NULL,
 end_body   integer             NOT NULL,
 kind       integer             NOT NULL,              -- @CompletionItemKind
 -- constraints --
 PRIMARY KEY (ref, name, start, end)
);

CREATE TABLE IF NOT EXISTS zamba.invocation
(
 ref        integer             NOT NULL REFERENCES files(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 name       text                NOT NULL,
 start      integer             NOT NULL,              -- in bytes
 end        integer             NOT NULL,
 -- constraints --
 PRIMARY KEY (ref, name, start, end)
);

CREATE TABLE IF NOT EXISTS zamba.implementation
(
 ref        integer             NOT NULL REFERENCES files(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 name       text                NOT NULL,
 start      integer             NOT NULL,              -- in bytes
 end        integer             NOT NULL,
 -- constraints --
 PRIMARY KEY (ref, name, start, end)
);

-- these tables live within the inmemory database, since parsing these can be done on demand for each session
CREATE TABLE IF NOT EXISTS locals
(
 ref        integer             NOT NULL REFERENCES files(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 name       text                NOT NULL,
 type       text,
 ins_text   text,                                      -- needs to be handled in code
 start      integer             NOT NULL,              -- in bytes
 end        integer             NOT NULL,
 start_body integer             NOT NULL,
 end_body   integer             NOT NULL,
 kind       integer             NOT NULL,              -- @CompletionItemKind
 -- constraints --
 PRIMARY KEY (ref, name, start, end)
);

CREATE TABLE IF NOT EXISTS languages
(
 rowid      integer             PRIMARY KEY AUTOINCREMENT,
 name       text                NOT NULL UNIQUE
);

-- for each session the accessible documents are referenced
CREATE TABLE IF NOT EXISTS documents
(
 ref        integer             NOT NULL REFERENCES files(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 lang       integer             NOT NULL REFERENCES languages(rowid) ON UPDATE CASCADE ON DELETE CASCADE,
 PRIMARY KEY (ref, lang)
);
