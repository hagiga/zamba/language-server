DELETE FROM zamba.member
WHERE ref = (SELECT rowid
             FROM zamba.files
             WHERE zamba.files.uri = ?);
DELETE FROM zamba.invocation
WHERE ref = (SELECT rowid
             FROM zamba.files
             WHERE zamba.files.uri = ?);
DELETE FROM zamba.implementation
WHERE ref = (SELECT rowid
             FROM zamba.files
             WHERE zamba.files.uri = ?);
DELETE FROM locals
WHERE ref = (SELECT rowid
             FROM zamba.files
             WHERE zamba.files.uri = ?);
