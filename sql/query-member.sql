SELECT f.uri, m.name, m.type, m.ins_text, m.start, m.end, m.start_body, m.end_body, NULL, m.kind
FROM zamba.member m
INNER JOIN zamba.files f ON m.ref = f.rowid
WHERE
  EXISTS (SELECT 1
          FROM documents d
            INNER JOIN languages l ON d.lang = l.rowid
          WHERE d.ref = f.rowid AND l.name = ? )
  AND m.ref IN (SELECT rowid FROM zamba.files WHERE zamba.files.uri = ?)
  AND m.name like ?
  AND m.start >= ?
  AND m.end <= ?;
