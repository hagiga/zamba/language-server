SELECT uri, name, ins_text, kind, local, type
FROM (
      SELECT f.uri, m.name, m.ins_text, m.kind, 1 as local, m.type
      FROM locals m
        INNER JOIN zamba.files f ON m.ref = f.rowid
      WHERE
        EXISTS (SELECT 1
                FROM documents d
                  INNER JOIN languages l ON d.lang = l.rowid
                WHERE d.ref = f.rowid AND l.name = ?)
        AND m.ref IN (SELECT rowid FROM zamba.files WHERE zamba.files.uri = ?)
        AND m.name like ?
        AND m.start >= ?
        AND m.end <= ?
)
UNION
SELECT uri, name, ins_text, kind, local, type
FROM (
      SELECT f.uri, m.name, m.ins_text, m.kind, NULL as local, m.type
      FROM zamba.member m
        INNER JOIN zamba.files f ON m.ref = f.rowid
      WHERE
        EXISTS (SELECT 1
                FROM documents d
                  INNER JOIN languages l ON d.lang = l.rowid
                WHERE d.ref = f.rowid AND l.name = ?)
        AND m.name like ?
      LIMIT 100
)
ORDER BY
  local ASC,
  ins_text DESC;
