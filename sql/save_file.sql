INSERT INTO zamba.files (uri, namespace, time_mod) VALUES (?, ?, ?)
ON CONFLICT(uri)
DO UPDATE SET
  uri = excluded.uri,
  namespace = excluded.namespace,
  time_mod = excluded.time_mod;
