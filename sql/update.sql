-- update ends
UPDATE OR REPLACE zamba.member
SET
  end_body = end_body + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND ? BETWEEN start_body AND end_body;

UPDATE OR REPLACE locals
SET
  end_body = end_body + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND ? BETWEEN start_body AND end_body;

-- shift following
UPDATE OR REPLACE zamba.member
SET
  start = start + ?,
  end = end + ?,
  start_body = start_body + ?,
  end_body = end_body + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start > ?; -- and 'start_body > ?' ? => shall not be possible, given that the body appears after the name.

UPDATE OR REPLACE locals
SET
  start = start + ?,
  end = end + ?,
  start_body = start_body + ?,
  end_body = end_body + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start > ?; -- and 'start_body > ?' ? => shall not be possible, given that the body appears after the name.


UPDATE OR REPLACE zamba.invocation
SET
  start = start + ?,
  end = end + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start > ?;

UPDATE OR REPLACE zamba.implementation
SET
  start = start + ?,
  end = end + ?
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start > ?;
