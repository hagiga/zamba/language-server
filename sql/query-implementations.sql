SELECT f.uri, i.name, i.start, i.end
FROM zamba.implementation i
  INNER JOIN zamba.files f ON i.ref = f.rowid
WHERE
  EXISTS (SELECT 1
          FROM documents d
            INNER JOIN languages l ON d.lang = l.rowid
          WHERE d.ref = f.rowid AND l.name = ?)
  AND i.name = ?;
