-- only delete if one is affected.
-- can the end be omitted?
DELETE FROM locals
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start >= ?
  AND end <= ?;
DELETE FROM zamba.member
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start >= ?
  AND end <= ?;
DELETE FROM zamba.invocation
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start >= ?
  AND end <= ?;
DELETE FROM zamba.implementation
WHERE
      ref = (SELECT rowid FROM zamba.files WHERE uri = ?)
  AND start >= ?
  AND end <= ?;
