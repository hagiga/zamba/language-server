SELECT f.uri, m.name, m.type, m.ins_text, m.start, m.end, m.start_body, m.end_body, NULL, m.kind
FROM zamba.member m
  INNER JOIN zamba.files f ON m.ref = f.rowid
WHERE
  m.name like ?;
