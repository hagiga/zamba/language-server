# zamba - language server

[Website - About zamba](https://zamba.hagiga.de) - contains a short screen recording.

## purpose

Provide basic navigation and auto-completion capabilities for my editor of choice.
A language server which treats system resources with care.

## building

Make sure to `--init` and `--recurse` the submodules when cloning.
TODO: Due to requirements on my workplace, I need to revisit alternatives to submodules.

### Gambit properties

This server uses [gambit-C](https://www.gambitscheme.org) as the implementation language.

> prefix syntax, keyword-syntax allowed, (main)-method included.
> `v4.9.4-41-ga34094ff 20220603221047 x86_64-pc-linux-gnu "./configure '--prefix=/home/tomes/runenv/gambit' '--enable-single-host' '--enable-debug-c-backtrace' '--enable-bignum' '--enable-ratnum' '--enable-poll' '--enable-multiple-threaded-vms'"`

### prerequisites

- sqlite3
- tree-sitter
- tree-sitter-languages (supported languages listed at website or source code)
- git binary added to `$PATH`

### instructions

`make` to build the language server as an executable.
`make deps` to build the dependencies used in the interpreter.
They reside in the `bin`-directory.
Then you can run `gsi src/zamba.scm` via the interpreter.

## configuration

\@see [use-zamba](docs/use-zamba.md)
Because this language server could be a server for any programming language, it maybe neglect the benefit of the language server protocol.

### workspace configuration

One can create a file called `.zamba` at the root of the project.
Its content lists a path to index in each line.

### command line arguments

Currently following start-up arguments are provided:

 - '--tcp port-number' which wraps the stdio ports with an tcp-port opened at the provided port.
 - '--repl (#!optional port-number)' which starts an repl to the server instance in gambit scheme. This enables one to inspect the global environment of the server, as well as changing it. The repl is hosted on an TCP/IP-port. The port is propagated via your editor ui. If your editor does not support displaying messages of a language server, feel free to open a ticket. Also, this is the bespoken "runtime environment".
 - '--omit-abstract-syntax-tree' when hovering a variable, suppress printing out the abstract syntax tree.
 - '--disable-text-sync/--tag-mode' a low computation mode, where zamba tells your client to not synchronize the document content. This will only work, if the client supports the didSave-notification. Otherwise zamba will shut itself down while initializing.
 - '--debug' tells zamba do write every log-message to a dedicated file in the /tmp-directory. Obviously not working on windows.

## versioning

Now uses git tags.
Features should now be included in dedicated merge commits.
Most likely they will get squashed but I am still discovering my personal favorite workflow.

Semantic versioning should be applied but I am not too familiar with the workflow yet.
Therefore differences to the specification of [semver](https://www.semver.org) appear.

## test coverage

Currently the core of zamba is tested.
It misses tests for the _request_ methods, namely:
- textDocument/declaration
- textDocument/definition
- textDocument/implementation
- textDocument/hover
- textDocument/references
- textDocument/completion

## licensing

MPL v. 2.0

This conforms with
 - MIT (tree-sitter)
 - APACHE 2.0/LGPL gambit
 - Any licsense I authored (tbdeclared).

### development environment

```
$ uname -a
Linux ALURakete 4.19.0-11-amd64 #1 SMP Debian 4.19.146-1 (2020-09-17) x86_64 GNU/Linux
$ lscpu
Architecture:                    x86_64
Byte Order:                      Little Endian
Address sizes:                   36 bits physical, 48 bits virtual
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           23
Model name:                      Intel(R) Core(TM)2 Duo CPU     P8600  @ 2.40GHz
Stepping:                        10
CPU MHz:                         1470.589
CPU max MHz:                     2394.0000
CPU min MHz:                     798.0000
BogoMIPS:                        4778.54
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        3 MiB
$ cd lib/tree-sitter && git rev-parse HEAD
46a2a5933f30930d668d4cf785705be4c7dfa713
$ cd lib/tree-sitter-java && git rev-parse HEAD
ac14b4b1884102839455d32543ab6d53ae089ab7
$ cd lib/tree-sitter-python && git rev-parse HEAD
dafcef7943229ec9d530b36ed67d758e659f4c6c
```
