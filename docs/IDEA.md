# IDEA

Following diagram shows how tree-sitter should be included by the language server:
![interaction](interaction).

This was almost exactly implemented.

## reasoning
1. Using tree sitter, one shall archive an agnostic language server
2. Parsing-capabilities are independent. Only the semantic behaviour needs to get tested.
3. Processing a syntax tree of a language is most convenient with Sexp, so adapting the goal of the language server seems reasonable.

### building a parser

In order to use a generated parser, one has to build the shared library in advance.

These are my initial steps to build and link a parser (by example of "tree-sitter-java").
```
# run from the root of the git project.
# optimized for size (-Os)
gcc -o libtree-sitter-java.so -I./src src/parser.c --shared -Os -lstdc++
gsc -ld-options "-L. -ltree-sitter-java" -o ts-java-binding.o1 ts-java-binding.scm
LD_LIBRARY_PATH=§PWD:$LD_LIBRARY_PATH gsi
(load "ts-java-binding.o1")
```
## diagnostics

Due to the fact that querying for `Missing` and `Error` nodes is not possibile with tree-sitter, there is no diagnostic support yet.

Their would be missing compiler warnings anyways, but this shouldn't be in the scope of a editor tool anyway.
