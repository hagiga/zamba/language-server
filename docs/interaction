@startuml
left to right direction

actor client

package "language server" {
  package "textDocument" {
    (didOpen)
    (didChange)
    (didClose)
  }

  package "tree-sitter" {
    (new parser) as parser_new
    (new language parser) as parser_init

    (new source tree) as source_tree_new

    (update source tree) as source_tree_edit
  }
  (resolve language\n standard library) as std_lib

  (resolve language parser) as get_lang_parser

  (free resources) as freeing
  (free source tree) as source_tree_free
  (free language parser) as lang_parser_free

  (create source tree) as source_tree_new
  (save source tree) as source_tree_save

  (walk dependencies /\nimports) as dependencies

}

client -- (didOpen)
client -- (didChange)
client -- (didClose)

(didOpen) -- source_tree_new
(didOpen) <-right- std_lib : <<includes>>

source_tree_save <- dependencies : <<includes>>
dependencies <- source_tree_new : <<includes>>

source_tree_new <-- source_tree_save : <<extends>>
source_tree_new <-- get_lang_parser : <<extends>>
get_lang_parser <- parser_new : <<includes>>
parser_new <- parser_init : <<includes>>



(didChange) -- source_tree_edit : translate TextDocumentEdit\nto TSInputEdit

source_tree_edit <-- source_tree_save : <<extends>>




(didClose) -down- freeing

freeing <-down- source_tree_free : <<includes>>
freeing <-- lang_parser_free : <<includes>>
lang_parser_free <- get_lang_parser : <<includes>>





note "use caching to exclude\nstandard library\n(currently done on initialize)" as n1
dependencies .. n1
std_lib .. n1
note left of parser_init : Load bindings on demand
note right of source_tree_free : Last tree frees language standard lib

@enduml
