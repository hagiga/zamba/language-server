# project configuration

Different projects in different languages have different requirements on the _location_ for its dependencies.\\
Additional hurdle: Dependencies can be in the form of a binary.

## Disclaimer

The exact implementation is not stable since I lack _experience_ and _feedback_.
The following outlines my intention.

# introduction to the protocol

Microsofts language server protocol passes three `initialization`-options to the language server:

1. `rootPath` - Which is the path the language server is started from.
2. `rootUri` - Which should have a higher precedence; Similar to `rootPath` but prepends the scheme (`file://`) to the `rootPath`.
3. `workspaceFolders` - Which is user-defined within the parameter of the `initialize`-request used to prepare the server.

Visual Studio Code uses a `*.json`-file to declare dependencies.
It is a good thing that this is not within the protocol (refer to [extenson](project-configuration.md) below).

# approach

Zamba does not aim to support `.vscode-workspaces.json` yet.
Once opting into this it implies a `json`-dependency; Elaboration follows.

Zamba can use the defined options to grasp the overall layout of the project.
Unfortunately this will also include bindings which may not in the scope of the project.

> If there is a directory `{tools|helper}/` - for example - zamba will parse it as well.

It is sufficient to declare the `rootPath` or `rootUri` to rely on this behaviour.

Before prompting the option for more granularity listed below, the following should be interesting in advance:

## extension

Similar to Microsofts `.vscode-workspaces.json` zamba uses  a dedicated project file.
Zamba looks for a file within the `rootPath`/`rootUri` directory named `.zamba`.
This file contains paths to discover (recursively) for bindings.
It assumes that there is _one_ path for each _line_.
After every save of a file, zamba will watch for changes on the content (by comparing the last time of modification!).

> Note that zamba caches parsed files so the effort is "neglectable". Feel free to write to this file when issuing any build.

This is the "dynamic" approach of dependency (sources!) resolution.
It enables one to extend the build system to append bindings to the file in a simple matter when building a project.

> I need this behaviour in the future to write out the path of a source-dependency within java build systems.

It should not be necessary to include a json-writing-dependency within the build system even though it appears to be small (promised elaboration...).

## workspaceFolders

If you want more granularity when iterating dependencies you need to build upon your language server client (editor).
You can shadow the `rootPath`/`rootUri` by declaring the `workspaceFolders` in a more granular matter.
Unfortunately the implementation of _default initialization options_ differ between free and open source software editors.
Therefore one has to be proficient with the language server client option of its editor - for example some assume a json-key named `workspace_folders`, Some `workspaceFolders` and some expect a parent key beforehand -.
Anyhow; `IFF` you propagate `workspaceFolders`, `rootPath`/`rootUri` are ignored.
Zambas [extension](project-configuration.md) to discover dependencies described above are still guaranteed.

## limitations

UTF-16 files are transformed inconsistently between lsc-clients. This is nothing what zamba can account for. UTF-8 files are therefore recommended.

