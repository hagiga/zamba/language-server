# introduction

When using an _integrated_ development environment one puts assumptions about it.
The most relevant features in decreasing order are:
- declarations and definitions should be resolvable
- refactoring should be reliable
- common sequential tasks should be grouped and executable in one step.

With certain shortcomings, these features can be supported language agnostic.
Luckily the ordering of these features matches with my opinion.

If one can resolve declarations and definitions reliably, refactoring can reuse this capability.
Common task which are executed in sequential order are to be defined, but the current status quo is:
- accepting completions should extend imports
- changes on the arguments provided to a _reference_ should prompt a action to change the declaration.
  Then all other references needs to be invalidated (no verification of the call signature yet);
  Requires persisting diagnostics.

# resolving dependencies

I am claiming that programming languages can be put in two categories.
Either they use explicit declarations which can appear within any scope.
Or they rely on a runtime environment where all bindings are accessible from.

| Language | Category |
|----------|----------|
| C        | explicit |
| D        | explicit |
| java     | explicit |
| scheme   | runtime  |
| perl     | runtime  |
| python   | runtime  |

This assumption aids the complexity in discovering dependencies.
The following two chapters elaborate on the strategy.

## recognize explicit declared dependencies

> Things to consider:
> - standard library
> - shadowing
> - lexical scoped imports

For a given file, one _knows_ which bindings are available.
In addition, the depth (referred to within the AST used by zamba) can be determined as well.
If this information is stored, one can enlarge the query.

> Before any action, available dependencies needs to be respected.
> If there is an error, refactoring would not be reliable.
> There needs to be a prompt for the user to let him know which bindings _could not have been updated_?

1.  resolve all available bindings
2.a There are no special cases.
2.b There are shadowed bindings when including dependencies.
2.c Dependencies were renamed.
2.d There are no results.
3.  Consider the standard library, if applicable.

A given file points to its explicitly declared dependencies.
There is no explicit necessity to use a graph.
The standard library _could_ be considered as well.

> renamed dependencies could be treated similar to classes.

## recognize dependencies included in the runtime

Languages which have implicit dependencies need further processing.
One has to determine which bindings are available, so I think a graph could help here.

But how to go about the ordering of includes?
One needs the parent class.

> Maybe it could be deduced by the main method, or a shebang; But there could be multiple of those within the working directory.

So one needs to process all available files building graphs.
When a file changes multiple graphs need to be invalidated.
Each file can have multiple roots.

> There is no way to deduce the root the user wants to refer to.

This is similar to [recognize explicit declared dependencies](.) where the user needs to take care of changes.
A choice needs to be prompted (refer to a graph, or reference all).
This goes against the statement to be _don't be too smart and keep it simple_.

### thought experiment (to be deprecated)

Determining the scope on each execution of a query appears to be cumber stone.
The ordering can change within a coding session, so the graph needs to be **invalided** at certain times as well
<!-- TODO: DETERMINE WHEN -->

# elected strategy

## data structures

<!-- TODO: INVALIDATION; Can scatter across multiple graphs.. -->

graph of <file>
<file>: <path> + (<include> ...)
<include>: <file> + <source-location>
<source-location>: <row> + <col>

When the explicit strategy is elected, one can simply look "downwards" the graph.
Otherwise one refers to all files in the graph.

### resolve all bindings

Get all files which are included.

1a. Query all files which are referenced from the file in question, or:
1b. Query recursively with CTE, to retrieve _all_ files which include the file in question.
  2. (optional!) repeat the query but only aim at the root-nodes.

> Thanks to ##sqlite (libera.chat): Celelibi, Chunky_K1, Habbie.

FIXME: I am missing a crucial implication I forgot about.

> this could require resolving renames: We are not interested in this, currently.
> renamed dependencies could be treated like a class.
Then query all bindings against these files.

If there are concurrent include paths, prompt the user (__postponed__).

When bindings are shadowed we are falling back to the most recent reference.
It could be a user choice; The use case is not obvious/discovered/thought about yet.

If there are no results, fallback (existing behaviour).

#### invalidation

for each opened document cache its graph.
If a new include is found, invalidate the graph.
