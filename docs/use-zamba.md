# usage

After setting up the server by extending the LD_LIBRARY_PATH (refer to [building a parser](IDEA.md)) for tree-sitter and the language-parsers, you are good to go.

## adding language

zamba does not discover your LD_LIBRARY_PATH, so you need to instruct zamba to load a language.
If the declared language is not properly published, zamba will die.
Otherwise, zamba will load the shared library, which shall be called `libtree-sitter-*lang*`.so/dll/dylib.

### editor settings

This is client specific, but in order to simplify the configuration, here are some hints:

### vim-lsc

```vimscript
let g:lsc_auto_map = {'defaults': v:true } "'GoToDefinition': 'gd'}
let s:zamba = {
      \    'name': 'ZAMBA-LS',
      \    'command': 'zamba.sh', " where zamba.sh contains: tee ... | gsi /path/to/zamba/src/zamba.scm | tee ...
      \    'workspace_config': {
      \        'workspace_folders': {
      \            'name' : 'language_server',
      \            'uri' : 'file:///home/tomes/workspace/zamba-ls/',
      \        },
      \    },
      \  }
let g:lsc_server_commands = {
      \  'scheme' : s:zamba,
      \  'java' : s:zamba,
      \  'c' : s:zamba,
      \}

let g:lsc_enable_autocomplete = v:true
let g:lsc_trace_level = 'verbose'
```

### nvim-lsp-config ~v.0.5
```lua
nvim_lsp.zamba.setup {
  cmd = {
    "/path/to/gambit/bin/gsi",
    "/path/to/zamba-ls/src/zamba.scm"
  };
  filetypes = {
    "java" -- the associated filetypes
  };
  workspace_folders = {
    .. -- refer to config
  };
}
```

### emacs

```emacs-lisp
(with-eval-after-load 'lsp-mode

  (add-to-list 'lsp-language-id-configuration '(java-mode . "java"))

;; for tcp-ip (documentation purpose)
;; for stdin (recommended)
;; remove --tcp argument from executable-custom
;; remove change (lsp-tcp-connection [..]) => (lsp-stdio-connection (lambda () lsp-zamba-executable))

  (defcustom lsp-zamba-executable '("/path/to/gambit/current/bin/gsi"
                                    "/path/to/zamba-ls/src/zamba.scm"
                                    "--tcp")
    "Command to start the Digestif language server."
    :group 'lsp-java ; transitive to java-filetype
    :risky t
    :type 'file)

  (lsp-register-client
   (make-lsp-client :new-connection (lsp-tcp-connection (lambda (port) (append lsp-zamba-executable (list (number-to-string port)))))
                    :activation-fn (lsp-activate-on "java")
                    :priority -1
                    :server-id 'zamba))

```

### sublime

```
{ // within your LSP.sublime-settings, please append
	"clients": {
	  "zamba-ls": {
	    "enabled": true,
	    // you can either compile to a executable or call the interpreter like so:
	    "command": ["/path/to/gambit/bin/gsi", "/path/to/zamba-repo/src/zamba.scm"],
      // enable all files you want zamba associated with.
	    "selector": "source.java, source.python"
	  }
	}
}
```

## restrictions

It appears that LSP-clients are inconsistent with the `workspace/configuration`-implementation.
