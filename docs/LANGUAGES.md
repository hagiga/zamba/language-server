# languages

A language implementation does not need to declare too much, in order to leverage the tree-sitter grammar.
It needs to ship two tree-sitter queries for resolving local and member bindings.
It shall follow the requires project structure.
It shall at least include the template provided hereby.

> You have to replace occurences of **lang-name** with the lsp-filetype-name.

## project structure

```
lib/lang/**lang-name**/
├── component.scm
├── Makefile
├── queries
│   ├── locals.ss
│   └── members.ss
└── ts-**lang-name**-binding.scm
```

> Within zamba-root.

## template

```scheme
;; load the compiled part yourself
(load (path-normalize (string-append (path-directory (zamba-directory)) "bin/ts-**lang-name**-binding.o1")))

;; declare a namespace
(##namespace ("ts--**lang-name**#"
parent-scopes
imported-namespaces
filter-completions
))

;; propagate the associated filetypes.
(set! languages-by-suffix
  (append languages-by-suffix
          '((".file-suffix" . "**lang-name**"))))

;; add the own language-object of ts
(set! known-languages
  (cons (cons "python" (ts--python#language-new))
        known-languages))

;; declare the parent-scopes where zamba can look for bindings in.
;; valid associations are `local`, `member`, `all`.
(define parent-scopes
  '(("block" local member)
    ("module" all)))

;; for a given file, you should return a list of namespaces which are imported.
(define (imported-namespaces uri doc)
  '("default.namespace.of.std.lib.e.g."))

(define (filter-completions item-vector isIncomplete-box cursor uri doc word current-node)
  ;; one could add, filter and sort items.
  ;; by providing edit-commands, one can archive import functionalities...
  ;; the cursor is already placed at the parent-scope, but the current node is provided as well.
  ;; the document is loaded and the content can be retrieved.

  ;; Note that the box can be used to tell the client that no further completions can be provided.
  ;; it comes with #t as a default.

  ;; you should return the filtered or extended vector with completions in used json format.
    item-vector)
```

