
(test-begin! "Testing json library")

(load "lib/r5rs-json-library/json.scm")

(define (read-file file)
  (call-with-input-file
    file
    (lambda (p)
      (fold (lambda (s acc) (string-append acc s))
            ""
      (read-all p (lambda (p) (read-line p #\newline #t)))))))

(define decode
 (lambda (str fun)
            (call-with-input-string str (lambda (p) (fun p)))))

(test "decoding simple json values"
      assert (equal? '()                             (decode "{}" json-decode))
      assert (equal? '(("number" . 4711))            (decode (read-file "test/json/number.json") json-decode))
      assert (equal? '(("key" . "string"))           (decode (read-file "test/json/string.json") json-decode))
      assert (equal? '(("true" . #t) ("false" . #f)) (decode (read-file "test/json/boolean.json") json-decode))
      assert (equal? '(("null"))                     (decode (read-file "test/json/null.json") json-decode))
      assert (equal? '(("key" ("object" . "value"))) (decode (read-file "test/json/object.json") json-decode))
      assert (equal? '(("list" . #(1 2 3)))          (decode (read-file "test/json/array.json") json-decode))
      assert (equal? '(("objects" .
                        #((("first" . #((("key" . 0) ("value" . "xy")) 1 2 3))))))
                     (decode (read-file "test/json/nested-array.json") json-decode))
      assert (equal?
               '(("method" . "textDocument/didChange")
                 ("jsonrpc" . "2.0")
                 ("params"
                  ("contentChanges"
                   .
                   #((("range"
                       ("end" ("character" . 0) ("line" . 2))
                       ("start" ("character" . 0) ("line" . 2)))
                      ("text"
                       .
                       "  public static final String TAG = \"ALMOST\";")
                      ("rangeLength" . 0))))
                  ("textDocument"
                   ("uri" . "file:///imaginary.java")
                   ("version" . 2))))
               (decode (read-file "test/json/didChange-almost-empty-file.json") json-decode)))

(test "decoding json with indentation"
      assert (equal? '(("first" ("nested" . 1)) ("second" . #(2)))
                     (decode (read-file "test/json/with-indentation.json") json-decode)))

(test "decoding advanced numbers"
      assert (equal? '(("number" . -4.7110815e-87)) (decode (read-file "test/json/number_adv.json") json-decode)))

(test "decoding strings with escape sequences"
      assert (equal? '(("str" . "This\"quoted\"andsolidus\\goback\bmakeformfeed\fcrlf\n\rtab\tunicodeᅴ冀reversesolidus/end."))           (decode (read-file "test/json/string_adv.json") json-decode)))

(test "decoding random messages to zamba"
      assert (equal? '(("jsonrpc" . "2.0")
                       ("method" . "initialize")
                       ("id" . 1)
                       ("params"
                        ("rootUri" . "file:///home/user/workspace/directory")
                        ("capabilities"
                         ("workspace" ("configuration" . #t) ("applyEdit" . #t))
                         ("textDocument"
                          ("completion"
                           ("completionItem"
                            ("snippetSupport" . #f)
                            ("tagSupport" ("valueSet" . #(1)))
                            ("deprecatedSupport" . #t)))
                          ("synchronization"
                           ("willSaveWaitUntil" . #f)
                           ("willSave" . #f)
                           ("didSave" . #t))
                          ("codeAction"
                           ("codeActionLiteralSupport"
                            ("codeActionKind"
                             ("valueSet" . #("quickfix" "refactor" "source")))))
                          ("signatureHelp" ("dynamicRegistration" . #f))
                          ("hover" ("contentFormat" . #("plaintext" "markdown")))
                          ("definition" ("dynamicRegistration" . #f))
                          ("declaration" ("dynamicRegistration" . #f))
                          ("implementation" ("dynamicRegistration" . #f))
                          ("references" ("dynamicRegistration" . #f))))
                        ("clientInfo" ("name" . "client"))
                        ("processId" . 4711)
                        ("trace" . "off")))
                     (decode (read-file "test/json/initialize.json") json-decode))
      assert (equal? '(("method" . "textDocument/didOpen")
                       ("jsonrpc" . "2.0")
                       ("params"
                        ("textDocument"
                         ("uri" . "file:///home/user/workspace/file.txt")
                         ("version" . 1)
                         ("languageId" . "text")
                         ("text" . "❤")))) (decode (read-file "test/json/didOpen.json") json-decode)))

(define encode
  (lambda (json fun)
    (call-with-output-string
      (lambda (p) (fun json p)))))

(test "encode jsons of different kinds"
      assert (equal? "42" (encode 42 json-encode))
      assert (equal? "\"string\"" (encode "string" json-encode))
      assert (equal? "null" (encode '() json-encode))
      assert (equal? "{\"key\":null}" (encode '("key") json-encode))
      assert (equal? "{\"key\":42}" (encode '("key" . 42) json-encode))
      assert (equal? "{\"key\":42,\"second\":false}" (encode '(("key" . 42) ("second" . #f)) json-encode))
      assert (equal? "[42]" (encode '#(42) json-encode))
      assert (equal? "[{\"key\":42},{\"ky\":33},\"string\"]" (encode '#(("key" . 42) ("ky" . 33) "string") json-encode))
      assert (equal? "[{\"key\":[1,2,3,4,{\"another\":[\"a\",\"b\",\"c\"]}]},\"string\"]" (encode '#(("key" . #(1 2 3 4 ("another" . #("a" "b" "c")))) "string") json-encode))
      assert (equal? "{\"key\":{\"nested\":4711,\"second\":\"value\"},\"another\":[1,2,3,4,5]}"
                     (encode '(("key"
                                     (("nested" . 4711)
                                      ("second" . "value")))
                                    ("another" . #(1 2 3 4 5))) json-encode)))

(test "encode json-rpc headers"
      assert (equal? "{\"jsonrpc\":\"2.0\",\"id\":1,\"error\":{\"code\":-1,\"message\":\"errmsg\",\"data\":null}}"
                     (encode '(("jsonrpc"  . "2.0")
                                    ("id"       . 1)
                                    ("error"
                                     ("code"    . -1)
                                     ("message" . "errmsg" )
                                     ("data"))) json-encode))
      assert (equal? "{\"jsonrpc\":\"2.0\",\"id\":1,\"result\":{\"example\":\"data\"}}"
                     (encode '(("jsonrpc" . "2.0")
                                    ("id"      . 1)
                                    ("result"  ("example" . "data"))) json-encode)))
