(load "lib/r5rs-json-library/json.scm")

(define (read-file file)
  (call-with-input-file
    file
    (lambda (p)
      (fold (lambda (s acc) (string-append acc s))
            ""
      (read-all p (lambda (p) (read-line p #\newline #t)))))))

(define (header message)
  (string-append "Content-Length:" (number->string (string-length message)) (list->string '(#\return #\newline #\return #\newline)) message))

(define (write-msg json-file p)
  (display (header (read-file json-file)) p)
  (force-output p))

(define (write-str str p)
  (display (header str) p)
  (force-output p))

(define (read-msg p)
  (let* ((raw (read-line p #\newline #f))
         (len (string->number (substring raw 16 (- (string-length raw) 1))))
         (buffer (make-string len)))
    (read-line p)
    (read-substring buffer 0 len p (- len 1))
    buffer))

(define (shutdown p)
  (write-msg "test/json/shutdown.json" p)
  (read-msg p)
  (write-msg "test/json/exit.json" p))

