
(test-begin! "testing hover requests")

(test "hover shall include the AST" assert (equal? #t #t))
(test "hover shall not include the AST" assert (equal? #t #t))
;; This requires the upcoming dependency-graph
(test "hover resolves the correct comments above an item" assert (equal? #t #t))

;; TODO: Upcoming
;(test "hover on a variable prompts useful information" assert (equal? #t #t))
