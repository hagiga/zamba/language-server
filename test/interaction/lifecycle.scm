
(test-begin! "Server Lifecycle interactions")

(load "src/zamba.scm")
(include "../stdio-util.scm")

(define zamba
  (open-process
    (list path: "gsi"
          arguments: '("src/zamba.scm"))))

(write-msg "test/json/initialize-faulty.json" zamba)

(define reply (with-input-from-string
                (read-msg zamba)
                (lambda ()
                  (json-decode (current-input-port)))))

(test "Server shuts down gracefully, if initialization is faulty"
      assert (equal?
               `(("jsonrpc" . "2.0")
                 ("id" . 1)
                 ("error"
                  ("code" . ,server-error-start)
                  ("message"
                   .
                   "client did not publish its capabilities.")
                  ("data")))
               reply)
      assert (equal? server-initializing (/ (process-status zamba) 256)))

(close-port zamba)

(define zamba
  (open-process
    (list path: "gsi"
          arguments: '("src/zamba.scm"))))

(write-msg "test/json/initialize.json" zamba)

(define reply (with-input-from-string
              (read-msg zamba)
              (lambda ()
                (json-decode (current-input-port)))))

(write-msg "test/json/initialized.json" zamba)

(test "server correctly replys to initialize"
      assert (equal? `("serverInfo" ("name" . "ZAMBA-LS") ("version" . ,version))
                     (assoc "serverInfo" (cdr (assoc "result" reply))))

      assert (equal? #t (list? (assoc "result" reply)))
      assert (equal? '("workspace" ("configuration" . #t) ("workspaceFolders" ("supported" . #f) ("changeNotifications" . #f)))
                     (assoc "workspace" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("textDocumentSync"
                       ("save" ("includeText" . #t))
                       ("openClose" . #t)
                       ("change" . 2))
                     (assoc "textDocumentSync" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("hoverProvider" . #t)
                     (assoc "hoverProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("declarationProvider" . #t)
                     (assoc "declarationProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("definitionProvider" . #t)
                     (assoc "definitionProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("implementationProvider" . #t)
                     (assoc "implementationProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("referencesProvider" . #t)
                     (assoc "referencesProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply))))))

      assert (equal? '("completionProvider" ("resolveProvider" . #f))
                     (assoc "completionProvider" (cdr (assoc "capabilities" (cdr (assoc "result" reply)))))))

(test "finish initialization phase - callback"
      assert (equal? #t (process-status zamba 1 #t)))

(write-msg "test/json/no-params.json" zamba)

(define reply (with-input-from-string
              (read-msg zamba)
              (lambda ()
                (json-decode (current-input-port)))))

(test "a message does not carry a parameter"
      assert (equal? `(("jsonrpc" . "2.0")
                       ("id" . 3)
                       ("error"
                        ("code" . ,invalid-params)
                        ("message" . "no params provided to didOpen")
                        ("data")))
                     reply))

(shutdown zamba)

(close-port zamba)

(test "gracefully shut down"
      assert (equal? 0 (process-status zamba 1)))

