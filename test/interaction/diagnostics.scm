
(test-begin! "diagnostics of syntax errors")

;(pp
;  (ts--node-error?
;  (ts--tree-root-node
;    (lsp-document-tree
;      (table-ref documents "file:///Main.java")))))

(load "src/zamba.scm")
(include "../stdio-util.scm")

(define (thread-send thread . args) 'done)

(textDocument/didOpen
  `(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("languageId" . "java")
     ("version" . 1)
     ;; You should open up this file alongside
     ("text" . ,(read-file "test/projects/java/Erroneous.java")))))

(test "verify that no diagnostics are generated if disablesd"
      assert (eq? #f (publish-diagnostics))
      assert (eq? 0 (table-length diagnostics)))

(textDocument/didClose
  '(("textDocument"
     ("uri" . "file:///Erroneous.java"))))

(publish-diagnostics #t)

(textDocument/didOpen
  `(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("languageId" . "java")
     ("version" . 1)
     ;; You should open up this file alongside
     ("text" . ,(read-file "test/projects/java/Erroneous.java")))))

(test "show diagnostics if file contains an error after load immediately"
      assert (eq? #t (publish-diagnostics))
      assert (eq? 1 (table-length diagnostics))
      assert (eq? 4 (length (table-ref diagnostics "file:///Erroneous.java")))
      ;; read comments about ordering.
      ;; One client could demand ordering, though.
      ;; first, add the errors
      ;; then adds the missing identifiers without reversing.
      assert (equal? '((("range"
                      ("start" ("character" . 49) ("line" . 4))
                      ("end" ("character" . 49) ("line" . 4)))
                     ("severity" . 2)
                     ("code" . "Syntax Error")
                     ("message" . "missing \";\""))
                    (("range"
                      ("start" ("character" . 36) ("line" . 6))
                      ("end" ("character" . 36) ("line" . 6)))
                     ("severity" . 2)
                     ("code" . "Syntax Error")
                     ("message" . "missing identifier"))
                    (("range"
                      ("start" ("character" . 4) ("line" . 13))
                      ("end" ("character" . 5) ("line" . 13)))
                     ("severity" . 1)
                     ("code" . "Syntax Error")
                     ("message" . "Abstract Syntax Tree parsing error"))
                    (("range"
                      ("start" ("character" . 11) ("line" . 9))
                      ("end" ("character" . 12) ("line" . 9)))
                     ("severity" . 1)
                     ("code" . "Syntax Error")
                     ("message" . "Abstract Syntax Tree parsing error"))) (table-ref diagnostics "file:///Erroneous.java")))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("version" . 2))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . ";")
                           ("range"
                            ("end"
                             ("character" . 49)
                             ("line" . 4))
                            ("start"
                             ("character" . 49)
                             ("line" . 4))))))))

(test "after correction a missing statement, the amount of diagnostics is reduced accordingly."
      assert (eq? 3 (length (table-ref diagnostics "file:///Erroneous.java")))
      assert (equal? #(
                       "package de.hagiga.zamba;\n"
                       "\n"
                       "public class Erroneous {\n"
                       "\n"
                       "  public static final String TAG = \"No-Semicolon\";\n"
                       "\n"
                       "  public Erroneous(MissingIdentifier) {\n"
                       "    super();\n"
                       "\n"
                       "    String s a = new String();\n"
                       "  }\n"
                       "\n"
                       "  public void set() {\n"
                       "    = this; // unknown\n"
                       "  }\n"
                       "}\n"
                       "\n")
                       (lsp-document-content
                         (table-ref documents "file:///Erroneous.java")))

      ;; we are reversing on each change, because we do not care.
      assert (equal? '((("range"
                         ("start" ("character" . 11) ("line" . 9))
                         ("end" ("character" . 12) ("line" . 9)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error"))
                       (("range"
                         ("start" ("character" . 4) ("line" . 13))
                         ("end" ("character" . 5) ("line" . 13)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error"))
                       (("range"
                         ("start" ("character" . 36) ("line" . 6))
                         ("end" ("character" . 36) ("line" . 6)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing identifier")))
                     (table-ref diagnostics "file:///Erroneous.java")))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("version" . 3))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . "    String\n")
                           ("range"
                            ("end"
                             ("character" . 0)
                             ("line" . 8))
                            ("start"
                             ("character" . 0)
                             ("line" . 8))))))))

(test "prepare a larger change"
      assert (eq? 3 (length (table-ref diagnostics "file:///Erroneous.java")))
      assert (equal? #("package de.hagiga.zamba;\n"
                       "\n"
                       "public class Erroneous {\n"
                       "\n"
                       "  public static final String TAG = \"No-Semicolon\";\n"
                       "\n"
                       "  public Erroneous(MissingIdentifier) {\n"
                       "    super();\n"
                       "    String\n"
                       "\n"
                       "    String s a = new String();\n"
                       "  }\n"
                       "\n"
                       "  public void set() {\n"
                       "    = this; // unknown\n"
                       "  }\n"
                       "}\n"
                       "\n")
                       (lsp-document-content
                         (table-ref documents "file:///Erroneous.java")))

      assert (equal? '((("range"
                         ("start" ("character" . 10) ("line" . 10))
                         ("end" ("character" . 10) ("line" . 10)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing \";\""))
                       (("range"
                         ("start" ("character" . 36) ("line" . 6))
                         ("end" ("character" . 36) ("line" . 6)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing identifier"))
                       (("range"
                         ("start" ("character" . 4) ("line" . 14))
                         ("end" ("character" . 5) ("line" . 14)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error")))
                     (table-ref diagnostics "file:///Erroneous.java")))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("version" . 4))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . " it")
                           ("range"
                            ("end"
                             ("character" . 10)
                             ("line" . 8))
                            ("start"
                             ("character" . 10)
                             ("line" . 8))))))))

(test "apply a second change which introduces a new missing-diagnostic"
      ;assert (eq? 4 (length (table-ref diagnostics "file:///Erroneous.java")))
      assert (equal? #("package de.hagiga.zamba;\n"
                       "\n"
                       "public class Erroneous {\n"
                       "\n"
                       "  public static final String TAG = \"No-Semicolon\";\n"
                       "\n"
                       "  public Erroneous(MissingIdentifier) {\n"
                       "    super();\n"
                       "    String it\n"
                       "\n"
                       "    String s a = new String();\n"
                       "  }\n"
                       "\n"
                       "  public void set() {\n"
                       "    = this; // unknown\n"
                       "  }\n"
                       "}\n"
                       "\n")
                       (lsp-document-content
                         (table-ref documents "file:///Erroneous.java")))

      assert (equal? '((("range"
                         ("start" ("character" . 13) ("line" . 8))
                         ("end" ("character" . 13) ("line" . 8)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing \";\""))
                       (("range"
                         ("start" ("character" . 11) ("line" . 10))
                         ("end" ("character" . 12) ("line" . 10)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error"))
                       (("range"
                         ("start" ("character" . 4) ("line" . 14))
                         ("end" ("character" . 5) ("line" . 14)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error"))
                       (("range"
                         ("start" ("character" . 36) ("line" . 6))
                         ("end" ("character" . 36) ("line" . 6)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing identifier")))
                     (table-ref diagnostics "file:///Erroneous.java")))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///Erroneous.java")
     ("version" . 5))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . ";")
                           ("range"
                            ("end"
                             ("character" . 13)
                             ("line" . 8))
                            ("start"
                             ("character" . 13)
                             ("line" . 8))))))))


(test "Fix the missing-diagnostic introduced"
      ;assert (eq? 4 (length (table-ref diagnostics "file:///Erroneous.java")))
      assert (equal? #("package de.hagiga.zamba;\n"
                       "\n"
                       "public class Erroneous {\n"
                       "\n"
                       "  public static final String TAG = \"No-Semicolon\";\n"
                       "\n"
                       "  public Erroneous(MissingIdentifier) {\n"
                       "    super();\n"
                       "    String it;\n"
                       "\n"
                       "    String s a = new String();\n"
                       "  }\n"
                       "\n"
                       "  public void set() {\n"
                       "    = this; // unknown\n"
                       "  }\n"
                       "}\n"
                       "\n")
                       (lsp-document-content
                         (table-ref documents "file:///Erroneous.java")))

      assert (equal? '((("range" ;; reverse again.
                         ("start" ("character" . 36) ("line" . 6))
                         ("end" ("character" . 36) ("line" . 6)))
                        ("severity" . 2)
                        ("code" . "Syntax Error")
                        ("message" . "missing identifier"))
                       (("range"
                         ("start" ("character" . 4) ("line" . 14))
                         ("end" ("character" . 5) ("line" . 14)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error"))
                       (("range"
                         ("start" ("character" . 11) ("line" . 10))
                         ("end" ("character" . 12) ("line" . 10)))
                        ("severity" . 1)
                        ("code" . "Syntax Error")
                        ("message" . "Abstract Syntax Tree parsing error")))
                     (table-ref diagnostics "file:///Erroneous.java")))

(define previous-diagnostics (table-ref diagnostics "file:///Erroneous.java"))

(test "one can query diagnostics for all files on demand"
      assert (equal? `(("kind" . "full")
                       ("items"
                        . ;; this time there didn't occure a change,
                          ;; so we did not reverse the ordering
                        ,(list->vector previous-diagnostics)))
                     (lsp-response-res
                       (textDocument/diagnostic
                         '(("textDocument"
                            ("uri" . "file:///Erroneous.java")))))))
