
(test-begin! "testing completion requests")

(test "completing on the first letter" assert (equal? #t #t))
(test "completing in the middle of a word" assert (equal? #t #t))
(test "completing on the last letter" assert (equal? #t #t))
(test "completing after the word is written" assert (equal? #t #t))
(test "completing but there are no suggestions" assert (equal? #t #t))

;; TODO:
;; complete respecting the current context.
;; complete and resolve additional information

