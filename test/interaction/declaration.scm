
(test-begin! "request a declaration")

(test "request a declaration of not shadowed binding" assert (equal? #t #t))
(test "request a declaration of an shadowing binding" assert (equal? #t #t))
(test "request a declaration of an shadowing binding again and retrieve the shadowed binding" assert (equal? #t #t))
(test "request a declaration from another opened file" assert (equal? #t #t))
(test "request a declaration from another but not opened file" assert (equal? #t #t))
(test "request a declaration from a file not in the scope but cached" assert (equal? #t #t))
