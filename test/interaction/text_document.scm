
(test-begin! "Opening text document")

(load "src/zamba.scm")
(include "../stdio-util.scm")

(define zamba
  (open-process
    (list path: "gsi"
          arguments: '("src/zamba.scm"))))

(write-msg "test/json/initialize.json" zamba)
(read-msg zamba)
(write-msg "test/json/initialized.json" zamba)

(write-msg "test/json/didOpen-almost-empty-file.json" zamba)

(define content #("\n"
                  "public class Empty{\n"
                  "\n"
                  "}\n"))

(test "received the didOpen notification?"
      assert (equal? #t (process-status zamba 1 #t)))

(textDocument/didOpen
  (with-input-from-file
    "test/json/didOpen-almost-empty-file.json"
    (lambda ()
      (cdr (assoc "params" (json-decode (current-input-port)))))))

(define sexp "(program (class_declaration (modifiers) name: (identifier) body: (class_body)))")

(test "didOpen was processed correctly"
      assert (= 1 (table-length documents))
      assert (equal? "java" (lsp-document-language
                                (table-ref documents "file:///imaginary.java")))
      assert (equal? 1 (lsp-document-version
                                (table-ref documents "file:///imaginary.java")))
      assert (equal? #t (string?
                          (ts--node-sexp!
                            (ts--tree-root-node
                              (lsp-document-tree
                                (table-ref documents "file:///imaginary.java"))))))
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (string=? sexp (ts--node-sexp!
                              (ts--tree-root-node
                                (lsp-document-tree
                                  (table-ref documents "file:///imaginary.java")))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 14 19 1 23 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 14
                 19)))

(test-begin! "test incremental text synchronization")

(define old-tree (ts--tree-copy!
                   (lsp-document-tree
                     (table-ref documents "file:///imaginary.java"))))

(vector-set! content 2 "  public static final String TAG = \"ALMOST\";\n")

(textDocument/didChange
  (with-input-from-file
    "test/json/didChange-almost-empty-file.json"
    (lambda ()
      (cdr (assoc "params" (json-decode (current-input-port)))))))

(test "compare old to new parse-tree"
      assert (equal? (+ (string-length "(field_declaration (modifiers) type: (type_identifier) declarator: (variable_declarator name: (identifier) value: (string_literal))) ")
                        (string-length (ts--node-sexp! (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 14 19 1 67 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 14
                 19))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 50 53 23 65 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 50
                 53)))

(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(set! content (vector-append
                #("import java.util.*;\n")
                content))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 3))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . "import java.util.*;\n")
                           ("range"
                            ("end"
                             ("character" . 0)
                             ("line" . 0))
                            ("start"
                             ("character" . 0)
                             ("line" . 0))))))))

(test "appending on the top of the file"
      assert (equal? (+ (string-length "(import_declaration (scoped_identifier scope: (identifier) name: (identifier)) (asterisk)) ")
                        (string-length (ts--node-sexp!  (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73)))


(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 4))
    ("contentChanges" . #((("rangeLength" . 0)
                           ("text" . "\npublic @interface Marker {}\n")
                           ("range"
                            ("end"
                             ("character" . 0)
                             ("line" . 5))
                            ("start"
                             ("character" . 0)
                             ("line" . 5))))))))

(set! content (vector-append
                content
                #("\n"
                  "public @interface Marker {}\n")))

(test "appending on the bottom of the file"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (+ (string-length " (annotation_type_declaration (modifiers) name: (identifier) body: (annotation_type_body))") (string-length (ts--node-sexp!  (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73))
      assert (equal?
               '(("file:///imaginary.java" "Marker" #f #f 107 113 89 116 #f 10))
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 107 113)))


(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 5))
    ("contentChanges" . #((("rangeLength" . 1)
                           ("text" . "")
                           ("range"
                            ("end"
                             ("character" . 0)
                             ("line" . 7))
                            ("start"
                             ("character" . 27)
                             ("line" . 6))))))))


(set! content
  #("import java.util.*;\n"
    "\n"
    "public class Empty{\n"
    "  public static final String TAG = \"ALMOST\";\n"
    "}\n"
    "\n"
    "public @interface Marker {}"))

(test "deleting the trailing newline character"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (string-length (ts--node-sexp! (ts--tree-root-node old-tree)))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73))
      assert (equal?
               '(("file:///imaginary.java" "Marker" #f #f 107 113 89 116 #f 10))
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 107 113)))


(test-begin! "full text synchroization")

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 6))
    ("contentChanges" . #((("text" . "\npublic class Empty{\n\n}\n"))))))

(set! content
  #("\n"
    "public class Empty{\n"
    "\n"
    "}\n"))

(test "resetting the content"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (string=? sexp (ts--node-sexp!
                              (ts--tree-root-node
                                (lsp-document-tree
                                  (table-ref documents "file:///imaginary.java")))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 14 19 1 23 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 14
                 19)))

(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 7))
    ("contentChanges" . #((("text" . "\npublic class Empty{\n  public static final String TAG = \"ALMOST\";\n}\n"))))))

(vector-set! content 2 "  public static final String TAG = \"ALMOST\";\n")

(test "compare old to new parse-tree"
      assert (equal? (+ (string-length "(field_declaration (modifiers) type: (type_identifier) declarator: (variable_declarator name: (identifier) value: (string_literal))) ")
                        (string-length (ts--node-sexp! (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 14 19 1 67 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 14
                 19))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 50 53 23 65 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 50
                 53)))

(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(set! content (vector-append
                #("import java.util.*;\n")
                content))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 8))
    ("contentChanges" . #((("text" . "import java.util.*;\n\npublic class Empty{\n  public static final String TAG = \"ALMOST\";\n}\n"))))))

(test "appending on the top of the file"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (+ (string-length "(import_declaration (scoped_identifier scope: (identifier) name: (identifier)) (asterisk)) ")
                        (string-length (ts--node-sexp!  (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73)))

(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(set! content (vector-append
                content
                #("\n"
                  "public @interface Marker {}\n")))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 9))
    ("contentChanges" . #((("text" . "import java.util.*;\n\npublic class Empty{\n  public static final String TAG = \"ALMOST\";\n}\n\npublic @interface Marker {}\n"))))))

(test "appending on the bottom of the file"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (+ (string-length " (annotation_type_declaration (modifiers) name: (identifier) body: (annotation_type_body))")
                        (string-length (ts--node-sexp!  (ts--tree-root-node old-tree))))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73))
      assert (equal?
               '(("file:///imaginary.java" "Marker" #f #f 107 113 89 116 #f 10))
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 107 113)))

(define old-tree
  (ts--tree-copy!
    (lsp-document-tree
      (table-ref documents "file:///imaginary.java"))))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 10))
    ("contentChanges" . #((("text" . "import java.util.*;\n\npublic class Empty{\n  public static final String TAG = \"ALMOST\";\n}\n\npublic @interface Marker {}"))))))

(set! content
  #("import java.util.*;\n"
    "\n"
    "public class Empty{\n"
    "  public static final String TAG = \"ALMOST\";\n"
    "}\n"
    "\n"
    "public @interface Marker {}"))

(test "deleting the trailing newline character"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (string-length (ts--node-sexp! (ts--tree-root-node old-tree)))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 87 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "TAG" "String" #f 70 73 43 85 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 70
                 73))
      assert (equal?
               '(("file:///imaginary.java" "Marker" #f #f 107 113 89 116 #f 10))
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 107 113)))

(textDocument/didChange
  '(("textDocument"
     ("uri" . "file:///imaginary.java")
     ("version" . 11))
    ("contentChanges" . #((("text" . "import java.util.*;\n\npublic class Empty{\n  public String tag = \"ALMOST\";\n}\n\npublic @interface Marker {}"))))))

(set! content
  #("import java.util.*;\n"
    "\n"
    "public class Empty{\n"
    "  public String tag = \"ALMOST\";\n"
    "}\n"
    "\n"
    "public @interface Marker {}"))

(test "apply two changes at once"
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (equal? (string-length (ts--node-sexp! (ts--tree-root-node old-tree)))
                     (string-length (ts--node-sexp!
                                      (ts--tree-root-node
                                        (lsp-document-tree
                                          (table-ref documents "file:///imaginary.java"))))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 34 39 21 74 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 34
                 39))
      assert (equal?
               '(("file:///imaginary.java" "tag" "String" #f 57 60 43 72 #f 5))
               (query--db statement-query-member "java" "file:///imaginary.java" "tag"
                 57
                 60))
      assert (equal?
               '(("file:///imaginary.java" "Marker" #f #f 94 100 76 103 #f 10))
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 94 100)))

(test-begin! "text document synchronization with low computation mode")

(text-synchronization 0) ; enable low computation mode
(set! output-thread (make-thread (lambda args 'DONE)))
(define was-called #f)
(define watch-zamba-configuration-file (lambda x (set! was-called #t)))

(textDocument/didSave
  '(("textDocument"
     ("uri" . "file:///imaginary.java"))
    ("text" . "\npublic class Empty{\n\n}\n")))

(set! content #("\n"
                "public class Empty{\n"
                "\n"
                "}\n"))

(test "updating the content on didSave"
      assert (equal? #t was-called)
      assert (equal? 0 (text-synchronization))
      assert (equal? 12 (lsp-document-version
                          (table-ref documents "file:///imaginary.java")))
      assert (equal? content
                     (lsp-document-content
                       (table-ref documents "file:///imaginary.java")))
      assert (string=? sexp (ts--node-sexp!
                              (ts--tree-root-node
                                (lsp-document-tree
                                  (table-ref documents "file:///imaginary.java")))))
      assert (equal?
               '(("file:///imaginary.java" "Empty" #f #f 14 19 1 23 #f 7))
               (query--db statement-query-member "java" "file:///imaginary.java" "Empty"
                 0
                 19))
      assert (equal?
               '()
               (query--db statement-query-member "java" "file:///imaginary.java" "TAG"
                 0
                 90))
      assert (equal?
               '()
               (query--db statement-query-member "java" "file:///imaginary.java" "Marker"
                 0 200)))

(test-begin! "closing the document")

(define freed-tree #f)
(define ts--tree-free! (lambda (x) (set! freed-tree x)))

(textDocument/didClose
  '(("textDocument"
     ("uri" . "file:///imaginary.java"))))

(test "document did close correctly"
      assert (equal? #t (if freed-tree #t #f))
      assert (equal? 'gone (table-ref documents "file:///imaginary.java" 'gone)))


(shutdown zamba)
(close-port zamba)
