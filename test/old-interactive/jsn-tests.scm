(define debug (make-parameter #t))

(display
  (json-encode
    (call-with-input-file
      "test/json/didOpen.json"
      (lambda (p) (json-decode p)))))

(call-with-input-file
      "test/json/initialize.json"
      (lambda (p) (json-decode p)))

(json-encode
(call-with-input-file
      "test/json/nvim_init.json"
      (lambda (p) (json-decode p))))

(display
  (json-encode
    (call-with-input-file
      "test/json/initialize.json"
      (lambda (p) (json-decode p)))))

(define uc #\a)

(char->integer uc)

(string->list
  (number->string (char->integer uc) 16))

(integer->char
  (+ (* (hex->int #\1) (expt 16 0))
     (* (hex->int #\6) (expt 16 1))
     (* (hex->int #\0) (expt 16 2))
     (* (hex->int #\0) (expt 16 3))))

(define x (call-with-input-file
      "test/json/initialize.json"
      (lambda (p) (json-decode p))))


(define x '("initializing zamba"
            (("clientInfo" ("version" . "0.6.0") ("name" . "Neovim"))
             ("workspaceFolders"
              .
              #((("uri" . "file:///home/tomes/workspace/zamba-ls/")
                 ("name" . "language_server"))))
             ("capabilities"
              ("textDocument"
               ("implementation" ("linkSupport" . #t))
               ("declaration" ("linkSupport" . #t))
               ("definition" ("linkSupport" . #t))
               ("typeDefinition" ("linkSupport" . #t))
               ("signatureHelp"
                ("dynamicRegistration" . #f)
                ("signatureInformation"
                 ("activeParameterSupport" . #t)
                 ("parameterInformation" ("labelOffsetSupport" . #t))
                 ("documentationFormat" . #("plaintext" "markdown"))))
               ("codeAction"
                ("resolveSupport" ("properties" . #("edit")))
                ("codeActionLiteralSupport"
                 ("codeActionKind"
                  ("valueSet"
                   .
                   #("source.organizeImports"
                     "source"
                     "refactor.rewrite"
                     "refactor.inline"
                     "refactor.extract"
                     "refactor"
                     "quickfix"
                     "SourceOrganizeImports"
                     "Source"
                     "RefactorRewrite"
                     "RefactorInline"
                     "RefactorExtract"
                     "Refactor"
                     "QuickFix"
                     "Empty"
                     ""))))
                ("dynamicRegistration" . #f)
                ("dataSupport" . #t))
               ("references" ("dynamicRegistration" . #f))
               ("completion"
                ("completionItemKind"
                 ("valueSet"
                  .
                  #(25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1)))
                ("contextSupport" . #f)
                ("dynamicRegistration" . #f)
                ("completionItem"
                 ("snippetSupport" . #f)
                 ("commitCharactersSupport" . #f)
                 ("preselectSupport" . #f)
                 ("deprecatedSupport" . #f)
                 ("documentationFormat" . #("plaintext" "markdown"))))
               ("hover"
                ("dynamicRegistration" . #f)
                ("contentFormat" . #("plaintext" "markdown")))
               ("documentHighlight" ("dynamicRegistration" . #f))
               ("documentSymbol"
                ("symbolKind"
                 ("valueSet"
                  .
                  #(26
                    25
                    24
                    23
                    22
                    21
                    20
                    19
                    18
                    17
                    16
                    15
                    14
                    13
                    12
                    11
                    10
                    9
                    8
                    7
                    6
                    5
                    4
                    3
                    2
                    1)))
                ("dynamicRegistration" . #f)
                ("hierarchicalDocumentSymbolSupport" . #t))
               ("synchronization"
                ("willSaveWaitUntil" . #f)
                ("didSave" . #t)
                ("dynamicRegistration" . #f)
                ("willSave" . #f))
               ("publishDiagnostics"
                ("tagSupport" ("valueSet" . #(2 1)))
                ("relatedInformation" . #t))
               ("rename" ("dynamicRegistration" . #f) ("prepareSupport" . #t)))
              ("workspace"
               ("workspaceFolders" . #t)
               ("applyEdit" . #t)
               ("workspaceEdit" ("resourceOperations" . #("delete" "create" "rename")))
               ("configuration" . #t)
               ("symbol"
                ("symbolKind"
                 ("valueSet"
                  .
                  #(26
                    25
                    24
                    23
                    22
                    21
                    20
                    19
                    18
                    17
                    16
                    15
                    14
                    13
                    12
                    11
                    10
                    9
                    8
                    7
                    6
                    5
                    4
                    3
                    2
                    1)))
                ("dynamicRegistration" . #f)
                ("hierarchicalWorkspaceSymbolSupport" . #t)))
              ("callHierarchy" ("dynamicRegistration" . #f))
              ("window"
               ("workDoneProgress" . #t)
               ("showMessage" ("messageActionItem" ("additionalPropertiesSupport" . #f)))
               ("showDocument" ("support" . #f))))
             ("processId" . 435751)
             ("initializationOptions")
             ("trace" . "off")
             ("rootPath" . "/home/tomes/workspace/zamba-ls/test/files")
             ("rootUri" . "file:///home/tomes/workspace/zamba-ls/test/files"))))

(json-encode x)

(define y "{\"id\":1,\"method\":\"initialize\",\"jsonrpc\":\"2.0\",\"params\":{\"trace\":\"off\",\"rootPath\":\"\\/home\\/tomes\\/workspace\\/zamba-ls\\/test\\/files\",\"rootUri\":\"file:\\/\\/\\/home\\/tomes\\/workspace\\/zamba-ls\\/test\\/files\",\"clientInfo\":{\"version\":\"0.6.0\",\"name\":\"Neovim\"},\"workspaceFolders\":[{\"uri\":\"file:\\/\\/\\/home\\/tomes\\/workspace\\/zamba-ls\\/\",\"name\":\"language_server\"}],\"capabilities\":{\"workspace\":{\"workspaceEdit\":{\"resourceOperations\":[\"rename\",\"create\",\"delete\"]},\"configuration\":true,\"symbol\":{\"symbolKind\":{\"valueSet\":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]},\"dynamicRegistration\":false,\"hierarchicalWorkspaceSymbolSupport\":true},\"workspaceFolders\":true,\"applyEdit\":true},\"textDocument\":{\"synchronization\":{\"didSave\":true,\"dynamicRegistration\":false,\"willSave\":false,\"willSaveWaitUntil\":false},\"references\":{\"dynamicRegistration\":false},\"implementation\":{\"linkSupport\":true},\"publishDiagnostics\":{\"relatedInformation\":true,\"tagSupport\":{\"valueSet\":[1,2]}},\"declaration\":{\"linkSupport\":true},\"codeAction\":{\"codeActionLiteralSupport\":{\"codeActionKind\":{\"valueSet\":[\"\",\"Empty\",\"QuickFix\",\"Refactor\",\"RefactorExtract\",\"RefactorInline\",\"RefactorRewrite\",\"Source\",\"SourceOrganizeImports\",\"quickfix\",\"refactor\",\"refactor.extract\",\"refactor.inline\",\"refactor.rewrite\",\"source\",\"source.organizeImports\"]}},\"dynamicRegistration\":false,\"dataSupport\":true,\"resolveSupport\":{\"properties\":[\"edit\"]}},\"definition\":{\"linkSupport\":true},\"typeDefinition\":{\"linkSupport\":true},\"signatureHelp\":{\"signatureInformation\":{\"parameterInformation\":{\"labelOffsetSupport\":true},\"documentationFormat\":[\"markdown\",\"plaintext\"],\"activeParameterSupport\":true},\"dynamicRegistration\":false},\"rename\":{\"prepareSupport\":true,\"dynamicRegistration\":false},\"hover\":{\"contentFormat\":[\"markdown\",\"plaintext\"],\"dynamicRegistration\":false},\"completion\":{\"contextSupport\":false,\"completionItem\":{\"preselectSupport\":false,\"deprecatedSupport\":false,\"documentationFormat\":[\"markdown\",\"plaintext\"],\"snippetSupport\":false,\"commitCharactersSupport\":false},\"dynamicRegistration\":false,\"completionItemKind\":{\"valueSet\":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]}},\"documentHighlight\":{\"dynamicRegistration\":false},\"documentSymbol\":{\"symbolKind\":{\"valueSet\":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]},\"dynamicRegistration\":false,\"hierarchicalDocumentSymbolSupport\":true}},\"callHierarchy\":{\"dynamicRegistration\":false},\"window\":{\"workDoneProgress\":true,\"showMessage\":{\"messageActionItem\":{\"additionalPropertiesSupport\":false}},\"showDocument\":{\"support\":false}}},\"initializationOptions\":{},\"processId\":436744}}")

(call-with-input-string
  x
  (lambda (p) (json-decode p)))
