(load "../../src/util.scm")

(define byte-1 #b01111111)
(define byte-2 (+ #b11011111
                  #b10111111))
(define byte-3 (+ #b11101111
                  #b10111111
                  #b10111111))
(define byte-4 (+ #b11101111
                  #b10111111
                  #b10111111
                  #b10111111))

(define byte1 #b01111111) ;#x7f
(define byte2 #b11111111111) ;#x7ff
(define byte3 #b1111111111111111) ;#xffff up until here 2 bytes utf-16
(define byte4 #x10ffff)
(define utf-8-string "∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i), ∀x∈ℝ: ⌈x⌉ = −⌊−x⌋, α ∧ ¬β = ¬(¬α ∨ β)")

