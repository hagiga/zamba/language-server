
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; loading recursive is a big thread to the progam.
;; one needs to prevent recursion when walking dependencies.

(load "src/zamba")
(load "bin/ts-java-binding.o1")
(set! known-languages `(("java" . ,(ts--java#language-new))))
(define muri "test/files/Observable")
(define mcontent (call-with-input-file muri read-large-chunk))
(define mlang (cdr (assoc "java" known-languages)))
(define mtree (ts-parse-with-string mlang mcontent))
(define jdoc (make-lsp-document "debug" "java" mtree))
(table-set! documents "Observable" jdoc)

;(time (ts--java#make-dependency-graph jdoc mcontent))

(time (ts--java#make-bindings muri jdoc mcontent))

(sqlite3--exec database "PRAGMA main.cache_size=20000;")

(sqlite3--exec database "PRAGMA main.cache_size=20000;")
(sqlite3--exec database "PRAGMA java.journal_mode=off;")
(sqlite3--query (sqlite3--prepare database "PRAGMA main.cache_size;") (lambda x (pp x)))
(sqlite3--query (sqlite3--prepare database "PRAGMA main.foreign_keys;") (lambda x (pp x)))
(sqlite3--query (sqlite3--prepare database "PRAGMA main.mmap_size;") (lambda x (pp x)))

(sqlite3--bind! statement-select-binding-id
                "file:///home/tomes/workspace/zamba-ls/test/files/SimpleClass.java"
                "StrangeInterface")
(sqlite3--query statement-select-binding-id
                (lambda x (pp x)))

(load "bin/sqlite3-binding.o1")
(define database (sqlite3--open "file::memory:?cache=shared"))
(define (read-large-chunk p)
  (let* ((buffer (make-string 100000))
         (size (read-substring buffer 0 100000 p)))
    (substring buffer 0 size)))

(sqlite3--exec database "ATTACH 'file:java?cache=shared&mode=memory' as java;")

(for-each (lambda (x)
            (sqlite3--step! x)
            (sqlite3--finalize! x))
          (sqlite3--prepare
            database
            (call-with-input-file
              (path-normalize
                "lib/lang/java/sql/create-statement.sql")
              read-large-chunk)))

(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/binding-insert-statement.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/select-binding-id.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/insert-argument.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/insert-field.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/insert-function.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/insert-interface.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/insert-superclass.sql") read-large-chunk))
(sqlite3--prepare database (call-with-input-file (path-normalize "lib/lang/java/sql/select-function.sql") read-large-chunk))


(define mquery (make-query jdoc
                           (fold (lambda (u str)
                                   (string-append str
                                     (call-with-input-file
                                       (path-normalize u) read-large-chunk))) ""
                                 '("lib/lang/java/binding/class.ss"
                                   "lib/lang/java/binding/interface.ss"
                                   "lib/lang/java/binding/enum.ss"
                                   "lib/lang/java/binding/annotation.ss"
                                   "lib/lang/java/binding/field.ss"
                                   "lib/lang/java/binding/constructor.ss"
                                   "lib/lang/java/binding/function.ss"
                                   ))))
(time
(ts-query-execute!
  (ts--tree-root-node mtree)
  mquery
  (lambda x
    (let ((no (car x))
          (pattern-index (cadr x))
          (captures (caddr x)))
      (for-each
        (lambda (y)
          (pp (list
                "number: " no
                "pattern: " pattern-index
                "y: " y
                "capture-index: " (ts--query-capture-index y)
                "capture: " (ts--query-capture-name-for-id
                              mquery
                              (ts--query-capture-index y))
                "value: " (substring mcontent
                            (ts--node-start-byte
                              (ts--query-capture-node y))
                            (ts--node-end-byte
                              (ts--query-capture-node y))))))
        captures))))
)
