;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; loading recursive is a big thread to the progam.
;; one needs to prevent recursion when walking dependencies.

(load "src/zamba")
(define muri "test/files/SimpleClass.java")
(define mcontent (call-with-input-file muri read-large-chunk))
(define mlang (cdr (assoc "java" known-languages)))
(define mtree (ts-parse-with-string mlang mcontent))
(define mtable (make-table))
(define jdoc (make-lsp-document
               "debug"
               mlang
               mtree))
(table-set! documents "file:///home/tomes/workspace/zamba-ls/test/files/SimpleClass.java" jdoc)

(define (ts-query-capture! doc patterns thunk #!optional (outline #f))
  (letrec* ((cursor (ts--query-cursor-new)) ;; FIXME: reuse or share this struct
            (root (ts--tree-root-node (lsp-document-tree doc)))
            (error* (make-unsigned-int32*))
            (query-error* (make-ts--query-error))
            (query (ts--query-new (lsp-document-language doc)
                                  patterns
                                  (string-length patterns)
                                  error*
                                  query-error*))
            (abort (if (not (= 0 (ts--query-error-get query-error*)))
                     (error (string-append
                              "could not create the query. error offset: "
                              (number->string (ts--resolve-uint-32 error*))
                              " error code: " (number->string query-error*)))))
            ;; count of individual sexp
            (capture-count (ts--query-count-capture query))
            (get-capture-names
              (lambda (lst ct)
                (if (= ct capture-count)
                  (reverse lst)
                  (get-capture-names (cons (ts--query-capture-name-for-id query ct) lst) (+ ct 1)))))
            (string-count (ts--query-count-string query))
            (get-string-literals
              (lambda (lst ct)
                (if (= ct string-count)
                  (reverse lst)
                  (get-string-literals (cons (ts--query-string-value-for-id query ct) lst) (+ ct 1)))))
            (pattern-length (make-unsigned-int32*))
            (get-steps
              (lambda (lst wrk p i)
                (if (> i 0)
                  (case (ts--predicate-type p)
                    ((0) (get-steps (cons (reverse wrk) lst) '() (ts--predicate-next! p) (- i 1)))
                    ((1) (get-steps lst (cons (cons 'capture (ts--query-capture-name-for-id query (ts--predicate-value p))) wrk) (ts--predicate-next! p) (- i 1)))
                    ((2) (get-steps lst (cons (cons 'string (ts--query-string-value-for-id query (ts--predicate-value p))) wrk) (ts--predicate-next! p) (- i 1)))
                    (else (error "undefined TSQueryPredicateStepType")))
                  (reverse lst))))
            (pattern-count (ts--query-count-pattern query))
            (get-pattern-predicates
              ;; (list "pattern0-predicates0" (list-ref (list-ref pattern-predicates 0) 0))
              (lambda (lst ct)
                (if (= ct pattern-count)
                  (reverse lst)
                  (get-pattern-predicates (cons (get-steps '() '() (ts--query-predicates-for-pattern query ct pattern-length) (ts--resolve-uint-32 pattern-length)) lst) (+ ct 1)))))
            (capture-names (get-capture-names '() 0))
            (pred-literals (get-string-literals '() 0))
            (pattern-predicates (get-pattern-predicates '() 0))
            )
    ;; predicates could be implemented with closures. Something like this:
    ;; (letrec ((arg-1 #f)
    ;;          (arg-2 #f)
    ;;          (setter (lambda (x)
    ;;                    (cond
    ;;                      ((and arg-1 arg-2) (error "invalid call to closure"))
    ;;                      ((not arg-1) (set! arg-1 x) setter)
    ;;                      ((not arg-2) (set! arg-2 x) func)
    ;;                      (else (error "invalid state occured")))))
    ;;          (func (lambda () ;; eq?
    ;;                  (if (and arg-1 arg-2)
    ;;                    (cond
    ;;                      ((and
    ;;                         (eq? (car arg-1) 'capture)
    ;;                         (eq? (car arg-2) 'capture))
    ;;                       (list (cdr arg-1) (cdr arg-2) 'equal-cap))
    ;;                      ((and
    ;;                         (eq? (car arg-1) 'capture)
    ;;                         (eq? (car arg-2) 'string))
    ;;                       (list (cdr arg-1) (cdr arg-2) 'equal-match))
    ;;                      (else
    ;;                        'everything-sucks))
    ;;                    (error (string-append "invalid arguments"
    ;;                                          (object->string arg-1)
    ;;                                          (object->string arg-2)))))))
    ;;   setter)
    (case query
      ((#f) (error "query was not created. Check arguments to ts-query-execute!"))
      (else
        (ts--query-cursor-match-limit-set! cursor (match-limit))

        (if outline
            (ts--query-limit-execution-range!
              cursor
              (car outline)
              (cdr outline)))
        (ts--query-cursor-execute! cursor query root)

        (if (ts--query-cursor-did-exceed-limit? cursor) (display "EXCEEDED LIMIT"))

        (letrec* ((matches '())
                  (match (make-ts--query-match)) ;; TODO: free
                  (pred-size (make-unsigned-int32*)) ;; TODO: free
                  ;(make-predicate
                  ;  (lambda (p)
                  ;    (case (ts--predicate-type p)
                  ;      ((0 1) (error "invalid first step of predicate"))
                  ;      (cond
                  ;        ((eq? "eq?" (ts--query-string-value-for-id query (ts--predicate-value p)))
                  ;         (let ((args '())
                  ;               (func (lambda (captures)
                  ;                       (let* ((compare (map (lambda (x)
                  ;                                              (let ((cap (call/cc (lambda (y) y))))
                  ;                                                (cond
                  ;                                                  ((procedure? cap)
                  ;                                                   (for-each (lambda (y)
                  ;                                                               (if (string=? (ts--query-capture-name-for-id query (ts--query-capture-index y)) x)
                  ;                                                                 (cap y))) captures)
                  ;                                                   x)
                  ;                                                  (else (substring mcontent (ts--node-start-byte cap) (ts--node-end-byte cap))))))
                  ;                                            args))
                  ;                              (res (call/cc (lambda (x) x)))
                  ;                              (first (car compare)))
                  ;                         (if (procedure? res)
                  ;                           (begin
                  ;                             (for-each (lambda (x) (if (not (string=? x first))
                  ;                                                     (res #f)))
                  ;                                       (cdr compare))
                  ;                             #t)
                  ;                           #f))))
                  ;               (me (lambda (p arg)
                  ;                     (if (= (ts--predicate-type p) 0) ;; done
                  ;                       (func arg)
                  ;                       (begin
                  ;                         (set! args
                  ;                           (cons
                  ;                             (if (= (ts--predicate-type p) 1)
                  ;                               (ts--query-capture-name-for-id query (ts--predicate-value p))
                  ;                               (ts--query-string-value-for-id query (ts--predicate-value p)))
                  ;                             args))
                  ;                         me)))))
                  ;           (let lp ((func (me (ts--predicate-next! p))))
                  ;             (if (procedure? func)
                  ;               (lp (me (ts--predicate-next! p)))
                  ;               func)))))
                  ;      (else (error "unknown predicate")))))
                  ;(verify-match
                  ;  (lambda (abort)
                  ;    (let* ((p (ts--query-predicates-for-pattern query (ts--query-match-pattern-index match) pred-size))
                  ;           (predicate (if (> pred-size 0)
                  ;                        (make-predicate p)
                  ;                        (abort #t)))
                  ;           )
                  ;      (for-each

                  ;        (ts--query-cursor-remove-match! cursor (ts--query-match-id match))
                  ;        #f)
                  ;      #t)))
                  ;(iterate-match
                  ;  (lambda (captures i)
                  ;    (if (=> i (ts--query-match-capture-count match)) captures
                  ;      (iterate-match (cons (ts--query-match-capture-node-at match i) captures) (+ i 1)))))

                  (walk-match-cursor
                    (lambda ()
                      (if (ts--query-cursor-next-match! cursor match)
                        (let ((pred (if (> (length pattern-predicates) (ts--query-match-pattern-index match))
                                      (list-ref pattern-predicates (ts--query-match-pattern-index match))
                                      #f))
                              (captures (iterate-match '() 0))
                              (skip (call/cc (lambda (x) x))))
                          (if (procedure? skip)
                            (begin
                              (for-each (lambda (cap)
                                          ;; has-predicates!
                                          "nuttin"
                                          (let* ((cap (ts--query-match-capture-node-at match i))
                                                 (cap-name (ts--query-capture-name-for-id query (ts--query-capture-index cap))))
                                            (thunk cap-name cap)
                                            (walk-match-cursor))
                                          ))))))))
                      (capture-index (make-unsigned-int32*))
                      (walk-capture-cursor
                        (lambda (captures)
                          (if (ts--query-cursor-next-capture! cursor match capture-index)
                            (let ((capture (ts--query-match-capture match)))
                              (pp (list
                                    "capture-names: " capture-names
                                    "overall-capture-index: " (ts--resolve-uint-32 capture-index)
                                    "pattern-index: " (ts--query-match-pattern-index match)
                                    "capture-name" (ts--query-capture-name-for-id
                                                     query
                                                     (ts--query-capture-index capture))
                                    "match-id: " (ts--query-match-id match)))
                              (walk-capture-cursor (cons
                                                     (substring mcontent
                                                                (ts--node-start-byte (ts--query-capture-node  (ts--query-match-capture match)))
                                                                (ts--node-end-byte (ts--query-capture-node  (ts--query-match-capture match))))
                                                     captures)))
                            captures)))
                      (res (walk-capture-cursor '())))
                    (ts--query-cursor-free! cursor)
                    (ts--query-free! query)
                    ;; FIXME: free memory
                    ;; error*
                    ;; query-error*
                    ;; pattern-length
                    ;; match
                    ;; capture-index
                    res)))))

(ts-query-capture! jdoc "(interface_declaration
                                   (identifier) @name
                                   ) @interface
(method_invocation) @method
(class_declaration
  (identifier) @name
  (superclass) @super
  )" #f)

;"((class_declaration
;    (identifier) @name
;    ) @declaration
;  (#match? @name \"SimpleClass\"))")
