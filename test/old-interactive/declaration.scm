;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; loading recursive is a big thread to the progam.
;; one needs to prevent recursion when walking dependencies.

(load "src/zamba")
(define muri "test/files/SimpleClass.java")
(define mcontent (call-with-input-file muri read-large-chunk))
(define mlang (cdr (assoc "java" known-languages)))
(define mtree (ts-parse-with-string mlang mcontent))
(define mtable (make-table))
(define jdoc (make-lsp-document
               "debug"
               mlang
               mtree))
(table-set! documents "file:///home/tomes/workspace/zamba-ls/test/files/SimpleClass.java" jdoc)

(table-for-each (lambda (k v) (print (list "tree=" (lsp-document-tree v))) (newline)) documents)

(lsp-document-tree-set! #4 #3)

;(ts--java#make-dependency-graph jdoc mcontent)
(table-for-each (lambda (k v) (print v) (newline)) documents)

(ts-declaration-internal
  "java"
  jdoc
  (get-file-content jdoc)
  15
  19
  )


