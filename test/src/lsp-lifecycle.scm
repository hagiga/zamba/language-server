
(test-begin! "Lifecycle Methods and Workspace Initialization")

(load "src/zamba.scm")
(define thread-send (lambda x 'SINK))

(define params
    '(("workspaceFolders" .  #((("uri" . "file:///home/user/workspace/directory/") ("name" . "language_server"))))
      ("rootUri" . "file:///home/user/workspace/directory")
      ("capabilities"
       ("workspace"
        ("workspaceFolders" . #t)
        ("configuration" . #t)
        ("applyEdit" . #t))
       ("textDocument"
        ("publishDiagnostics" . #t)
        ("completion"
         ("completionItem"
          ("snippetSupport" . #f)
          ("tagSupport" ("valueSet" . #(1)))
          ("deprecatedSupport" . #t)))
        ("synchronization"
         ("willSaveWaitUntil" . #f)
         ("willSave" . #f)
         ("didSave" . #t))
        ("codeAction"
         ("codeActionLiteralSupport"
          ("codeActionKind"
           ("valueSet" . #("quickfix" "refactor" "source")))))
        ("signatureHelp" ("dynamicRegistration" . #f))
        ("implementation" ("linkSupport" . #f))
        ("references" ("linkSupport" . #f))
        ("declaration" ("linkSupport" . #f))
        ("hover" ("contentFormat" . #("plaintext" "markdown")))
        ("definition" ("dynamicRegistration" . #f))))
      ("clientInfo" ("name" . "client"))
      ("processId" . 4711)
      ("trace" . "off")))

(test "variables start in correct state"
        assert (equal? #f (workspace-configuration))
        assert (equal? #f (workspace-folders-resolved))
        assert (equal? #f (workspace-folders-change))
        assert (equal? #f (did-save))
        assert (equal? 2 (text-synchronization))
        assert (equal? #f (publish-diagnostics))
        assert (equal? #f (hover))
        assert (equal? #f (declaration))
        assert (equal? #f (definition))
        assert (equal? #f (implementation))
        assert (equal? #f (references))
        assert (equal? #f (completion-resolve))
        assert (= server-not-initialized (status-code)))

(define server-capabilities (lsp-response-res (initialize params)))
(test "status code updates after initialize"
      assert (= server-initializing (status-code)))

(test "state changed after calling initialize"
      assert (equal? #t (workspace-configuration))
      assert (equal? #t (workspace-folders-resolved))
      assert (equal? #t (workspace-folders-change))
      assert (equal? #t (did-save))
      assert (equal? 2 (text-synchronization))
      assert (equal? #t (publish-diagnostics))
      assert (equal? #t (hover))
      assert (equal? #t (declaration))
      assert (equal? #t (definition))
      assert (equal? #t (implementation))
      assert (equal? #t (references))
      assert (equal? #f (completion-resolve)))

(test "result looks like expected"
      assert (equal? `(("capabilities"
                        ("workspace"
                         ("configuration" . #t)
                         ("workspaceFolders" ("supported" . #t) ("changeNotifications" . #t)))
                        ("textDocumentSync"
                         ("save" ("includeText" . #t))
                         ("openClose" . #t)
                         ("change" . 2))
                        ("diagnosticProvider" . #t)
                        ("hoverProvider" . #t)
                        ("declarationProvider" . #t)
                        ("definitionProvider" . #t)
                        ("implementationProvider" . #t)
                        ("referencesProvider" . #t)
                        ("completionProvider" ("resolveProvider" . #f)))
                       ("serverInfo" ("name" . "ZAMBA-LS") ("version" . ,version)))
                     server-capabilities))

(test "workspaceFolders are taken"
      assert (= 1 (length workspaces))
      assert (equal? "file:///home/user/workspace/directory/" (lsp-workspace-uri (car workspaces)))
      assert (equal? 'workspace-folder (lsp-workspace-kind (car workspaces))))

;; reconfigure
(define params
    '(("rootPath" . "file:///home/user/workspace/directory/false")
      ("rootUri" . "file:///home/user/workspace/directory/correct")
      ("capabilities"
       ("textDocument"
        ("completion"
         ("completionItem"
          ("snippetSupport" . #f)
          ("tagSupport" ("valueSet" . #(1)))
          ("deprecatedSupport" . #t)))
        ("synchronization"
         ("willSaveWaitUntil" . #f)
         ("willSave" . #f)
         ("didSave" . #f))
        ("publishDiagnostics" ("any"))
        ("codeAction"
         ("codeActionLiteralSupport"
          ("codeActionKind"
           ("valueSet" . #("quickfix" "refactor" "source")))))
        ("implementation" ("linkSupport" . #f))))
      ("clientInfo" ("name" . "client"))
      ("processId" . 4711)
      ("trace" . "off")))

(workspace-configuration #f)
(workspace-folders-resolved #f)
(workspace-folders-change #f)
(did-save #f)
(text-synchronization 2)
(publish-diagnostics #f)
(hover #f)
(declaration #f)
(definition #f)
(implementation #f)
(references #f)
(completion-resolve #f)

(test "respecting client capabilities"
      assert (equal? `(("capabilities"
                        ("workspace"
                         ("configuration" . #f)
                         ("workspaceFolders" ("supported" . #f) ("changeNotifications" . #f)))
                        ("textDocumentSync"
                         ("save" ("includeText" . #f))
                         ("openClose" . #t)
                         ("change" . 2))
                        ("diagnosticProvider" . #t)
                        ("hoverProvider" . #f)
                        ("declarationProvider" . #f)
                        ("definitionProvider" . #f)
                        ("implementationProvider" . #t)
                        ("referencesProvider" . #f)
                        ("completionProvider" ("resolveProvider" . #f)))
                       ("serverInfo" ("name" . "ZAMBA-LS") ("version" . ,version)))
                     (lsp-response-res (initialize params)))
      assert (equal? #f (workspace-configuration))
      assert (equal? #f (workspace-folders-resolved))
      assert (equal? #f (workspace-folders-change))
      assert (equal? #f (did-save))
      assert (equal? 2 (text-synchronization))
      assert (equal? #t (publish-diagnostics))
      assert (equal? #f (hover))
      assert (equal? #f (declaration))
      assert (equal? #f (definition))
      assert (equal? #t (implementation))
      assert (equal? #f (references))
      assert (equal? #f (completion-resolve))
      assert (= server-initializing (status-code)))

(test "rootUri is used as a workspace"
      assert (= 1 (length workspaces))
      assert (equal? "file:///home/user/workspace/directory/correct" (lsp-workspace-uri (car workspaces)))
      assert (equal? 'workspace-root (lsp-workspace-kind (car workspaces))))

;; reconfigure
(define params
    '(("capabilities"
       ("textDocument"
        ("codeAction"
         ("codeActionLiteralSupport"
          ("codeActionKind"
           ("valueSet" . #("quickfix" "refactor" "source")))))))
      ("clientInfo" ("name" . "client"))
      ("processId" . 4711)
      ("trace" . "off")))

(workspace-configuration #f)
(workspace-folders-resolved #f)
(workspace-folders-change #f)
(did-save #f)
(text-synchronization 2)
(hover #f)
(declaration #f)
(definition #f)
(implementation #f)
(references #f)
(completion-resolve #f)

(test "respecting client capabilities"
      assert (equal? `(("capabilities"
                        ("workspace"
                         ("configuration" . #f)
                         ("workspaceFolders" ("supported" . #f) ("changeNotifications" . #f)))
                        ("textDocumentSync"
                         ("save" ("includeText" . #f))
                         ("openClose" . #t)
                         ("change" . 2))
                        ("diagnosticProvider" . #f)
                        ("hoverProvider" . #f)
                        ("declarationProvider" . #f)
                        ("definitionProvider" . #f)
                        ("implementationProvider" . #f)
                        ("referencesProvider" . #f)
                        ("completionProvider" ("resolveProvider" . #f)))
                       ("serverInfo" ("name" . "ZAMBA-LS") ("version" . ,version)))
                     (lsp-response-res (initialize params)))
      assert (equal? #f (workspace-configuration))
      assert (equal? #f (workspace-folders-resolved))
      assert (equal? #f (workspace-folders-change))
      assert (equal? #f (did-save))
      assert (equal? 2 (text-synchronization))
      assert (equal? #f (publish-diagnostics))
      assert (equal? #f (hover))
      assert (equal? #f (declaration))
      assert (equal? #f (definition))
      assert (equal? #f (implementation))
      assert (equal? #f (references))
      assert (equal? #f (completion-resolve))
      assert (= server-initializing (status-code)))

(test "test workspace defaults to current directory of zamba"
      assert (= 1 (length workspaces))
      assert (equal? (string-append "file://" (current-directory)) (lsp-workspace-uri (car workspaces)))
      assert (equal? 'workspace-root (lsp-workspace-kind (car workspaces))))


(define (thread-send . args) 'stub)
(initialized '())

(test "status code updates after initializd"
      assert (= server-initialized (status-code)))
