
(test-begin! "Cache initialization")
(load "src/zamba.scm")

(test "default values are set"
      assert (equal? #t (not (null? known-languages))))

(test "read-large-chunk unit test"
      assert (equal? "read\nexactlyas\r\nOKAY" (call-with-input-string "read\nexactlyas\r\nOKAY" read-large-chunk)))

(test "read-content unit test"
      assert (equal? #("don't\n" "read\n" "exactlyas\r\n" "OKAY\n") (call-with-input-string "don't\nread\nexactlyas\r\nOKAY\n" read-content)))

(test "no documents are loaded at startup"
      assert (equal? 0 (table-length documents)))

(define doc (make-lsp-document 0 "dummy-language" "no-tree"))
(table-set! documents "file:///dummy" doc)

(test "get document by an uri."
      assert (equal? doc (document-by-uri "file:///dummy")))

(test "session databases exists if debug"
      assert (equal? (debug)
                     (file-exists? (string-append (cache-dir) "session"))))

(test "cache database exists."
      (equal? #t (file-exists? (string-append (cache-dir) "cache"))))

(test "SQLite3 statements are prepared"
      assert (equal? #f (not statement-query-file-indexed))
      assert (equal? #f (not statement-save-opened))
      assert (equal? #f (not statement-renew-file))
      assert (equal? #f (not statement-save-file))
      assert (equal? #f (not statement-save-language))
      assert (equal? #f (not statement-save-binding))
      assert (equal? #f (not statement-save-local))
      assert (equal? #f (not statement-save-invocation))
      assert (equal? #f (not statement-save-implementation))
      assert (equal? #f (not statement-query-rowid))
      assert (equal? #f (not statement-query-language))
      assert (equal? #f (not statement-query-open-docs))
      assert (equal? #f (not statement-query-completions))
      assert (equal? #f (not statement-query-local))
      assert (equal? #f (not statement-query-member))
      assert (equal? #f (not statement-query-all))
      assert (equal? #f (not statement-query-imported))
      assert (equal? #f (not statement-query-references))
      assert (equal? #f (not statement-query-impl))
      assert (equal? #f (not statements-update-by-offset))
      assert (equal? #f (not statements-drop-between))
      assert (equal? #f (not statement-release-doc)))
