
(test-begin! "Initialize Project-Configuration-File")

(load "src/zamba.scm")

(define thread-send (lambda x 'SINK))

(test "default values in config.scm"
      assert (string=? "\n" (line-ending))
      assert (string=? (current-directory) (zamba-directory))
      assert (equal? #f (uses-repl?))
      assert (= 8000 (match-limit))
      assert (eqv? #t (show-abstract-syntax-tree))
      assert (= 0.1 (progress-report-interval))
      assert (string=? "off" (trace-lvl))
      assert (eqv? #f (debug))
      assert (equal? #t (procedure? watch-zamba-configuration-file)))

(test "without config-file"
      assert (equal? 'no-config-file (watch-zamba-configuration-file)))

(call-with-output-file
  ".zamba" (lambda (p)
             (display (string-append (current-directory) "test/src")
                      p)
             (newline p)
             (force-output p)))

(load "src/config.scm")

(test "with config-file"
      assert (equal? (string-append (current-directory) ".zamba")
                     (watch-zamba-configuration-file)))


(test "updating config-file"
      (thread-sleep! 1) ;; my filesystem only keeps modification times in steps of seconds
      (call-with-output-file
        ".zamba" (lambda (p)
                   (display (string-append (current-directory) "test/lib")
                            p)
                   (newline p)
                   (force-output p)))
      assert (equal?  'reread-config-file (watch-zamba-configuration-file)))

(delete-file ".zamba")
