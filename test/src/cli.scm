
(test-begin! "Resolving Languages")

(load "src/zamba.scm")
(test "preparation of supported languages"
      assert (equal? '((".java" . "java")
                       (".py"   . "python")
                       (".h"    . "c") (".c"    . "c")
                       (".scm"  . "scheme") (".ss"   . "scheme"))
                     languages-by-suffix))

(test "supported-languages contains the expected languages"
      assert (equal? '(("java" . 1) ("python" . 2) ("c" . 3) ("scheme" . 4))
                     supported-languages))

(test-begin! "Command Line Arguments")
;
;(test-against "src/zamba.scm" "src/util.scm")
;
;;; retrieve internal binding under test
;;; by defining it here, it can use recursion.
(define parse-config (resolve "src/zamba.scm" 'parse-config))
;
(test "arguments are processed as desired."
      assert (eq? #f (uses-repl?))
      assert (eq? #f (debug))
      assert (eq? #t (show-abstract-syntax-tree))
      assert (= 2 (text-synchronization))
      assert (= 0 (length (parse-config
                            '("--tag-mode"
                              "--debug"
                              "--repl" "4711"
                              "--omit-abstract-syntax-tree"))))
      assert (eq? #t (uses-repl?))
      assert (= 4711 (socket-info-port-number
                         (tcp-server-socket-info gamrepl#tcp-server)))
      assert (eq? #t (debug))
      assert (eq? #f (show-abstract-syntax-tree))
      assert (= 0 (text-synchronization)))

(delete-file (string-append (cache-dir) "cache"))
