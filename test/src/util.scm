(test-begin! "Auxilary functions from util.scm")

(load "src/zamba.scm")

(test "read in a file relative to the current directory"
      assert (string=? "{\n  \"jsonrpc\": \"2.0\",\n  \"method\": \"textDocument/didOpen\",\n  \"id\": 3\n}\n\n"
                       (read-file-relative-to
                         (current-directory)
                         "test/json/no-params.json")))

(test "get a comparable modifiation time"
      assert (equal? #t (number? (modification-time? (path-expand
                                                       "test/src/util.scm"))))
      assert (equal? 0 (remainder (modification-time? (path-expand
                                                        "test/src/util.scm"))
                                  1)))

(define content (call-with-input-file
                  "langfiles/java-simple/SimpleClass.java"
                  read-content))
(define doc (make-lsp-document 0 "java" (ts-parse "java" content)))
(lsp-document-content-set! doc content)

(test "reading from the root node and retrieve complete content"
      assert (equal? (fold
                       (lambda (line acc)
                         (string-append acc line))
                       ""
                       (vector->list content))
                     (word-at (ts--tree-root-node (lsp-document-tree doc)) doc)))

(test "reading the first comment in the top-level spanning multiple lines (hardcoded)"
      assert (equal? "(block_comment)"
                     (ts--node-sexp!
                       (ts--node-child-at
                         (ts--tree-root-node (lsp-document-tree doc))
                         6)))
      assert (equal? "/**\n * THIS IS A JAVA DOC COMMENT\n */"
                     (read-node
                       content
                       (ts--node-child-at
                         (ts--tree-root-node (lsp-document-tree doc))
                         6))))

(test "reading the first marker-interface in the top-level (hardcoded)"
      assert (equal? "public @interface MarkerInterface {}"
                     (read-node
                       content
                       (ts--node-child-at
                         (ts--tree-root-node (lsp-document-tree doc))
                         8))))

(test "parsing uris with german umlauts"
      assert (equal? "/Ümläutö/"
                     (parse-URI "file:///%C3%9Cml%C3%A4ut%C3%B6/")))

(test "parsing uri with four, three and two byte-sized utf-8 characters."
      assert (equal? "/🂱↯◌͕/"
                     (parse-URI "file:///%F0%9F%82%B1%E2%86%AF%E2%97%8C%CD%95/")))

(test "byte length in utf-8"
      assert (equal? 2 (byte-length "ʀ"))
      assert (equal? 3 (byte-length "↯"))
      assert (equal? 4 (byte-length "🂱"))
      assert (equal? 5 (byte-length "hello"))
      assert (equal? 9 (byte-length "🂱hello")))

(test "byte length in utf-16"
      assert (equal? 2 (byte-length "ʀ" 1))
      assert (equal? 2 (byte-length "↯" 1))
      assert (equal? 4 (byte-length "🂱" 1))
      assert (equal? 10 (byte-length "hello" 1))
      assert (equal? 14 (byte-length "🂱hello" 1)))

(test "code point to utf-8 bytes"
      assert (equal? 5 (code-point->utf-8-length "hello" 5))
      assert (equal? 4 (code-point->utf-8-length "🂱hello" 1))
      assert (equal? 5 (code-point->utf-8-length "🂱hello" 2))
      assert (equal? 9 (code-point->utf-8-length "🂱hello" 6)))

(test "code point to utf-16 bytes"
      assert (equal? 5 (code-point->utf-16-length "hello" 5))
      assert (equal? 2 (code-point->utf-16-length "🂱hello" 1))
      assert (equal? 3 (code-point->utf-16-length "🂱hello" 2))
      assert (equal? 7 (code-point->utf-16-length "🂱hello" 6)))

(define test-workspaces
  (vector->workspace '#((("name" "workspace-title0") ("uri" . "my-uri"))
                      (("name" "workspace-title1") ("uri" . "my-uri")))))

(test "converting workspace params"
      assert (equal? #t (lsp-workspace? (car test-workspaces)))
      assert (equal? 2 (length test-workspaces))
      assert (equal? 'workspace-folder (lsp-workspace-kind (car test-workspaces))))

(test "list manipulations"
      assert (equal? '(the last item) (remove '(not the last item) 'not))
      assert (equal? '(item) (remove '(not the last item) 'not 3))
      assert (equal? '(not the last) (remove '(not the last item) 'item))
      assert (equal? '(not the item) (remove '(not the last item) 'last))
      assert (equal? '(not the) (remove '(not the last item) 'last 2))
      )
