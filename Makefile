#
#    This file is part of zamba.
#
#   zamba is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#   zamba is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License along with zamba. If not, see <https://www.gnu.org/licenses/>.
#
# buildfile for the zamba language server.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-lsqlite3 -ltree-sitter "
OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

LOAD_LIBS=sqlite3-binding.o1 ts-binding.o1
LANG=$(patsubst lib/lang/%/,%,$(wildcard lib/lang/*/))

BIN_DIR=$(PWD)/bin
MKDIR=mkdir -p

export BIN_DIR

.PHONY: langs test

all: deps
	$(GSC) -o bin/zamba -exe src/zamba.scm
	make clean

# needed for executing in the interpreter
deps: libs langs

libs: $(LOAD_LIBS)

langs: .bin-dir
	for l in $(LANG) ; do \
		$(MAKE) -o $(BIN_DIR)/ -C "lib/lang/$$l" ; \
	done

%.o1: .bin-dir
	$(MAKE) -o $(BIN_DIR)/ -C "lib/gambit-$*"

.bin-dir:
	@read -p "Libraries shall be located in the LD_LIBRARY_PATH. Continue with any key." key
	if [ ! -d $(BIN_DIR) ]; then $(MKDIR) $(BIN_DIR); fi

test:
	./lib/ensure/runner.scm \
		test/lib/json.scm \
		test/src/cli.scm \
		test/src/config.scm \
		test/src/cache.scm \
		test/src/lsp-lifecycle.scm \
		test/src/util.scm \
		test/interaction/lifecycle.scm \
		test/interaction/text_document.scm \
		test/interaction/diagnostics.scm

distClean: clean
	$(RM) -r bin

clean:
	$(RM) src/*.c
	$(RM) src/*.o

# single loadable file..
#$(GSC) -c -o bin/ts-java-binding.c lib/lang/java/ts-java-binding.scm
#$(GSC) -link -flat -o bin/ts-java-binding.o1.c bin/ts-java-binding.c
#$(GSC) -cc-options "-D___DYNAMIC" -obj bin/ts-java-binding.c bin/ts-java-binding.o1.c
#gcc -shared bin/ts-java-binding.o bin/ts-java-binding.o1.o -o bin/ts-java-binding.o1 -ltree-sitter -ltree-sitter-java
# working loadable lib:
#$(GSC) -c -o bin/common.c lib/lang/common.scm
#$(GSC) -c -o bin/ts-java-binding.c lib/lang/java/ts-java-binding.scm
#$(GSC) -link -flat -o bin/ts-java-binding.o1.c bin/common.c bin/ts-java-binding.c
#$(GSC) -cc-options "-D___DYNAMIC" -obj bin/common.c bin/ts-java-binding.c bin/ts-java-binding.o1.c
#gcc -shared bin/common.o bin/ts-java-binding.o bin/ts-java-binding.o1.o -o bin/ts-java-binding.o1 -ltree-sitter -ltree-sitter-java
