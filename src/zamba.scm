#!/usr/bin/env gsi-script -:-8,f8

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    _____   _    __  __ ____    _         _     ____
;;   |__  /  / \  |  \/  | __ )  / \       | |   / ___|
;;     / /  / _ \ | |\/| |  _ \ / _ \ _____| |   \___ \
;;    / /_ / ___ \| |  | | |_) / ___ \_____| |___ ___) |
;;   /____/_/   \_\_|  |_|____/_/   \_\    |_____|____/
;;
;; an language agnostic lsp-server, implemented with gambit scheme.
;; the name comes from lambda and microsofts samba, since
;; the lsp protocol is defined by them.
;;
;; Additionally, the tree-sitter library used is also of an employee (by this time of writing) of microsoft.
;; Thanks to the person, called Max Brunsfeld, for creating tree sitter.
;;  checkout: https://github.com/maxbrunsfeld

;; Defined by JSON RPC
(define parse-error            -32700)
(define invalid-request        -32600)
(define method-not-found       -32601)
(define invalid-params         -32602)
(define internal-error         -32603)
(define server-error-start     -32099)
(define server-error-end       -32000)
(define server-not-initialized -32002)
(define unknown-error-code     -32001)

;; Defined by the language server protocol.
(define request-cancelled      -32800)
(define content-modified       -32801)

;; Defined by zamba: active states
(define server-shutdown-requested   0)
(define server-shutdown-unexpected  1)
(define server-initializing         2)
(define server-initialized          4)

;; Defined by zamba: error-codes
(define end-of-input-stream         5)
(define unexpected-result           6)

;; thread-safe status-code
(define status-code ((let ((im (make-mutex 'state)) (code (make-parameter server-not-initialized)))
                       (lambda value
                         (mutex-lock! im)
                         (let ((res (if (null? value) code (code (car value)))))
                           (mutex-unlock! im)
                           res)))))

(load (path-normalize (string-append (path-directory (this-source-file)) "../bin/ts-binding.o1")))
(load (path-normalize (string-append (path-directory (this-source-file)) "../bin/sqlite3-binding.o1")))

;; with macros first:
(include "tree-walker.scm")

(include "util.scm")
(include "config.scm")
(include "lsp/diagnostics.scm")
(include "cache.scm")
(include "request.scm")
(include "lsp/lifecycle.scm")
(include "lsp/workspace.scm")
(include "lsp/text_document.scm")
(include "lsp/declaration.scm")
(include "lsp/completion.scm")
(include "lsp/hover.scm")
(include "lsp/references.scm")
(include "lsp/implementation.scm")
(include "lsp/structs.scm")
(include "../lib/r5rs-json-library/json.scm")
(include "../lib/lang/common.scm")

(define supported-languages
  (map
    (lambda (lang)
      (let ((res lang)
            (binding-path
              (path-normalize (string-append (zamba-directory)
                                             "lib/lang/" lang))))
        (when (file-exists? binding-path)
          (load (path-normalize (string-append (zamba-directory) "bin/ts-" lang "-binding.o1")))
          (load (string-append binding-path "component.scm"))
          (sqlite3--bind! statement-save-language lang)
          (sqlite3--step! statement-save-language)
          (sqlite3--reset! statement-save-language)
          (sqlite3--bind! statement-query-language lang)
          (sqlite3--query statement-query-language
                          (lambda (x)
                            (set! res (cons lang (car x)))))
          (sqlite3--reset! statement-query-language)
          res)))
    '("java" "python" "c" "scheme")))

(print-log type: 4
           "supported languages: " supported-languages)

;; This thread group bundles all threads created within the language server.
;; Note, this exclude threads from libraries.
;; There are four kinds of threads:
;; 1. The main thread, called primoridal in gambit scheme.
;;    chomp s the string into messages.
;; 2. The stdout-owner (output-thread), synchronization and encode + stdout.
;; 3. The work-handler (worker-thread), which receives messages from the client.
;; 4. For each request, a worker is created, so that the work-handler can do it's business.
(define zamba-worker-thread-group (make-thread-group "zamba-worker-threads"))

(define output-file
  (open-output-file
    (list path: (string-append (log-directory) "zamba")
          append: #t
          create: 'maybe)))

(define output-thread #f)

(define worker-thread
  ;; this member needs to be on the top-level scope.
  (letrec ((parse
             (lambda ()
               (let ((msg (thread-receive)))
                 (with-input-from-string
                   msg
                   (lambda ()
                     (with-exception-catcher
                       (lambda (ex)
                         (log-exception! ex "error while processing a new message")
                         #f)
                       (lambda () (handle-request
                                    (json-decode (current-input-port))))))))
               (thread-yield!)
               (parse))))
    (make-thread parse 'work-handler zamba-worker-thread-group)))

(define (main . args)
  (letrec ((parse-config
             (lambda (flags)
               (cond
                 ((or (member "--help" flags)
                      (member  "-h" flags)
                      (member   "help" flags))
                  (parameterize ((current-output-port stdout)) (print-help)))
                 ((member "--repl" flags)
                  (if (null? (cdr (member "--repl" flags)))
                      (parameterize ((current-output-port stderr)) (invalid-usage "repl")))
                  (load (path-normalize (string-append (zamba-directory) "lib/gamrepl/gamrepl.scm")))
                  (with-exception-catcher
                    (lambda (ex)
                      (log-exception! ex "error when starting the repl!")
                      (window/showMessage type: 2 "REPL start did not succeed.")
                       #f)
                    (lambda ()
                      (let ((port (string->number (cadr (member "--repl" flags)))))
                        (gamrepl#server-start
                          port-settings:
                          (list
                            local-port-number: (if port port 0)
                            reuse-address: #t)
                          server-name: "zamba development-server"))
                      (uses-repl? #t)))
                  (parse-config (remove flags "--repl" 2)))
                 ((member "--omit-abstract-syntax-tree" flags)
                  (show-abstract-syntax-tree #f)
                  (parse-config (remove flags "--omit-abstract-syntax-tree")))
                 ((member "--debug" flags)
                  (debug #t)
                  (parse-config (remove flags "--debug")))
                 ((member "--tag-mode" flags)
                  (text-synchronization 0)
                  (parse-config (remove flags "--tag-mode")))
                 ((member "--disable-text-sync" flags)
                  (text-synchronization 0)
                  (parse-config (remove flags "--disable-text-sync")))
                 ((null? flags) '())
                 (else
                   (parameterize ((current-output-port stderr)) (invalid-usage flags))))))
           (print-help
             (lambda ()
               (print   "zamba - (language agnostic) language server (") (print version) (print ")") (newline)
               (println "MPLv2 - no warranty but much love")
               (println "")
               (println "Usage: zamba [options] ||    actual usage; started by lsp-client.")
               (println "       zamba [commands]      for one-time tasks.")
               (println "")
               (println "options:")
               (println " --debug                     local bindings will get written to a file")
               (println "                             database instead of in-memory. Creates a")
               (println "                             logging file in the default-temporary")
               (println "                             directory. Use while developing.")
               (println " --tag-mode ||")
               (println " --disable-text-sync         tells the language server client (editor)")
               (println "                             to not sync the file content( changes) when")
               (println "                             the opened files change. Instead: Read from")
               (println "                             from disc initially and imitate changes when")
               (println "                             saving a file. Requires the language server")
               (println "                             client to support the textDocument/didSave")
               (println "                             capability to work reliably. Saves resources")
               (println "                             and aims to replace classic tag-generating")
               (println "                             programs.")
               (println " --omit-abstract-syntax-tree when invoking textDocument/hover the abstract")
               (println "                             syntax tree from the current cursor location")
               (println "                             is omitted. This shrinks the output of the")
               (println "                             hover functionality in case the editor limits")
               (println "                             the available vertical content.")
               (println " --repl port                 hosts a repl on the provided port to connect")
               (println "                             to the scheme runtime of zamba itself.")
               (println "                             Full introspection of the instance and exe-")
               (println "                             cuting scheme code. Connect via `nc`.")
               (println "                             Security risk; Should be an compile-time ")
               (println "                             option to prohibiting the usage. Sorry \\o/")
               (println "")
               (println "commands:")
               (println " -h     ||")
               (println " --help ||")
               (println " help                        emit this message.")
               (println "")
               (println "zamba is a language server and therefore does not provide interactive")
               (println "commands besides listed. You have to kill it via a (term-)signal if")
               (println "started in such way.")
               (println "")
               (println "https://zamba.hagiga.de")
               (exit 0)))
           (invalid-usage
             (lambda (arg)
               (print   "zamba - (language agnostic) language server (") (print version) (print ")") (newline)
               (println "MPLv2 - no warranty but much love")
               (println "")
               (cond
                 ((and (string? arg)
                       (string=? arg "repl"))
                  (println "Missing tcp-port when invoking zamba."))
                 (else
                   (print "Bad command line usage with: ")
                   (for-each (lambda (it) (print it) (print " "))
                             arg)
                   (newline)))
               (println "Get help by executing:")
               (println "    zamba --help")
               (println "")
               (exit 2)))

           (output-loop
             (lambda ()
               ;; this could use format (srfi-28)
               (let* ((item (thread-receive))
                      (message (call-with-output-string
                                 (lambda (p)
                                   (json-encode item p))))
                      (msg (string-append
                             "Content-Length: " (number->string (string-length message))
                             (list->string '(#\return #\newline))
                             (list->string '(#\return #\newline))
                             message)))
                 (write-substring msg 0 (string-length msg) stdout)
                 (force-output stdout))
               (if (= server-shutdown-requested (status-code))
                   'end
                   (output-loop))))

           (start-up
             (lambda ()
               (thread-start! output-thread)
               (thread-start! worker-thread)
               (with-output-to-port stderr read-stdin)))

           (read-stdin
             (lambda ()
               (let ((line (read-line stdin #\newline #t)))
                 (with-exception-catcher
                   (lambda (ex)
                     (log-exception! ex "on STDIN: Rave on.")
                     (if (eq? line #!eof)
                         (exit end-of-input-stream))
                     #f)
                   (lambda ()
                     (if (and (string? line)
                              (> (string-length line) 15)
                              (string=? (substring line 0 15) "Content-Length:"))
                         (let* ((len (string->number
                                       (list->string
                                         (reverse
                                           (fold (lambda (c acc)
                                                   (if (and (> (char->integer c) 47)
                                                            (< (char->integer c) 58))
                                                       (cons c acc) acc))
                                                 '() (string->list line))))))
                                (request (make-string len)))
                           (let drop ((drpdrp (read-line stdin #\newline #t)))
                             (if (not (equal? drpdrp "\r\n"))
                                 (drop (read-line stdin #\newline #t))))
                           (read-substring request 0 len stdin (- len 1))
                           (thread-send worker-thread request)
                           (thread-resume! worker-thread)))))
                 (read-stdin)))))

    (set! output-thread (make-thread output-loop 'stdout-owner zamba-worker-thread-group))
    (thread-specific-set! (current-thread) "STDIN")
    (thread-specific-set! output-thread "STDOUT")

    (parameterize ((current-output-port output-file)
                   (current-error-port output-file))

      (parse-config args)
      (print-log type: 4 "parsed program arguments: " args)

      (print-log type: 3 "debug-mode: " (debug))
      (print-log type: 3 "zamba-directory: " (zamba-directory))
      (print-log type: 3 "cache-dir " (cache-dir))
      (print-log type: 3 "current working directory: " (current-directory))
      (print-log type: 3 "current version: " version)
      (print-log type: 3 "targeted lsp-version: 3.15.0." "The protocol should be backwards compatible.")

      (cond
        ((member "--tcp" args)
         (let* ((server (open-tcp-server (list reuse-address: #t
                                               local-port-number: (string->number (cadr (member "--tcp" args))))))
                (socket (read server)))
           (parameterize ((stdin socket)
                          (stdout socket)
                          (stderr socket))
             (start-up)
             (close-port socket))))
        (else
          (start-up))))))

(define version (with-input-from-process (list path: "git" arguments: '("describe")) read-line))
