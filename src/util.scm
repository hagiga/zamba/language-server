;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Utility functions.

(define (read-file-relative-to file path)
  (call-with-input-file
    (path-normalize
      (string-append (path-directory file)
                     path))
    read-large-chunk))

(define (modification-time? file)
  (inexact->exact
    (floor
      (time->seconds
        (if (file-exists? file)
            (file-info-last-modification-time
              (file-info file))
            (current-time))))))

(define (word-at node doc)
  (read-node (lsp-document-content doc) node))

(define (read-node content node)
  ;; spanning over multiple rows supported.
  (letrec* ((start-p (ts--node-start-point node))
            (end-p (ts--node-end-point node))

            (start-row (ts--point-row start-p))
            (start-col (ts--point-col start-p))

            (end-row (ts--point-row end-p))
            (end-col (ts--point-col end-p))
            (append-text
              (lambda (to-append i-row)
                (cond
                  ((= i-row end-row)
                   (string-append
                     to-append
                     (substring
                       (vector-ref content i-row)
                       (if (= i-row start-row) start-col 0)
                       (if (= i-row end-row) end-col (string-length (vector-ref content i-row))))))
                  ((< i-row end-row)
                   (append-text
                     (string-append
                       to-append
                       (if (= i-row start-row)
                           (substring
                             (vector-ref content i-row)
                             start-col
                             (string-length (vector-ref content i-row)))
                           (vector-ref content i-row)))
                     (+ i-row 1)))
                  (else (error "unpredictable error state"))))))
    (when (= end-col 0)
      (set! end-row (- end-row 1))
      (set! end-col (string-length (vector-ref content end-row))))

    (append-text "" start-row)))

;; TODO: curry all repeatedly called procedures.
(define (parse-URI raw)
  ;;  foo://example.com:8042/over/there?name=ferret#nose
  ;;  \_/   \______________/\_________/ \_________/ \__/
  ;;   |           |            |            |        |
  ;;scheme     authority       path        query   fragment
  ;;   |   _____________________|__
  ;;  / \ /                        \
  ;;  urn:example:animal:ferret:nose
  (letrec ((hex->int (lambda (c d)
                       (string->number
                                  (string-append "#x" (list->string (list (integer->char c) (integer->char d)))))))

           (read-uri
             (lambda (uri lst)
               (if (null? lst)
                 (list->string (map integer->char (reverse uri)))
                 (case (car lst)
                   ((37)
                    (let ((digit-1 (hex->int (cadr lst) (caddr lst))))
                      (cond
                        ((< digit-1 #x80) (read-uri (cons digit-1
                                                          uri)
                                                    (cdddr lst)))
                        ((< digit-1 #xE0) (read-uri (cons (bitwise-xor (fxarithmetic-shift (bitwise-and digit-1 #b00011111) 6)
                                                                       (bitwise-and (hex->int (car (cddddr lst)) (cadr (cddddr lst))) #b00111111))
                                                          uri)
                                                    (cdddr (cdddr lst))))
                        ((< digit-1 #xF0) (read-uri (cons (bitwise-xor (fxarithmetic-shift (bitwise-and digit-1 #b00001111) 12)
                                                                       (fxarithmetic-shift (bitwise-and (hex->int (car (cddddr lst)) (cadr (cddddr lst))) #b00111111) 6)
                                                                       (bitwise-and (hex->int (car (cdddr (cddddr lst))) (cadr (cdddr (cddddr lst)))) #b00111111))
                                                          uri)
                                                    (cdddr (cdddr (cdddr lst)))))
                        ((< digit-1 #xF8) (read-uri (cons (bitwise-xor (fxarithmetic-shift (bitwise-and digit-1 #b00000111) 18)
                                                                       (fxarithmetic-shift (bitwise-and (hex->int (car (cddddr lst)) (cadr (cddddr lst))) #b00111111) 12)
                                                                       (fxarithmetic-shift (bitwise-and (hex->int (car (cdddr (cddddr lst))) (cadr (cdddr (cddddr lst)))) #b00111111) 6)
                                                                       (bitwise-and (hex->int (car (cdddr (cdddr (cddddr lst)))) (cadr (cdddr (cdddr (cddddr lst))))) #b00111111))
                                                          uri)
                                                    (cdddr (cdddr (cdddr (cdddr lst)))))))))
                   (else (read-uri (cons (car lst) uri) (cdr lst))))))))
    (read-uri '() (map char->integer (string->list (substring raw 7 (string-length raw)))))))

(define default-encoding (make-parameter 0))
(define (byte-length s #!optional (enc (default-encoding)))
  (letrec ((len (string-length s))
           (acc (lambda (op #!optional (size 0) (p 0))
                  (if (= p len) size
                      (acc op (+ size (op (string-ref s p))) (+ p 1))))))
    (cond
    ((= enc 0) (acc  char->UTF-8-byte-size))
    ((= enc 1) (acc char->UTF-16-byte-size))
    (else (error "unknown encoding request when calculating byte length of string.")))))

(define char->UTF-8-byte-size
  (lambda (c #!optional (c-int (char->integer c)))
    (cond
      ((< c-int     #x80) 1)
      ((< c-int    #x800) 2)
      ((< c-int  #x10000) 3)
      ((< c-int #x110000) 4)
      (else (raise "invalid character")))))

(define char->UTF-16-byte-size
  (lambda (c) ; attribution: https://stackoverflow.com/a/66679955/6783797
    (if (< (char->integer c) #x10000) 2 4)))

;; @DEPRECATED
(define (code-point->utf-8-length s end)
  (letrec ((acc (lambda (size p)
                  (if (= p end) size
                      (acc (+ size (char->UTF-8-byte-size (string-ref s p)))
                           (+ p 1))))))
    (acc 0 0)))

;; @DEPRECATED
(define (code-point->utf-16-length s end)
  (letrec ((acc (lambda (size p)
                  (if (= p end) size
                      (acc (+ size
                              (/ (char->UTF-16-byte-size (string-ref s p)) 2))
                           (+ p 1))))))
    (acc 0 0)))

(define (vector->workspace ws)
  (map (lambda (it) (make-lsp-workspace (cdr (assoc "uri" it)) 'workspace-folder))
       (vector->list ws)))

; @DEPRECATED
(define (string-compare? str for)
  (if (and (string? str) (string? for))
      (if (equal? str for)
          #t
          (if (> (string-length str) (string-length for))
              (let iter ((p 0))
                (if (<= p (string-length str))
                    (let match ((q 0))
                      (cond
                        ((= (string-length for) q) #t)
                        ((= (+ p q) (string-length str)) #f)
                        ((eqv?
                           (string-ref str (+ p q))
                           (string-ref for q)) (match (+ 1 q)))
                        (else (if (> q 0)
                                  (iter (+ p q))
                                  (iter (+ p 1))))))
                    #f))
              #f))))

(define (remove lst item #!optional (consume 1))
  (cond ((null? lst)  '())
        ((equal? item (car lst)) (list-tail lst consume))
        (else         (cons (car lst)
                            (remove (cdr lst) item consume)))))

(define (lowercase str)
  (list->string
      (map char-downcase
           (string->list str))))

(define log-exception!
  (lambda (ex . args)
    (print-log type: 1
               "received an " (object->string ex)
               (if (null? args) "" args)
               (cond
                 ((error-exception? ex)
                  (list
                    "message: " (error-exception-message ex)
                    "parameters: " (error-exception-parameters ex)))
                 ((nonprocedure-operator-exception? ex)
                  (list
                    "operator: " (nonprocedure-operator-exception-operator ex)
                    "arguments: " (nonprocedure-operator-exception-arguments ex)))
                 ((wrong-number-of-arguments-exception? ex)
                  (list
                    "procedure: " (wrong-number-of-arguments-exception-procedure ex)
                    "arguments: " (wrong-number-of-arguments-exception-arguments ex)))
                 ((range-exception? ex)
                  (list
                    "procedure: " (range-exception-procedure ex)
                    "arguments: " (range-exception-arguments ex)))
                 ((unbound-global-exception? ex)
                  (list
                    "variable: " (unbound-global-exception-variable ex)))
                 ((cfun-conversion-exception? ex)
                  (list
                    "procedure: " (cfun-conversion-exception-procedure ex)
                    "arguments: " (cfun-conversion-exception-arguments ex)
                    "message: " (cfun-conversion-exception-message ex)))
                 ((type-exception? ex)
                  (list
                    "procedure: " (type-exception-procedure ex)
                    "arguments: " (type-exception-arguments ex)))
                 (else (with-output-to-string
                         (lambda () (display-exception ex))))))))

;; All logs are done twice: Write to a std-file and tell the client.
;; Note that the client does not receive all messages.
(define (print-log #!key (type 5) #!rest args)
  (let ((print-out
          (lambda (type)
            ;message-types: 1 (error), 2 (warning), 3 (info), 4 (log)
            (thread-send
              output-thread
              `(("method" . "window/logMessage")
                ("jsonrpc" . "2.0")
                ("params"
                 ("type" . ,type)
                 ("message" . ,(object->string args))))))))

    (when (and (debug)
               ;; magic number 6: development purpose. shortens the log
               (<= type 6))
      (display "PID: "             output-file)
      (display (process-pid)       output-file)
      (display " T: "        output-file)
      (display (exact->inexact
                 (/ (inexact->exact (floor (* 1000 (real-time))))
                    1000))         output-file)
      (display (case type
                 ((1)  " ERROR ")
                 ((2)  " WARN  ")
                 ((3)  " INFO  ")
                 (else " DEBUG ")) output-file)
      (display (thread-name
                 (current-thread)) output-file)
      (display #\:                 output-file)
      (display #\space             output-file)
      (display (thread-specific
                 (current-thread)) output-file)
      (display #\space             output-file)
      (display #\-                 output-file)
      (display #\space             output-file)
      (display args                output-file)
      (newline                     output-file)
      (force-output                output-file))

    (when (and (< type 2)
               (not (equal? "off" (trace-lvl))))
      (println port: stderr (object->string args))
      (force-output  stderr))

  ;; lsp-logging:
  (if (> (status-code) server-initialized)
    (cond
      ((equal? "off" (trace-lvl)) 'silent)
      ((equal? "messages" (trace-lvl)) (if (= 2 type) (print-out type)))
      ;; verbose, but still omitting things.
      (else (if (and (> type 1) (< type 5)) (print-out type)))))))
