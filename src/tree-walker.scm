;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-syntax walk-tree
  (syntax-rules ()
    ((_ (cursor start end) start-node predicate thunk ...)
     (letrec* ((walk-up
                 (lambda ()
                   (if (ts--cursor-parent! cursor)
                     (if (ts--cursor-sibling-next! cursor)
                       (walk-tree)
                       (walk-up))
                     #f)))
               (walk-tree
                 (lambda ()
                   (let ((start (ts--node-start-point (ts--cursor-current-node cursor)))
                         (end (ts--node-end-point (ts--cursor-current-node cursor))))
                     (if predicate
                       (begin thunk ...)
                       (if (ts--cursor-child! cursor)
                         (walk-tree)
                         (if (ts--cursor-sibling-next! cursor)
                           (walk-tree)
                           (walk-up)))))))
               (cursor (ts--cursor-new start-node))
               (res (walk-tree)))
       (ts--cursor-free! cursor)
       res))))

(define-syntax query-db
  (syntax-rules ()
    ((_ sql args ...)
     (let ((res '())
           (stmt (if (string? sql) (sqlite3--prepare database sql) sql)))
       (sqlite3--bind! stmt args ...)
       (sqlite3--query stmt (lambda (x) (set! res (cons x res))))
       (if (string? sql)
           (sqlite3--finalize! stmt)
           (sqlite3--reset! stmt))
       res))))

(define (query--db sql . args)
  (let ((res '())
        (stmt (if (string? sql) (sqlite3--prepare database sql) sql)))
    (print-log "args: " (append (list stmt) args))
    (apply
      sqlite3--bind!
      (append (list stmt) args))
    (sqlite3--query stmt (lambda (x)
                           (print-log "res: " x)
                           (set! res (cons x res))))
    (if (string? sql)
        (sqlite3--finalize! stmt)
        (sqlite3--reset! stmt))
    res))

;; TODO: this needs to be either more dynamic or restrictive.
(define (lang-function symbol #!key (lang #f) (doc #f))
  (with-exception-catcher
    (lambda (ex)
      (log-exception! ex " when calling into a language component: " symbol lang doc)
      (lambda args "not implemented in this language."))
    (lambda ()
      (eval
        (string->symbol
          (string-append
            "ts--"
            (if lang lang
                (if (lsp-document? doc)
                    (lsp-document-language doc)
                    (error "invalid invocation")))
            "#" (symbol->string symbol)))))))
