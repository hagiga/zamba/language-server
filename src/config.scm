;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; for CLIENT-/SERVER-CAPABILITIES, please look at lifecycle.scm.
;;
;; Within this file, preparation of parameters is done.
;;
;; directories are used to persist data.
;;
;; the cache-dir holds database the bindings are stored in.
;; the config-dir holds a database as well, but this is used
;; for things I do not know about.
;;

(define stdin (current-input-port))
(define stdout (current-output-port))
(define stderr (current-error-port))

(define line-ending (make-parameter (list->string '(#\newline))))
(define directory-separator (make-parameter "/"))
(define zamba-directory (make-parameter (path-normalize (string-append (path-directory (this-source-file)) "../"))))

(define uses-repl? (make-parameter #f))

;; TODO: Let the user deactivate it via start-up flag.
(define diagnostics-enabled (make-parameter #t))

(define cache-dir
  (make-parameter
    (string-append
      (path-strip-trailing-directory-separator
        (if (char=? #\/ (string-ref (current-directory) 0))
            (path-expand "~/.cache/zamba")
            (string-append (getenv "TEMP") "zamba")))
      (directory-separator))
    (lambda (x)
      (if (string? x)
          (let ((path (path-expand x)))
            (if (not (file-exists? path)) (create-directory path))
            path)
          (raise "invalid cache-dir")))))

(define config-dir
  (make-parameter
    (string-append
      (path-strip-trailing-directory-separator
        (if (char=? #\/ (string-ref (current-directory) 0))
            (path-expand "~/.config/zamba")
            (string-append (getenv "LOCALAPPDATA") "zamba")))
      (directory-separator))
    (lambda (x)
      (if (string? x)
          (let ((path (path-expand x)))
            (if (not (file-exists? path)) (create-directory path))
            path)
          (raise "invalid config-dir")))))

(define log-directory
  (make-parameter
    (string-append
      (path-strip-trailing-directory-separator
        (if (char=? #\/ (string-ref (current-directory) 0))
            "/tmp"
            (getenv "TEMP")))
        (directory-separator))
    (lambda (x)
      (if (string? x)
          (path-expand x)
          (raise "invalid logging-directory")))))

;; query-cursor match limit.
;; refer to ts-query-execute!
(define match-limit (make-parameter 8000))

;; include a part of the abstract syntax tree when hover information is displayed.
(define show-abstract-syntax-tree (make-parameter #t))

;; how many emits within 100%?
;; on each omit, gc is forced.
(define progress-report-interval (make-parameter 0.1))

(define trace-lvl (make-parameter "off"))
(define debug (make-parameter #f))

(define watch-zamba-configuration-file #f)
(letrec
  ((project-configuration-file #f)
   (last-configuration-change #f)
   (now
     (lambda ()
       (time->seconds
         (file-info-last-modification-time
           (file-info project-configuration-file)))))
   (find-project-configuration-file
     (lambda (path)
       (when (not (string=? "" path))
         (let ((break (call/cc (lambda (x) x))))
           (if (procedure? break)
               (for-each
                 (lambda (it)
                   (when (and (file-exists? it)
                              (eq? (file-type it) 'regular))
                     (if (string=? ".zamba" it)
                         (break (string-append path it)))))
                 (directory-files
                   (list path: path
                         ignore-hidden: 'dot-and-dot-dot)))
               (begin
                 (print-log type: 4 "found config-file")
                 (set! project-configuration-file break)
                 (set! last-configuration-change (now)))))
         (if (not project-configuration-file)
             (find-project-configuration-file
               (path-directory
                 (path-strip-trailing-directory-separator
                   path))))))))
  (find-project-configuration-file (current-directory))
  (print-log type: 4
             "project specific config file: "
             project-configuration-file)

  (set! watch-zamba-configuration-file
    (lambda ()
      (cond
        ((and project-configuration-file
              (file-exists? project-configuration-file)
              (number? last-configuration-change)
              (< last-configuration-change
                 (now)))
         (set! last-configuration-change (now))
         (workspace-update-configuration-file project-configuration-file)
         'reread-config-file)
        ((and project-configuration-file
              (file-exists? project-configuration-file)
              (number? last-configuration-change))
         project-configuration-file)
        (else 'no-config-file)))))
