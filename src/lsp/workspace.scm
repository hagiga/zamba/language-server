;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; list of lsp-workspaces (structs.scm)
;;
;; The currently used kinds are:
;; 'workspace-root if there were no workspace-folders when initializing zamba
;; 'workspace-folder if there were such
;; 'project-specific from the file `.zamba` (project root)
(define workspaces '())

(define workspace-discovery-mutex (make-mutex 'directory-iterator))

(define (file-is-indexed? file)
  (let ((uri (string-append "file://" file))
        (mod-time (modification-time? file))
        (res #f))
    (sqlite3--bind! statement-query-file-indexed uri)
    (sqlite3--query statement-query-file-indexed
                    (lambda (x)
                      (set! res (<= mod-time (cadr x)))))
    (sqlite3--reset! statement-query-file-indexed)
    res))

(define (init-workspaces params)
  (let retry ((workspace-folders (assoc "workspaceFolders" params)))
    (cond
      ((and (workspace-folders-resolved)
            workspace-folders
            (vector? (cdr workspace-folders)))
       (if (= 0 (vector-length (cdr workspace-folders)))
           (begin
             (print-log type: 2
                        "User-provided workspaceFolders are empty;"
                        "Falling back to rootUri/rootPath/current-working-directory.")
             (retry #f))
           (set! workspaces (vector->workspace (cdr workspace-folders)))))
      (else
        (set! workspaces
          (list
            (make-lsp-workspace
              (cond
                ((assoc "rootUri" params) (cdr (assoc "rootUri" params)))
                ((assoc "rootPath" params)
                 (if (string>=? (cdr (assoc "rootPath" params)) "file://")
                     (cdr (assoc "rootPath" params))
                     (string-append "file://" (cdr (assoc "rootPath" params)))))
                (else
                  (print-log type: 2
                             "Client did not report necessary informations for workspaces or root paths."
                             "Falling back to the current-working-directory.")
                  (string-append "file://" (current-directory))))
              'workspace-root))))))

  (print-log type: 4
             "finished with: " workspaces)

  ;; not necessary yet.
  ;(if (workspace-configuration)
  ;    (send-request "workspace/configuration" '("params" ("key" . "value"))))

  ;; TODO: Iff the server is shared across projects in the future,
  ;;       one could request the workspaceFolders, since the initialize method would
  ;;       not be called again; But this is not in the protocol (appears to be supported by vim-lsc, though!).
  ;;       FIXME: the zamba-configuration-file would not support this use case yet.
  ;(if (workspace-folders-resolved)
  ;    (send-request "workspace/workspaceFolders"))

  (if (eq? 'no-config-file (watch-zamba-configuration-file))
      ;; start discovering without respecting config file
      (workspace-folders-invalidated!))
  'done)

(define (workspace-update-configuration-file file)
  (set! workspaces
    (append
      (fold (lambda (it ws)
              (if (eq? (lsp-workspace-kind it) 'project-specific)
                  (begin ;; invalidate previously loaded files by
                         ;; deleting the accessible-mark ones
                    (sqlite3--bind! statement-release-doc
                                    (lsp-workspace-uri it))
                    (sqlite3--step! statement-release-doc)
                    (sqlite3--reset! statement-release-doc)
                    ws)
                  (cons it ws)))
            '()
            workspaces)
      (fold (lambda (path ws)
              (if (file-exists? path)
                  (cons (make-lsp-workspace
                          (string-append "file://" path)
                          'project-specific) ws)
                  ws))
            '()
            (call-with-input-file
              file
              (lambda (p)
                (read-all p (lambda (p) (read-line p))))))))

  (print-log type: 4 "workspaces after joining: " workspaces)

  (workspace-folders-invalidated!))

;; Callback to the request.
(define (workspace/workspaceFolders . params)
  (print-log type: 1 "We never made a request to the method workspace/workspaceFolders"))

(define (workspace/didChangeWorkspaceFolders params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didChangeWorkspaceFolders")
      (begin
        (for-each
          (lambda (event)
            (let ((changes (cdr event)))
              (if (= 1 (vector-length changes))
                  (if (vector? (vector-ref changes 0))
                      'no-workspace-table-in-event
                      (cond
                        ((string=? (car event) "added")   (set! workspaces (append (vector->workspace changes)
                                                                                   workspaces)))
                        ((string=? (car event) "removed") (set! workspaces (remove workspaces
                                                                                   (vector->workspace changes)))))))))
          (cdr (assoc "event" params)))
        ;; start iterating workspace folders
        (workspace-folders-invalidated!))))

;; Callback to the request.
(define (workspace/configuration params)
  (print-log type: 1 "workspace/configuration was not issued!" params)
  (workspace/didChangeConfiguration params))

;; DOES NOT WORK CONSISTENTLY ACROSS CLIENTS
;; Clients may always answer with all settings.
;; Once the server is in daily use, this should be FIXME (read revisited).
;; Has to be registered.
(define (workspace/didChangeConfiguration params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didChangeWorkspaceFolders")
      (begin
        ;; even though this method is not in actual use yet,
        ;; it showcases the possibilty to process language-specific configuration.
        (print-log type: 4 "didChangeConfiguration" params)
        (for-each
          (lambda (pair)
            (print-log "configuration-pair:" pair)
            ((lang-function 'workspace-configuration lang: (car pair)) params))
          known-languages)

        (workspace-folders-invalidated!))))

;; is called manually after a change of the configuration or the folders.
(define (workspace-folders-invalidated! . args)
  (when (and workspaces
          (mutex-lock! workspace-discovery-mutex 0))
    (print-log type: 3 "workspaces:" workspaces)
    (letrec* ((discover
                (lambda (path lst)
                  (if (file-exists? path)
                      (fold
                        (lambda (file acc)
                          (let ((f (string-append (path-directory path) file)))
                            (if (eq? (file-type f) 'directory)
                                (discover (string-append f (directory-separator)) acc)
                                (if (assoc (path-extension file) languages-by-suffix)
                                    (if (member f acc)
                                        acc
                                        (cons f acc))
                                    acc))))
                        lst
                        (directory-files path))
                      lst)))

              (parse-file!
                (lambda (path lang)
                  (let* ((content (call-with-input-file path read-content))
                         (uri (string-append "file://" path))
                         (tree (ts-parse lang content))
                         (doc (make-lsp-document 'opened-implicitly lang tree)))
                    (make-bindings uri doc content)
                    (ts--tree-free! tree))))

              (files
                (fold
                  (lambda (ws acc)
                    (print-log type: 4 "walking workspace-directory: " (lsp-workspace-uri ws))
                    (discover
                      (path-expand
                        (string-append (path-strip-trailing-directory-separator
                                         (parse-URI (lsp-workspace-uri ws))) (directory-separator))) acc))
                  '()
                  workspaces))
              (discovered-files (length files))

              (ins (make-string 0))
              (old ##repl-output-port)
              (in (open-string ins)))

      (set! ##repl-output-port (lambda () in))
      (when (> discovered-files 0)
        (##gc-report-set! #t)
        (let ((count 0)
              (modulo-ct (floor (* discovered-files (progress-report-interval))))
              (progress (make-progress (string-append (number->string discovered-files) " files in workspaceFolders."))))
          (if (< modulo-ct 1) (set! modulo-ct 1))
          (for-each
            (lambda (f)
              (let ((lang (cdr (assoc (path-extension f) languages-by-suffix))))
                (when (not (file-is-indexed? f))
                  (with-exception-catcher
                    (lambda (ex)
                      (log-exception! ex "workspaces: file discovery. file: " f)
                      #f)
                    (lambda ()
                      (parse-file! f lang))))

                ;; add to accessible documents:
                ;; FIXME: dont save as uri: Huge FIXME
                (sqlite3--bind! statement-query-file-indexed (string-append "file://" f))
                (sqlite3--query statement-query-file-indexed
                                (lambda (x)
                                  (sqlite3--bind! statement-save-opened
                                                  (car x) (cdr (assoc lang supported-languages)))
                                  (sqlite3--step! statement-save-opened)
                                  (sqlite3--reset! statement-save-opened)))
                (sqlite3--reset! statement-query-file-indexed))
              (set! count (+ count 1))
              (when (= 0 (modulo count modulo-ct))
                (progress 'report (string-append "#indexed files: "
                                                 (number->string discovered-files) "/"
                                                 (number->string count))
                          (/ count discovered-files))))
            files)
          (progress 'end "indexed all files")
          (time (##gc) in)
          (print-log "gc-replast:" (get-output-string in))
          (##gc-report-set! #f)
          (set! ##repl-output-port old)
          (sqlite3--exec database "ANALYZE")
          (window/showMessage "accessible documents for zamba: " (object->string (caar (query-db statement-query-open-docs))))
          (mutex-unlock! workspace-discovery-mutex))))))

;; Callback to the request
;; null corresponds to all bindings.
(define (workspace/symbol . params)
  (print-log type: 1 "not implemented yet - request an symbol:" params))


(define (workspace/didChangeWatchedFiles . params)
  ;; has to be registered beforehand
  ;; notifies about create / change / delete
  (print-log type: 1 "not implemented yet - changes on files:" params))
