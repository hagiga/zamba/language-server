;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; typedefs
(define-structure lsp-response res)
(define-structure lsp-error code msg)

(define-structure lsp-workspace
                  id: f9a110c6-cdcc-4268-9dbe-34cc5b97a915
                  uri
                  kind)

(define-structure lsp-document
                  id: lsp-document-be577d43-af05-4b8b-832f-53568cd721cb
                  version                       ;; used for incremental edits
                  language                      ;; language-string
                  tree
                  (identifier        init: #f)  ;; language specific
                  (content           init: #f))

;; Overwrites
;(letrec ((tree-set lsp-document-tree-set!)
;         (tree-get lsp-document-tree)
;         (tree-get-renew (lambda (obj) ; only when tree is fresh
;                           (set! lsp-document-tree
;                             (lambda (obj) ; afterwards, only give copies
;                               (ts--tree-copy! (tree-get obj))))
;                           (tree-get obj))))
;
;  (set! lsp-document-tree-set!
;    (lambda (obj set)
;      ;; renew the getter-overwrite
;      (set! lsp-document-tree tree-get-renew)
;      (tree-set obj set)))
;
;  (set! lsp-document-tree tree-get-renew))
