;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Jumping to a declaration and to a definition overlaps.
;; A declaration is always the place where the _symbol_ is defined.
;; A defintion is the place where the _symbol_ gets an value assigned,
;; which can overlap as noted.
;;
;; This method could hold a jumpl-list like structure, so that
;; methods like textDocument/hover can benefit from this.
;; If you have an suggestion, feel free to present it as an feature request.
;; The user intent needs to be exactly specified, though.
;;
;; TODO: Respect dependencies.
(define (textDocument/declaration params)
  (if (not params) (make-lsp-error invalid-params "no params provided to declaration")
      (letrec* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
                (doc (table-ref documents uri))
                (pos (cdr (assoc "position" params)))
                (col (cdr (assoc "character" pos)))
                (row (cdr (assoc "line" pos)))
                (declarations
                  (map (lambda (it)
                         ;; convert from bytes to point.. thats bad..
                         ;; but we cant shift row + col.
                         ;; therefore:
                         ;; TODO: implement MRU's into documents => run clean up on schedule.
                         (let ((node (ts--node-descendant-for-byte-range
                                       (ts--tree-root-node
                                         (lsp-document-tree (document-by-uri (car it))))
                                       (list-ref it 4)
                                       (list-ref it 5))))
                           `(("uri"   . ,(car it))
                             ("range" . (("start" . (("line"      . ,(ts--point-row (ts--node-start-point node)))
                                                     ("character" . ,(ts--point-col (ts--node-start-point node)))))
                                         ("end"   . (("line"      . ,(ts--point-row (ts--node-end-point node)))
                                                     ("character" . ,(ts--point-col (ts--node-end-point node))))))))))
                       (find-declaration-nodes uri doc row col))))
        (make-lsp-response (if (null? declarations) '() (list->vector declarations))))))

;; TODO: filtering
;; for each declaration, search a query within the file,
;; iff the position was an assignment
(define (textDocument/definition params)
  (textDocument/declaration params))

(define (find-declaration-nodes uri doc row col
                                #!key
                                (start-node
                                  (ts--tree-root-node
                                    (lsp-document-tree doc))))
  (walk-tree (cursor start end)
    start-node
    (and (= row (ts--point-row start))
         (>= col (ts--point-col start))
         (< col (ts--point-col end))
         (= 0 (ts--node-child-count
                (ts--cursor-current-node cursor))))
    (let* ((node-hovered (ts--cursor-current-node cursor))
           (string-hovered (read-node (lsp-document-content doc) node-hovered))
           (_(print-log "hovered node: " string-hovered))
           (parent-scopes (eval (string->symbol (string-append "ts--" (lsp-document-language doc) "#parent-scopes"))))
           (lang--imported (lang-function 'imported-namespaces doc: doc))
           (rerun (call/cc (lambda (x) x)))
           (_1 (print-log "rerun?"))
           (scopes (seek-parent-scope! cursor parent-scopes))
           (_2 (ts--cursor-parent! cursor))
           (part (fold
                   (lambda (scope acc)
                     (print-log "scope?: " scope)
                     (cond
                       ((equal? scope 'local)
                        (let ((locals (query-db
                                        statement-query-local
                                        (lsp-document-language doc)
                                        uri
                                        string-hovered
                                        (ts--node-start-byte (ts--cursor-current-node cursor))
                                        (ts--node-end-byte   (ts--cursor-current-node cursor)))))
                          (print-log type: 4 "locals: " locals)
                          (if (null? locals) acc (append locals acc))))
                       ((equal? scope 'member)
                        (let ((members (query--db
                                         statement-query-member
                                         (lsp-document-language doc)
                                         uri
                                         string-hovered
                                         (ts--node-start-byte (ts--cursor-current-node cursor))
                                         (ts--node-end-byte   (ts--cursor-current-node cursor)))))
                          (print-log type: 4 "members: " members)
                          (if (null? members) acc (append members acc))))
                       (else
                         (letrec* ((uris (lang--imported uri doc))
                                   (_ (print-log "uris?: " uris))
                                   (make-variable-sql
                                     (lambda (str args)
                                       (cond
                                         ((null? args) str)
                                         ((null? (cdr args)) (string-append str "?);"))
                                         (else (make-variable-sql (string-append str "?, ") (cdr args))))))
                                   (imported (if (null? uris) '()
                                                 (apply
                                                   query--db
                                                   (eval `(list ,(make-variable-sql statement-query-imported uris)
                                                                ,(lsp-document-language doc)
                                                                ,string-hovered
                                                                ,@uris))))))
                           (print-log type: 4 "imported: " imported )
                           (if (null? imported)
                               (begin
                                 (set! scopes #f) ;; don't rerun
                                 (query--db statement-query-all string-hovered))
                               (append imported acc))))))
                   '() scopes)))
      (print-log type: 4 "what now? " part scopes)
      (if part
          (if (null? part)
              (if scopes
                  (begin
                    (ts--cursor-parent! cursor)
                    (rerun rerun))
                  '()) ;; we are at the top but without results.
              part)
          '()))))

(define (seek-parent-scope! cursor scopes)
  (letrec ((internal-seek
             (lambda ()
               (print-log type: 4 "seeking.. at: " (ts--node-type (ts--cursor-current-node cursor)))
               (let ((res (call/cc (lambda (x) x))))
                 (if (procedure? res)
                     (if (ts--cursor-parent! cursor)
                         (if (ts--node-named? (ts--cursor-current-node cursor))
                             (if (assoc (ts--node-type (ts--cursor-current-node cursor)) scopes)
                                 (cdr (assoc (ts--node-type (ts--cursor-current-node cursor)) scopes))
                                 (internal-seek))
                             (internal-seek))
                         '(all))
                     res)))))
    (internal-seek)))
