;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; Once opened, a document is aware of its language-parser.
(define (textDocument/didOpen params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didOpen")
      (let* ((doc  (cdr (assoc "textDocument" params)))
             (uri (cdr (assoc "uri" doc)))
             (text (cdr (assoc "text" doc)))
             (version (cdr (assoc "version" doc)))
             (lang (assoc (cdr (assoc "languageId" doc))
                          known-languages)))
        (print-log type: 5 "didOpen params: " uri version lang)

        (if lang
            (let* ((content (call-with-input-string text read-content))
                   (tree (ts-parse (car lang) content))
                   (lsp-doc (make-lsp-document version (car lang) tree)))
              (table-set! documents uri lsp-doc)
              (make-bindings uri lsp-doc content
                             include-locals?: #t
                             diagnostics?: (publish-diagnostics))
              (lsp-document-content-set! lsp-doc content)
              (sqlite3--bind! statement-query-file-indexed uri)
              (sqlite3--query statement-query-file-indexed
                              (lambda (x)
                                (sqlite3--bind! statement-save-opened
                                                (car x)
                                                (cdr (assoc
                                                       (car lang)
                                                       supported-languages)))
                                (sqlite3--step! statement-save-opened)
                                (print-log "marked file as opened: " uri)
                                (sqlite3--reset! statement-save-opened)))
              (sqlite3--reset! statement-query-file-indexed)
              'didOpen)
            'didOpenButIgnoredFile))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;               __ __        _  _  _  _    _     _ __ __          ;
;           /\ (_ (_ | ||\/||_ |_)|_)/ \|/|_|\ ||_(_ (_           ;
;          /--\__)__)|_||  ||_ |_)| \\_/|\|_| \||___)__)          ;
;                                                                 ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Language Clients are not entirely consistent:
; Emacs reports two byte long UTF-16 chars two times -
; Neovim reports two byte long UTF-16 chars only after + one char.
; vim-lsc will not handle two byte long UTF-16 chars at all.
; Due to this fact, the sync only works with UTF-8 encoding only.
;
; - - - - - - - - - - -|                   |- - - - - - - - - - - -
;                              _
;        _|o _ .__|_   _ _ .__|_o _   .__  _.   o.__  _|
;       (_||(/_| ||_  (_(_)| || |(_|  |(/_(_||_|||(/_(_|
;                                 _|        |
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;  FIXED  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (textDocument/didChange params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didChange")
      (letrec* ((full->incremental+-change
                  ;; does also set the byte positions
                  (lambda (lsp-doc chg)
                    (let* ((new-text (cdr (assoc "text" chg)))
                           (new-text-length (string-length new-text))
                           (content (lsp-document-content lsp-doc))
                           (old-row-count (vector-length content))
                           (change-text "")
                           (deleted-text #f))
                      (letrec ((start-row 0) (start-col 0) (start-byte 0)
                               (end-row 0)    (end-col 0)    (end-byte 0)
                               (change-start 0) (change-end 0)
                               (set-intermediate-values!
                                 (lambda (row col change-p byte-p)
                                   (set! start-row row)
                                   (set! start-col col)
                                   (set! start-byte byte-p)
                                   (set! change-start change-p)))
                               (seek-end!
                                 (lambda ()
                                   (letrec ((internal-seek!
                                              (lambda (str-p old-row-p old-col-p)
                                                (cond
                                                  ((<= str-p change-start)
                                                   ;; deletion, since we reached the start of the new text
                                                   (set! end-row old-row-p)
                                                   (set! end-col old-col-p)
                                                   (set! deleted-text (get-diff))
                                                   (set! end-byte (+ start-byte
                                                                     (byte-length deleted-text)
                                                                     0)))
                                                  ((and (= old-row-p start-row)
                                                        (= old-col-p start-col))
                                                   ;; we estimated an addition
                                                   (set! end-row old-row-p)
                                                   (set! end-col old-col-p)
                                                   (set! end-byte start-byte)
                                                   (set! change-text (substring new-text change-start str-p)))
                                                  ((< old-col-p 0)
                                                   (internal-seek! str-p (- old-row-p 1) (- (string-length (vector-ref content (- old-row-p 1))) 1)))
                                                  ((char=? (string-ref new-text str-p)
                                                           (string-ref (vector-ref content old-row-p) old-col-p))
                                                   (internal-seek! (- str-p 1) old-row-p (- old-col-p 1)))
                                                  (else ;; we found the end of the change
                                                    (if (= old-col-p (string-length (vector-ref content old-row-p)))
                                                        (begin
                                                          (set! end-row (+ old-row-p 1))
                                                          (set! end-col 0))
                                                        (begin
                                                          (set! end-row old-row-p)
                                                          (set! end-col (+ old-col-p 1))))
                                                    (set! change-text (substring new-text change-start (+ str-p 1)))
                                                    (set! deleted-text (get-diff))
                                                    (set! end-byte (+ start-byte (byte-length deleted-text))))))))
                                     (internal-seek!
                                       (- (string-length new-text) 1)
                                       (- old-row-count 1)
                                       (- (string-length (vector-ref content (- old-row-count 1))) 1)))))
                               (seek-start!
                                 (lambda (#!optional (str-p 0) (row 0) (col 0) (bytes 0))
                                   (cond
                                     ((= row old-row-count)
                                      ;; we have an addition at the end.
                                      (if (= col 0)
                                          (begin
                                            (set-intermediate-values! row 0 str-p bytes)
                                            (set! end-row row)
                                            (set! end-col 0)
                                            (set! change-text (substring new-text str-p new-text-length))
                                            (set! end-byte start-byte))
                                          (error "the column pointer does not point to the start of a line")))
                                     ((= str-p new-text-length)
                                      ;; we have a deletion at the end.
                                      (set-intermediate-values! row col str-p bytes)
                                      (set! end-row (- (vector-length content) 1))
                                      (set! end-col (string-length (vector-ref content end-row)))
                                      (set! change-text "")
                                      (set! deleted-text (get-diff))
                                      (set! end-byte (+ start-byte
                                                        (byte-length deleted-text))))
                                     ((= (string-length (vector-ref content row)) col)
                                      ;; next line
                                      (seek-start! str-p (+ row 1) 0 bytes))
                                     ((char=? (string-ref new-text str-p)
                                              (string-ref (vector-ref content row) col))
                                      ;; we can safely compare and move on.
                                      (seek-start! (+ str-p 1) row (+ col 1) (+ bytes
                                                                                (char->UTF-8-byte-size
                                                                                  (string-ref new-text str-p)))))
                                     (else ;; we have found a change
                                       (set-intermediate-values! row col str-p bytes)
                                       (seek-end!)))))
                               (get-diff
                                 (lambda ()
                                   (cond
                                     ((and (= start-row end-row)
                                           (not (= start-col end-col)))
                                      (substring (vector-ref content start-row)
                                                 start-col
                                                 end-col))
                                     ((>= start-row (vector-length content)) #f)
                                     (else
                                       (let ((first (vector-ref content start-row))
                                             (last (vector-ref content end-row))
                                             (mid-str (if (> end-row start-row)
                                                          (let lp ((row (+ start-row 1)) (str ""))
                                                            (if (< row end-row)
                                                                (lp (+ row 1)
                                                                    (string-append str (vector-ref content row)))
                                                                str))
                                                          "")))
                                         (string-append
                                           (substring first start-col (string-length first))
                                           mid-str
                                           (substring last 0 (string-length last)))))))))
                        (seek-start!)

                        `(("text" . ,change-text)
                          ("start-byte" . ,start-byte)
                          ("end-byte" . ,end-byte)
                          ("rangeLength" . ,(if deleted-text
                                                (byte-length deleted-text)
                                                0))
                          ("range"
                           ("start"
                            ("character" . ,start-col)
                            ("line" . ,start-row))
                           ("end"
                            ("character" . ,end-col)
                            ("line" . ,end-row))))))))

                (incr->incremental+-change
                  (lambda (lsp-doc chg)
                    (letrec ((find-start-byte
                               (lambda (content row col #!optional (max-row (vector-length content)))
                                 (letrec ((count-bytes
                                            (lambda (bytes row-p)
                                              (cond
                                                ((= row-p max-row) bytes)
                                                ((< row-p row) (count-bytes (+ bytes (byte-length (vector-ref content row-p))) (+ row-p 1)))
                                                ((= row-p row)
                                                 (let ((l (vector-ref content row-p)))
                                                   (if (= col 0)
                                                       bytes
                                                       (if (> col (string-length l))
                                                           (error "invalid position in didChange")
                                                           (+ bytes (byte-length (substring l 0 col)))))))
                                                (else (error "start byte would have been missed."))))))
                                   (count-bytes 0 0))))
                             (start-row (cdr (assoc "line"      (cdr (assoc "start" (cdr (assoc "range" chg)))))))
                             (start-col (cdr (assoc "character" (cdr (assoc "start" (cdr (assoc "range" chg)))))))
                             (end-row (cdr (assoc "line"      (cdr (assoc "end"   (cdr (assoc "range" chg)))))))
                             (end-col (cdr (assoc "character" (cdr (assoc "end"   (cdr (assoc "range" chg)))))))
                             (content (lsp-document-content lsp-doc)))
                      (let ((byte (find-start-byte content start-row start-col)))
                        (cons
                          `("start-byte" . ,byte)
                          (cons
                            `("end-byte" . ,(if (and (= start-row end-row)
                                                     (= start-col end-col))
                                                byte
                                                (find-start-byte content end-row end-col)))
                            chg))))))

                (insert-vector
                  (lambda (old start-row start-col text)
                    ;; TODO: set! new-end-line here.
                    (letrec ((old-length (vector-length old))
                             (has-newline? (lambda (line) (char=? #\newline (string-ref line (- (string-length line) 1)))))
                             (mutate-lines!
                               (lambda (chunks row col)
                                 (cond
                                   ((null? chunks) old)
                                   ((eof-object? (car chunks)) (error "error out"))
                                   ((>= row (+ (vector-length old) 1))
                                    ;; the command is way out of line, so move cursor
                                    mutate-lines! chunks (- row 1) col)
                                   ((= row (vector-length old))
                                    ;; addition at the bottom, okay
                                    (if (= col 0) ;; be strict
                                        (let ((v (make-vector (+ row 1))))
                                          (subvector-move! old 0 row v 0)
                                          (vector-set! v row (car chunks))
                                          (set! old v)
                                          (mutate-lines! (cdr chunks) (+ row 1) 0))
                                        (error "didChange pointing into a non-exisiting string in a non-exisiting row: Out-of-bounds!")))
                                   ((>= col (string-length (vector-ref old row)))
                                    ;; we have to put it onto a new line
                                    (if (has-newline? (car chunks))
                                        ;; own line
                                        (let ((v (make-vector (+ (vector-length old) 1))))
                                          (subvector-move! old 0 row v 0)
                                          (vector-set! v row (car chunks))
                                          (subvector-move! old row (vector-length old) v (+ row 1))
                                          (set! old v)
                                          (mutate-lines! (cdr chunks) (+ row 1) 0))
                                        ;; join with next
                                        (mutate-lines! chunks (+ row 1) 0)))
                                   ((< col (string-length (vector-ref old row)))
                                    (if (has-newline? (car chunks))
                                        ;; split
                                        (let ((v (make-vector (+ (vector-length old) 1))))
                                          (subvector-move! old 0 row v 0)
                                          (vector-set! v row
                                                       (string-append
                                                         (substring (vector-ref old row) 0 col)
                                                         (car chunks)))
                                          (vector-set! v (+ row 1)
                                                       (substring (vector-ref old row) col (string-length (vector-ref old row))))
                                          (subvector-move! old (+ row 1) (vector-length old) v (+ row 2))
                                          (set! old v)
                                          (mutate-lines! (cdr chunks) (+ row 1) 0))
                                        ;; join
                                        (begin
                                          (vector-set! old row
                                                       (string-append
                                                         (substring (vector-ref old row) 0 col)
                                                         (car chunks)
                                                         (substring (vector-ref old row) col (string-length (vector-ref old row)))))
                                          (mutate-lines! (cdr chunks) (+ row 1) 0))))
                                   (else
                                     (error (string-append
                                              "unexpected call with: "
                                              (object->string
                                                (list row " and " col " with: " chunks)))))))))

                      (mutate-lines!
                        (call-with-input-string text (lambda (p) (read-all p (lambda (p) (read-line p #\newline #t)))))
                        start-row start-col))))

                    (delete-vector
                      (lambda (old range-length start-row start-col end-row end-col)
                        (letrec ((walk-and-mutate!
                                   (lambda (row col rest)
                                     (if (> rest 0)
                                         (let ((line (vector-ref old row)))
                                           (cond
                                             ((>= (- (string-length line) col) rest)
                                              (vector-set! old row
                                                           (string-append
                                                             (substring (vector-ref old row) 0 col)
                                                             (substring (vector-ref old row) (+ col rest) (string-length (vector-ref old row)))))
                                              (walk-and-mutate! row 0 0)) ; ends
                                             (else ; join lines
                                               (vector-set! old row
                                                            (string-append
                                                              line
                                                              (vector-ref old (+ row 1))))
                                               (subvector-move! old (+ row 2) (vector-length old) old (+ row 1))
                                               (vector-shrink! old (- (vector-length old) 1))
                                               (walk-and-mutate! row col rest))))
                                         (if (and (= 1 (vector-length old))
                                                  (string=? "" (vector-ref old 0)))
                                             #() ;; edge case, if the entire buffer was erased.
                                             old)))))
                          (walk-and-mutate! start-row start-col range-length))))

                (apply-document-changes!
                  (lambda (chg)

                    (let* ((content (lsp-document-content lsp-doc))

                           (start-row (cdr (assoc "line"      (cdr (assoc "start" (cdr (assoc "range" chg)))))))
                           (start-col (cdr (assoc "character" (cdr (assoc "start" (cdr (assoc "range" chg)))))))
                           (start-p (make--ts-point start-row start-col))
                           (start-byte (cdr (assoc "start-byte" chg)))

                           (end-row (cdr (assoc "line"      (cdr (assoc "end"   (cdr (assoc "range" chg)))))))
                           (end-col (cdr (assoc "character" (cdr (assoc "end"   (cdr (assoc "range" chg)))))))
                           (end-p (make--ts-point end-row end-col))
                           (end-byte (cdr (assoc "end-byte" chg)))

                           (text (cdr (assoc "text" chg)))
                           (range-length (cdr (assoc "rangeLength" chg)))

                           (new-end-line start-row)
                           (new-end-col (fold (lambda (c acc)
                                                (if (char=? c #\newline)
                                                    (begin (set! new-end-line (+ new-end-line 1)) 0) ; reset col
                                                    (+ acc (/ (char->UTF-16-byte-size c) 2)))) ;; UTF-16 => UTF-8 test required.
                                              end-col (string->list text)))
                           (new-end-byte (+ start-byte (byte-length text)))
                           (new-end-p (make--ts-point new-end-line new-end-col)))

                      ;; apply deletions first
                      (set! content (delete-vector content range-length start-row start-col end-row end-col))

                      ;; if we have text, append it.
                      (if (string>? text "")
                          (set! content (insert-vector content start-row start-col text)))

                      ;; finally apply the change.
                      (lsp-document-content-set! lsp-doc content)

                      (print-log type: 5
                                 "resolved:"
                                 "start-byte"   start-byte
                                 "start-row"    start-row
                                 "start-col"    start-col
                                 "end-byte"     end-byte
                                 "end-row"      end-row
                                 "end-col"      end-col
                                 "new-end-byte" new-end-byte
                                 "text:"        text
                                 "len:"         (string-length text)
                                 "range-length" range-length)

                      (update-byte-positions!
                        uri
                        lsp-doc
                        start-byte end-byte
                        start-p end-p
                        new-end-byte
                        new-end-p))))

                (doc (cdr (assoc "textDocument" params)))
                (version (cdr (assoc "version" doc)))
                (uri (cdr (assoc "uri" doc)))
                (changes (cdr (assoc "contentChanges" params)))
                (lsp-doc (table-ref documents uri))
                (idx 0))

        (cond
          ((and lsp-doc (> version (lsp-document-version lsp-doc)) changes)
           (vector-for-each
             (lambda (ms-blsht)
               (set! idx (+ idx 1))
               (print-log type: 5
                          "content-change no: " idx
                          "incremental change: " (if (assoc "range" ms-blsht) #t #f))
               (apply-document-changes!
                 (if (assoc "range" ms-blsht)
                     (incr->incremental+-change lsp-doc ms-blsht)
                     (full->incremental+-change lsp-doc ms-blsht))))
             changes)
           (print-log type: 4 "number of changes for version" version "(provided/done(: " (vector-length changes) "/" idx)
           (lsp-document-version-set! lsp-doc version)
           'didChange)
          (else
            (print-log type: 1 "WHAT ABOUT THE VERSIONS? " version "/" (lsp-document-version lsp-doc))
            'did-NOT-Change)))))

(define (textDocument/didClose params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didClose")
      (let* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
             (doc (table-ref documents uri)))
        (ts--tree-free! (lsp-document-tree doc))
        (table-set! documents uri)
        (table-set! diagnostics uri)
        (print-log "freed document with uri/identifier: "
                   uri " / " (lsp-document-identifier doc)
                   (line-ending))
        (print-log "forcing garbage collection: "
                   (call-with-output-string
                     (lambda (p) (time (##gc) p))))
        'didClose)))

(define (textDocument/didSave params)
  (if (not params) (make-lsp-error invalid-params "no params provided to didSave")
      (let* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
             (text (assoc "text" params))
             (doc (table-ref documents uri)))

        (print-log type: 4
                   "did save document:" uri)

        (cond
          ((= (text-synchronization) 0)
           ;; keeping the ast in sync via full-text sync
           ;; this is faster than redoing all the work
           (textDocument/didChange
             `(("textDocument"
                ("uri" . ,uri)
                ("version" . ,(+ (lsp-document-version doc) 1)))
               ("contentChanges" . #((("text" . ,(if text
                                                     (cdr text)
                                                     (call-with-input-file
                                                       (substring uri 7 (string-length uri))
                                                       read-large-chunk))))))))

           (window/showMessage
             "imitated textDocument/didChange for low computation mode"
             (path-strip-directory
               (substring uri 7 (string-length uri)))))
          (else
            ;; when syncing documents:
            ;; restore the entire document in order
            ;; to recover from broken state.

            (lsp-document-content-set!
              doc
              (if text
                  (call-with-input-string (cdr (assoc "text" params)) read-content)
                  (call-with-input-file (substring uri 7 (string-length uri)) read-content)))

            (ts--tree-free! (lsp-document-tree doc))
            (lsp-document-tree-set!
              doc (ts-parse
                    (lsp-document-language doc)
                    (lsp-document-content doc)))

            ;; drop all bindings of uri:
            (sqlite3--bind! statement-renew-file uri)
            (sqlite3--step! statement-renew-file)
            (sqlite3--reset! statement-renew-file)

            (table-set! diagnostics uri)

            (make-bindings
              uri doc (lsp-document-content doc)
              include-locals?: #t
              diagnostics?: (publish-diagnostics))

            (window/showMessage
              "Renewed the document: "
              (path-strip-directory
                (substring uri 7 (string-length uri))))))

        (watch-zamba-configuration-file))))
