;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(define setter
  (lambda (x)
    (if (pair? x)
      (if (boolean? (cdr x))
        (cdr x)
        (if (null? x) #f #t))
      (if (boolean? x)
          x
          #f))))

(define text-synchronization (make-parameter 2))
(define did-save (make-parameter #f setter))
(define process-pid (make-parameter -1))

(define workspace-configuration (make-parameter #f setter))
(define workspace-folders-resolved (make-parameter #f setter))
(define workspace-folders-change (make-parameter #f setter))

(define hover (make-parameter #f setter))
(define definition (make-parameter #f setter))
(define declaration (make-parameter #f setter))
(define implementation (make-parameter #f setter))
(define declaration-link-support (make-parameter #f setter))
(define completion-resolve (make-parameter #f setter))
(define references (make-parameter #f setter))
(define work-done-progress (make-parameter #f setter))
(define publish-diagnostics (make-parameter #f setter))

;; planned when needed
;    ("signatureHelpProvider" . NULL)             ; arguments of a method-declaration
;    ("executeCommandProvider" . NULL)            ; maybe to connect the repl
;                                                 ; one could provide an environment => revisit after v.1.0.0

;; FIXME: link-support is not yet implemented, but hey. This is required, right?
(define (initialize params)
  (status-code server-initializing)
  (process-pid (cdr (assoc "processId" params)))
  (print-log type: 4
             "initializing zamba" params)
  (letrec ((client-cap-lvl-1 (lambda (root child) (assoc child (cdr (assoc root (client-capabilities))))))
           (client-cap-lvl-2 (lambda (root child child2) (assoc child2 (cdr (assoc child (cdr (assoc root (client-capabilities))))))))

           (client-capabilities (make-parameter (assoc "capabilities" params)))
           (result (call/cc (lambda (x) x))))

    (cond
      ((procedure? result) ;; starts here

       (print-log "client capabilitities: " (client-capabilities))

       (if (client-capabilities)
           (client-capabilities (cdr (client-capabilities)))
           (result (make-lsp-error server-error-start "client did not publish its capabilities.")))

       (when (assoc "workspace" (client-capabilities))
         (workspace-configuration (client-cap-lvl-1 "workspace" "configuration"))
         (workspace-folders-resolved (client-cap-lvl-1 "workspace" "workspaceFolders"))
         (workspace-folders-change (client-cap-lvl-1 "workspace" "workspaceFolders")))

       (when (assoc "textDocument" (client-capabilities))
         (hover (client-cap-lvl-1 "textDocument" "hover"))
         (definition (client-cap-lvl-1 "textDocument" "definition"))
         (declaration (client-cap-lvl-1 "textDocument" "declaration"))
         (implementation (client-cap-lvl-1 "textDocument" "implementation"))
         (if (declaration)
           (declaration-link-support (client-cap-lvl-2 "textDocument" "declaration" "linkSupport")))
         (references (client-cap-lvl-1 "textDocument" "references"))
         ;(completion-resolve (client-cap-lvl-1 "textDocument" "completion"))
         (if (client-cap-lvl-1 "textDocument" "synchronization")
           (did-save (client-cap-lvl-2 "textDocument" "synchronization" "didSave")))
         (if (diagnostics-enabled)
             (publish-diagnostics (client-cap-lvl-1 "textDocument" "publishDiagnostics"))))

       (when (assoc "window" (client-capabilities))
         (work-done-progress (client-cap-lvl-1 "window" "workDoneProgress")))

       (if (assoc "trace" params)
         (trace-lvl (cdr (assoc "trace" params))))

       (init-workspaces params)

       (if (and (= (text-synchronization) 0)
                (not (did-save)))
           (begin
             (thread-start!
               (make-thread (lambda () (thread-sleep! 2)
                              (exit server-shutdown-unexpected))
                            'suicide))
             (make-lsp-error invalid-params "Your client does not support the didSave-notification, so the low-computation-mode can not be used. Remove the command line argument."))
           (make-lsp-response
             `(("capabilities"
                ("workspace"
                 ("configuration" . ,(workspace-configuration))
                 ("workspaceFolders"
                  ("supported" . ,(workspace-folders-resolved))
                  ;; By replying with an string (id), one could unregister this capability.
                  ;; Currenlty, there is no use case detected for unregistering the capability.
                  ("changeNotifications" . ,(workspace-folders-change))))
                ("textDocumentSync"
                 ("save"
                  ("includeText" . ,(did-save)))
                 ("openClose" . #t)
                 ("change" . ,(text-synchronization)))
                ("diagnosticProvider" . ,(publish-diagnostics))
                ("hoverProvider" . ,(hover))
                ("declarationProvider" . ,(declaration))
                ("definitionProvider" . ,(definition))
                ("implementationProvider" . ,(implementation))
                ("referencesProvider" . ,(references))
                ("completionProvider"
                 ("resolveProvider" . ,(completion-resolve))))
               ("serverInfo"
                ("name" . "ZAMBA-LS")
                ("version" . ,version))))))
      ((lsp-error? result) result)
      (else (make-lsp-error server-error-start "unknown initialization path")))))

;;;;;;;;;;;;;;;;;;;;;;;
;;;; NOTIFICATIONS ;;;;
;;;;;;;;;;;;;;;;;;;;;;;

;message-types: 1 (error), 2 (warning), 3 (info), 4 (log)
(define (window/showMessage #!key (type 3) . msg)
  (thread-send output-thread
               `(("method" . "window/showMessage")
                 ("jsonrpc" . "2.0")
                 ("params" ("type" . ,type)
                  ("message" . ,(with-output-to-string
                     (lambda ()
                       (for-each display msg))))))))

;;;;;;;;;;;;;;;;;;;
;;;; Livecycle ;;;;
;;;;;;;;;;;;;;;;;;;

(define (initialized . params)
  (status-code server-initialized)

  (when (uses-repl?)
    (window/showMessage type: 2
      "started repl server at port: "
      (number->string
        (socket-info-port-number
          (tcp-server-socket-info gamrepl#tcp-server)))))

  (if (not (did-save))
    (window/showMessage type: 2
      "zamba wants the textDocument/didSave capability so it can drop document changes." (line-ending)
      "Client forces zamba to occupy more memory then necessary." (line-ending)
      "Request this feature at client maintainer."))

  (print-log type: 4 "finished")

  'initialized)

(define (shutdown . drop)
  (status-code server-shutdown-requested)
  (let ((workers (thread-group->thread-list
                   zamba-worker-thread-group)))
    (for-each
      (lambda (t)
        (case (thread-name t)
          (('repl-server) (gamrepl#server-stop))
          (('worker)      (thread-join! t 0))))
      workers))

  (make-lsp-response '()))

