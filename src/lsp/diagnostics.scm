;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Zamba only supports the push model of diagnostics.
;;
;; Mainly due to the fact that zamba does not want to get bothered by clients too much.
;; Diagnostics are currently only concerned with syntax.
;; Invalidation happens on text document changes.


;; lsp: DiagnosticSeverity
(define diagnostic-error   1)
(define diagnostic-missing 2)

(define (textDocument/diagnostic params)
  (if (not params) (make-lsp-error invalid-params "no params provided to diagnostic request")
      (let ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params))))))
        ;; TODO: recompute anyways.
        ;;       I do not have a language server client avialable who access
        ;;       this method; Therefore I could just test by the specification.
        ;;       Be aware: Microsoft shit. Who knows what happens.
        (make-lsp-response
          `(("kind" . "full")
            ("items" . ,(list->vector (table-ref diagnostics uri))))))))

(define (debounce-diagnostics uri)
  (if (publish-diagnostics)
      (let ((doc (table-ref documents uri)))
        (thread-send output-thread
                     `(("method" . "textDocument/publishDiagnostics")
                       ("jsonrpc" . "2.0")
                       ("params"
                        ("uri" . ,uri)
                        ("version" . ,(lsp-document-version doc))
                        ("diagnostics" . ,(list->vector (table-ref diagnostics uri)))))))))

(define (make-missing-diagnostics tree)
  (letrec* ((walk-up
              (lambda (acc)
                (if (ts--cursor-parent! cursor)
                    (if (ts--cursor-sibling-next! cursor)
                        (walk-tree acc)
                        (walk-up acc))
                    acc)))
            (walk-tree
              (lambda (acc)
                (let* ((node (ts--cursor-current-node cursor))
                       (new (if (ts--node-missing? (ts--cursor-current-node cursor))
                                (let ((msg (lowercase (ts--node-sexp! (ts--cursor-current-node cursor)))))
                                  (cons
                                    `(("range"
                                       ("start"
                                        ("character" . ,(ts--point-col (ts--node-start-point node)))
                                        ("line" . ,(ts--point-row (ts--node-start-point node))))
                                       ("end"
                                        ("character" . ,(ts--point-col (ts--node-end-point node)))
                                        ("line" . ,(ts--point-row (ts--node-end-point node)))))
                                      ("severity" . ,diagnostic-missing)
                                      ("code" . "Syntax Error")
                                      ("message" . ,(substring msg 1 (- (string-length msg) 1))))
                                    acc))
                                acc)))
                  (if (ts--cursor-child! cursor)
                      (walk-tree new)
                      (if (ts--cursor-sibling-next! cursor)
                          (walk-tree new)
                          (walk-up new))))))
            (cursor (ts--cursor-new tree))
            (res (walk-tree '())))
    (ts--cursor-free! cursor)
    res))

;; removes diagnostics from the affected lines.
;; shifts diagnotics in lines _after_ a change by the offset
(define (invalidate-diagnostics! uri start-p end-p new-end-p row-offset)
  (let ((start-row (ts--point-row start-p))
        (start-col (ts--point-col start-p))
        (end-row (ts--point-row end-p))
        (end-col (ts--point-col end-p))
        (new-end-row (ts--point-row new-end-p))
        (new-end-col (ts--point-col new-end-p)))
    (table-set!
      diagnostics uri
      (fold
        (lambda (it acc)
          (let ((diag-start-row (cdr (assoc "line"      (cdr (assoc "start" (cdr (assoc "range" it)))))))
                (diag-start-col (cdr (assoc "character" (cdr (assoc "start" (cdr (assoc "range" it)))))))
                (diag-end-row   (cdr (assoc "line"      (cdr (assoc "end"   (cdr (assoc "range" it)))))))
                (diag-end-col   (cdr (assoc "character" (cdr (assoc "end"   (cdr (assoc "range" it))))))))
            (print-log type: 2 "START-ROW" start-row "DIAG-START:" diag-start-row "END-ROW: " end-row "NEW END ROW: " new-end-row)
            (cond
              ((> diag-end-row end-row)
               (cons
                 `(("range"
                    ("start"
                     ("character" . ,diag-start-col)
                     ("line" . ,(+ diag-start-row row-offset)))
                    ("end"
                     ("character" . ,diag-end-col)
                     ("line" . ,(+ diag-end-row row-offset))))
                   ,(assoc "severity" it)
                   ,(assoc "code" it)
                   ,(assoc "message" it))
                 acc))
              ((< diag-end-row start-row)
               (cons it acc))
              ((and (= diag-start-row diag-end-row start-row end-row)
                    (< new-end-col end-col))
               (cons
                 `(("range"
                    ("start"
                     ("character" . ,(+ diag-start-col (+ new-end-col end-col)))
                     ("line" . ,diag-start-row))
                    ("end"
                     ("character" . ,(+ diag-end-col (+ new-end-col end-col)))
                     ("line" . ,diag-end-row)))
                   ,(assoc "severity" it)
                   ,(assoc "code" it)
                   ,(assoc "message" it))
                 acc))
              (else acc))))
        '()
        (table-ref diagnostics uri)))))
