;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; TODO: Respect dependencies.
(define (textDocument/implementation params)
  (if (not params) (make-lsp-error invalid-params "no params provided to implementation")
      (let* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
             (doc (table-ref documents uri))
             (pos (cdr (assoc "position" params)))
             (col (cdr (assoc "character" pos)))
             (row (cdr (assoc "line" pos))))

        (walk-tree (cursor start end)
          (ts--tree-root-node (lsp-document-tree doc))
          ;; if
          (and (= row (ts--point-row start))
               (>= col (ts--point-col start))
               (<  col (ts--point-col end))
               (= 0 (ts--node-child-count
                      (ts--cursor-current-node cursor))))
          (let ((results (query--db
                           statement-query-impl
                           (lsp-document-language doc)
                           (word-at (ts--cursor-current-node cursor) doc))))
            (if (null? results)
                (make-lsp-response '())
                (make-lsp-response
                  (list->vector
                    (map
                      (lambda (it)
                        (let ((node (ts--node-descendant-for-byte-range
                                      (ts--tree-root-node
                                        (lsp-document-tree (document-by-uri (car it))))
                                      (list-ref it 2)
                                      (list-ref it 3))))
                          `(("uri"   . ,(car it))
                            ("range" . (("start" . (("line"      . ,(ts--point-row (ts--node-start-point node)))
                                                    ("character" . ,(ts--point-col (ts--node-start-point node)))))
                                        ("end"   . (("line"      . ,(ts--point-row (ts--node-end-point node)))
                                                    ("character" . ,(ts--point-col (ts--node-end-point node))))))))))
                      results)))))))))
