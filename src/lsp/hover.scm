;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; The SPECIFICATION does not define what a hover-method shall display.
;; Ergo, each language can implement its own interpretation.
;;
;; What is relevant to a experienced/minimalist software engineer?
;; One can assume, that the cursor freezed at an object and the human is
;; thinking about his next code snipped about to write.
;;
;; => Do not show anything, if you are not sure it would be beneficial.
;;    Beeing silent is very much appreciated, but underestimated.
;;
;; If hovering a  variable,  provide doc-comments or technical details like memory occupation.
;; If hovering a  field,     provide doc-comments or technical details like memory occupation.
;; If hovering a  invocation provide doc-comments or show the file who declares it.
;; If hovering a  usage      provide doc-comments or show the file who declares it.
;; If hovering an enum       provide doc-comments or show siblings.
;;
;; DO NOT:
;; - Show usages or references, when hovering a definition.
;;   There is a dedicated method for this functionality.
;; - Show count of members, when hovering a parent declaration.
;;   This won't aid the workflow of the average user.
;;
;; My own README stated: Use Hover when you do not want to change your jumplist.
;; This was due to the fact that the previous version of zamba, which was
;; able to navigate within gambit-C source code, just dumped the entire function
;; in the hover window.
;; This was useful to keep y jumplist clean.
;; Nonetheless, the above text should generalize my intentions better.
;;
;; TODO: Respect dependencies and if uncertain, prompt the user beforehand!
(define (textDocument/hover params)
  (if (not params) (make-lsp-error invalid-params "no params provided to hover")
      (let* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
             (doc (table-ref documents uri))
             (pos (cdr (assoc "position" params)))
             (row (cdr (assoc "line" pos)))
             (col (cdr (assoc "character" pos)))
             (nodes (find-declaration-nodes uri doc row col)))
        (with-exception-catcher
          (lambda (ex)
            (log-exception! ex)
            (make-lsp-error internal-error
              (string-append
                "Hover failed at position: "
                (number->string row) "/" (number->string col) " in file: " uri)))
          (lambda ()
            (let ((ast (if (not (show-abstract-syntax-tree)) #f
                           (walk-tree (cursor start end)
                             (ts--tree-root-node (lsp-document-tree doc))
                             ;; if
                             (and (= row (ts--point-row start))
                                  (>= col (ts--point-col start))
                                  (<  col (ts--point-col end))
                                  (= 0 (ts--node-child-count
                                         (ts--cursor-current-node cursor))))
                             ;; then
                             (string-append
                               (line-ending) "current-node: " (ts--node-type (ts--cursor-current-node cursor))
                               (if (ts--cursor-current-node-field-name cursor)
                                 (string-append
                                   (line-ending) "       field: " (ts--cursor-current-node-field-name cursor))
                                                 "")
                               (line-ending) "  parent-ast:"
                               (line-ending) "```scheme"
                               (line-ending) (call-with-output-string
                                               (lambda (p)
                                                 (with-input-from-string
                                                   (ts--node-sexp!
                                                     (begin
                                                       (ts--cursor-parent! cursor)
                                                       (ts--cursor-current-node cursor)))
                                                   (lambda () (pp (read) p)))))
                               (line-ending) "```"
                               (line-ending)
                               (line-ending)))))
                  (hover-information
                    (if (null? nodes) ""
                        ((lang-function 'hover-information lang: (lsp-document-language doc))
                         nodes))))
              (make-lsp-response
                `(("contents"
                   ("kind"  . "markdown") ;; FIXME: this should be resolved and adapted.
                   ("value" . ,(string-append
                                 "# hover information:"
                                 (line-ending)
                                 (line-ending) (if (and (string? hover-information)
                                                        (> (string-length hover-information) 1))
                                                   hover-information
                                                   "no hover information found.")
                                 (line-ending)
                                 (line-ending) (if ast
                                                   (string-append
                                                     "# abstract-syntax-tree:"
                                                     (line-ending) ast)
                                                   ""))))))))))))
