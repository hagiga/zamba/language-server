;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; FIXME: unfortunately the returned error does not popup in neovim.
(define throw-on-empty-completion (make-parameter #t))

(define completion-item-kind-text 1)
(define completion-item-kind-method 2)
(define completion-item-kind-function 3)
(define completion-item-kind-constructor 4)
(define completion-item-kind-field 5)
(define completion-item-kind-variable 6)
(define completion-item-kind-class 7)
(define completion-item-kind-interface 8)
(define completion-item-kind-module 9)
(define completion-item-kind-property 10)
(define completion-item-kind-unit 11)
(define completion-item-kind-value 12)
(define completion-item-kind-enum 13)
(define completion-item-kind-keyword 14)
(define completion-item-kind-snippet 15)
(define completion-item-kind-color 16)
(define completion-item-kind-file 17)
(define completion-item-kind-reference 18)
(define completion-item-kind-folder 19)
(define completion-item-kind-enum-member 20)
(define completion-item-kind-constant 21)
(define completion-item-kind-struct 22)
(define completion-item-kind-event 23)
(define completion-item-kind-operator 24)
(define completion-item-kind-type-parameter 25)


;; Completions are aggregated
;;
;; TODO: Completions should respect the scope (of the cursor, that is this includes scoped imports)
;; but fall back to all available bindings (stdlib and not referenced files).
(define (textDocument/completion params)
  (if (not params) (make-lsp-error invalid-params "no params provided to completion")
      (let* ((uri (cdr (assoc "uri" (cdr (assoc "textDocument" params)))))
             (pos (cdr (assoc "position" params)))
             (col (cdr (assoc "character" pos)))
             (row (cdr (assoc "line" pos)))
             (doc (table-ref documents uri))
             (isIncomplete (box #t)))

        (walk-tree
          (cursor start end)
          (ts--tree-root-node (lsp-document-tree doc))
          (and (= row (ts--point-row start))
               (= (ts--node-child-count (ts--cursor-current-node cursor)) 0)
               (= col (ts--point-col end)))
          (let* ((node (ts--cursor-current-node cursor))
                 (_ (seek-parent-scope! cursor (eval (string->symbol (string-append "ts--" (lsp-document-language doc) "#parent-scopes")))))
                 (items (select-completion-candidate uri doc (word-at node doc) node))
                 (filtered (apply
                             (lang-function 'filter-completions lang: (lsp-document-language doc))
                             (list (list->vector items) isIncomplete cursor uri doc (word-at node doc) node))))
            (cond
              ((not filtered) (make-lsp-response '()))
              ((and (boolean? (unbox isIncomplete))
                    (vector? filtered))
               (make-lsp-response
                 `(("isIncomplete" . ,(unbox isIncomplete))
                   ("items" . ,filtered))))
              (else
                (make-lsp-error unexpected-result "language implementation return nonsense :("))))))))

(define (completionItem/resolve params)
  (print-log type: 2 "completionResolve not implemented, yet.")
  (if (not params) (make-lsp-error invalid-params "no params provided to completion")
      (make-lsp-response '())))

(define (select-completion-candidate uri doc search-string parent)
  (let ((res '())
        (byte-length (string-length search-string)))

    (sqlite3--bind! statement-query-completions
                    (lsp-document-language doc)
                    uri
                    (string-append search-string "%")
                    (ts--node-start-byte parent)
                    (ts--node-end-byte parent)
                    (lsp-document-language doc)
                    (string-append search-string "%"))
    (sqlite3--query statement-query-completions
                    (lambda (x)
                      (let ((it `(("label" . ,(list-ref x 1))
                                  ;; needs to be skipped, if empty
                                  ("insertText" . ,(if (list-ref x 2) (list-ref x 2) (list-ref x 1)))
                                  ("data" . ,(list-ref x 4)) ;; local?
                                  ("detail" . ,(string-append
                                                 "sig: "
                                                 (if (list-ref x 2)
                                                     (substring
                                                       (list-ref x 2)
                                                       (string-length (list-ref x 1))
                                                       (string-length (list-ref x 2))) "- ")
                                                 " type: " (if (list-ref x 5) (object->string (list-ref x 5)) "- ")
                                                 " @ " (path-strip-directory
                                                         (substring (list-ref x 0)
                                                                    7
                                                                    (string-length (list-ref x 0))))))
                                  ("kind" . ,(list-ref x 3)))))
                        (set! res (cons it res)))))
    (sqlite3--reset! statement-query-completions)
    res))
