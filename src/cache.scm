;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file holds the data structures necessary to operate zamba.
;; These definitions are internal.
;; Configuration is hold within config.scm.

;; mutated by the language implementations.
;; association list of ("language-identifier" . ts--language-instance)
(define known-languages '())

;; mutated by the language implementations.
;; association list, where the key is the extension and the cdr is the language-identifier
(define languages-by-suffix '())

(define (read-large-chunk p) (read-line p #f))
(define (read-content p)
  (letrec ((rd (lambda (lst line)
                 (if (eof-object? line)
                     lst
                     (rd (cons line lst)
                         (read-line p #\newline #t))))))
    (list->vector (reverse (rd '() (read-line p #\newline #t))))))

;; holds the opened documents
;; the keys are defined by the language.
;; refer to structs for guaranteed fields.
(define documents (make-table init: #f))

;; init @didOpen
;; invalidated @didChange
;; removed @didClose
(define diagnostics (make-table init: '()))

(define (document-by-uri uri)
  (let ((doc (table-ref documents uri)))
    (if doc doc
        (let ((lang (cdr (assoc (path-extension uri) languages-by-suffix))))
          (make-lsp-document
            'opened-implicitly
            lang
            (ts-parse
              lang
              (call-with-input-file (substring uri 7 (string-length uri)) read-content)))))))

;;;;;;;;;;;;
; DATABASE ;
;;;;;;;;;;;;

(print-log type: 4 "databases at: " (path-expand (cache-dir)))

;; database for a session, no need to persist
(define database (sqlite3--open
                   (string-append
                     "file:" (cache-dir) "session"
                     (if (not (debug)) "?cache=shared&mode=memory" ""))))

;; this database is used very much.
;; it can not live in memory, since parsing huge directories postpones start-up too much.
(sqlite3--exec database (string-append "ATTACH DATABASE 'file:" (cache-dir) "cache' as zamba;"))

;{{{

(define statement-query-file-indexed  #f)
(define statement-save-opened         #f)
(define statement-renew-file          #f)

(define statement-save-file           #f)
(define statement-query-rowid         #f)

(define statement-save-language       #f)
(define statement-save-binding        #f)
(define statement-save-local          #f)
(define statement-save-invocation     #f)
(define statement-save-implementation #f)

(define statement-query-language      #f)
(define statement-query-open-docs     #f)
(define statement-query-completions   #f)
(define statement-query-local         #f)
(define statement-query-member        #f)
(define statement-query-all           #f)
(define statement-query-imported      #f)
(define statement-query-references    #f)
(define statement-query-impl          #f)

(define statements-update-by-offset   #f)
(define statements-drop-between       #f)

(define statement-release-doc         #f)

(let ((sql-read
        (lambda (str)
          (call-with-input-file
            (string-append (path-expand (zamba-directory)) str)
            read-large-chunk))))

  ;; create table
  (for-each (lambda (sql)
              (let ((stmt (sqlite3--prepare database sql)))
                (when stmt
                  (sqlite3--step! stmt)
                  (sqlite3--finalize! stmt))))
            (call-with-input-string
              (sql-read "sql/create.sql")
              (lambda (p) (read-all p (lambda (p) (read-line p #\; #t))))))

  ;; prepare statements
  (set! statement-query-file-indexed  (sqlite3--prepare database (sql-read "sql/query-file.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-save-opened         (sqlite3--prepare database (sql-read "sql/save_opened.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-renew-file          (sqlite3--prepare database (sql-read "sql/drop-all-bindings-of-file.sql") SQLITE_PREPARE_PERSISTENT))

  (set! statement-save-language       (sqlite3--prepare database (sql-read "sql/save_lang.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-save-file           (sqlite3--prepare database (sql-read "sql/save_file.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-rowid         (sqlite3--prepare database (sql-read "sql/query-rowid.sql") SQLITE_PREPARE_PERSISTENT))

  (set! statement-save-binding        (sqlite3--prepare database (sql-read "sql/save_binding.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-save-local          (sqlite3--prepare database (sql-read "sql/save_local.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-save-invocation     (sqlite3--prepare database (sql-read "sql/save_invocation.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-save-implementation (sqlite3--prepare database (sql-read "sql/save_implementation.sql") SQLITE_PREPARE_PERSISTENT))

  (set! statement-query-language      (sqlite3--prepare database (sql-read "sql/query-language.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-open-docs     (sqlite3--prepare database "SELECT count(ref) FROM documents;" SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-completions   (sqlite3--prepare database (sql-read "sql/query-completions.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-local         (sqlite3--prepare database (sql-read "sql/query-local.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-member        (sqlite3--prepare database (sql-read "sql/query-member.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-all           (sqlite3--prepare database (sql-read "sql/query-global.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-imported      (sql-read "sql/query-imported.sql"))
  (set! statement-query-references    (sqlite3--prepare database (sql-read "sql/query-references.sql") SQLITE_PREPARE_PERSISTENT))
  (set! statement-query-impl          (sqlite3--prepare database (sql-read "sql/query-implementations.sql") SQLITE_PREPARE_PERSISTENT))

  (set! statements-update-by-offset   (map (lambda (sql) (sqlite3--prepare database sql SQLITE_PREPARE_PERSISTENT))
                                           (call-with-input-string
                                             (sql-read "sql/update.sql")
                                             ;; FIXME: VERY SENSIBLE: LINE ENDING STUFF
                                             (lambda (p)
                                               (fold (lambda (stmt acc)
                                                       (if (string=? "\n" stmt)
                                                           acc
                                                           (cons stmt acc)))
                                                     '()
                                                     (read-all p (lambda (p) (read-line p #\; #t))))))))
  (set! statements-drop-between       (map (lambda (sql) (sqlite3--prepare database sql SQLITE_PREPARE_PERSISTENT))
                                           (call-with-input-string
                                             (sql-read "sql/drop-between.sql")
                                             ;; FIXME: VERY SENSIBLE: LINE ENDING STUFF
                                             (lambda (p)
                                               (fold (lambda (stmt acc)
                                                       (if (string=? "\n" stmt)
                                                           acc
                                                           (cons stmt acc)))
                                                     '()
                                                     (read-all p (lambda (p) (read-line p #\; #t))))))))
  (set! statement-release-doc         (sqlite3--prepare database (sql-read "sql/delete-opened.sql") SQLITE_PREPARE_PERSISTENT)))


;;;;;;;;;;;;;;;;;;;
; keep db in sync ;
;;;;;;;;;;;;;;;;;;;

(define update-byte-positions!
  (lambda (uri lsp-doc
           start-byte end-byte
           start-p end-p
           new-end-byte
           new-end-p)
    (let* ((line-start-byte
             (- start-byte
                (byte-length
                  (substring (vector-ref (lsp-document-content lsp-doc)
                                         (ts--point-row start-p))
                             0 (ts--point-col start-p))))))

      (sqlite3--exec database "BEGIN")
      (drop-between-bytes uri start-byte end-byte)
      (update-by-byte-offset uri start-byte end-byte (- new-end-byte end-byte))
      (sqlite3--exec database "COMMIT")
      (print-log type: 5 "updated database after text-document update")

      (ts--tree-edit! (lsp-document-tree lsp-doc)
                      (make--ts-input-edit
                        start-byte end-byte
                        new-end-byte
                        start-p end-p
                        new-end-p))

      (let ((tree (ts-parse
                    (lsp-document-language lsp-doc)
                    (lsp-document-content lsp-doc)
                    (lsp-document-tree lsp-doc)))
            (old-tree (lsp-document-tree lsp-doc)))

        (lsp-document-tree-set! lsp-doc tree)

        (when (publish-diagnostics)
          (letrec* ((collect-start-and-end
                      (lambda (len p start end)
                        (cond
                          ((= len 1)       (cons (ts--range-start-point changed-ranges p)
                                                 (ts--range-end-point changed-ranges p)))
                          ((= p len)       (cons start end))
                          ((= p 0)         (collect-start-and-end len (+ p 1) (ts--range-start-point changed-ranges p) end))
                          ((= p (- len 1)) (collect-start-and-end len (+ p 1) start (ts--range-end-point changed-ranges p)))
                          (else            (collect-start-and-end len (+ p 1) start end)))))
                    (walk-changes
                      (lambda (len p)
                        (when (< p len)
                          (collect-diagnostics! uri (ts--node-descendant-for-byte-range
                                                      (ts--tree-root-node tree)
                                                      (ts--range-start-byte changed-ranges p)
                                                      (ts--range-end-byte changed-ranges p)))
                          (walk-changes len (+ p 1)))))
                    (len (make-unsigned-int32*))
                    (changed-ranges
                      (ts--tree-get-changed-ranges?
                        old-tree tree len)))
            ;; diagnostics only need to be updated iff a structural change on the AST occures.
            (if (> (ts--resolve-uint-32 len) 0)
              (let ((points (collect-start-and-end (ts--resolve-uint-32 len) 0 #f #f)))
                ;; tree sitter does not propose anything about the ordering.
                ;; it makes sense that they reuse their own implementation and
                ;; search depth first, which will result in the ordering provided
                ;; by the tests, where we reverse on each mutation.
                (if (and (car points) (cdr points))
                    (let ((row-offset (- (ts--point-row new-end-p) (ts--point-row end-p))))
                      (if (or (< (ts--point-row new-end-p) (ts--point-row end-p))
                              (and (= (ts--point-row new-end-p) (ts--point-row end-p))
                                   (< (ts--point-col new-end-p) (ts--point-col end-p))))
                          (invalidate-diagnostics! uri start-p end-p new-end-p row-offset)
                          (invalidate-diagnostics! uri (car points) (cdr points) new-end-p (- (ts--point-row new-end-p) (ts--point-row end-p))))
                      (walk-changes (ts--resolve-uint-32 len) 0))))
              (collect-diagnostics! uri (ts--node-descendant-for-byte-range
                                          (ts--tree-root-node
                                            (lsp-document-tree lsp-doc))
                                          line-start-byte
                                          end-byte)))
            (print-log type: 3
                       "updated diagnostics on #structural changes of the AST:"
                       (ts--resolve-uint-32 len))

            (unsigned-int32-free! len)
            (if changed-ranges
                (ts--range-free! changed-ranges))))

        (print-log type: 5 "updated tree after text-document update")
        (ts--tree-free! old-tree))

      (make-bindings
        uri lsp-doc (lsp-document-content lsp-doc)
        tree: (ts--node-descendant-for-byte-range
                (ts--tree-root-node
                  (lsp-document-tree lsp-doc))
                line-start-byte
                end-byte)
        include-locals?: #t)
      (print-log type: 5 "requeried bindings after text-document update"))))

;; called with every change within content
(define (drop-between-bytes uri start-byte end-byte)
  (print-log type: 5 "DELETE: " start-byte end-byte)
  (fold (lambda (stmt idx)
          (sqlite3--bind! stmt uri start-byte end-byte)
          (sqlite3--step! stmt)
          (case idx
            ((4) (print-log type: 5 "-" (sqlite3--changes database) "#locals"))
            ((3) (print-log type: 5 "-" (sqlite3--changes database) "#imports"))
            ((2) (print-log type: 5 "-" (sqlite3--changes database) "#members"))
            ((1) (print-log type: 5 "-" (sqlite3--changes database) "#invocation"))
            ((0) (print-log type: 5 "-" (sqlite3--changes database) "#implementation"))
            (else (error "we shouldnt be here")))
          (sqlite3--reset! stmt)
          (+ idx 1))
        0 statements-drop-between))

;; called just after the above method.
(define (update-by-byte-offset uri start-byte end-byte byte-offset)
  (print-log type: 5
             "UPDATE: " start-byte byte-offset end-byte)
  (fold (lambda (stmt idx)
          (case idx
            ((5) ;; update end_body, if affected
             (sqlite3--bind! stmt byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#member-ends"))
            ((4) ;; update end_body, if affected
             (sqlite3--bind! stmt byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#locals-ends"))
            ;; the following are shifting
            ((3)
             (sqlite3--bind! stmt byte-offset byte-offset byte-offset byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#member"))
            ((2)
             (sqlite3--bind! stmt byte-offset byte-offset byte-offset byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#locals"))
            ((1)
             (sqlite3--bind! stmt byte-offset byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#invocation"))
            ((0)
             (sqlite3--bind! stmt byte-offset byte-offset uri end-byte)
             (sqlite3--step! stmt)
             (print-log type: 5 "+" (sqlite3--changes database) "#implementation"))
            (else (error "UNEXPECTED STATMENT")))
          (sqlite3--reset! stmt)
          (+ idx 1))
        0 statements-update-by-offset))

(define (make-bindings uri doc content
                       #!key
                       (tree (ts--tree-root-node
                               (lsp-document-tree doc)))
                       (start-byte
                         (ts--node-start-byte tree))
                       (include-locals? #f)
                       (diagnostics? #f))
  (letrec ((file-diagnostics (table-ref diagnostics uri))
           (query (make-query
                    doc
                    (string-append
                      (call-with-input-file
                        (string-append
                          (zamba-directory)
                          "lib/lang/"
                          (lsp-document-language doc)
                          "/queries/members.ss")
                        read-large-chunk)
                      ;; refer to @collect-diagnostics!
                      (if (and (publish-diagnostics)
                            diagnostics?)  "\n(_ (ERROR) @error)\n" "")
                      (if include-locals?
                          (call-with-input-file
                            (string-append
                              (zamba-directory)
                              "lib/lang/"
                              (lsp-document-language doc)
                              "/queries/locals.ss") read-large-chunk)
                          ""))))
           ;; FIXME: some languages dont want the suffix removed!
           (ns (path-strip-extension
                 (path-strip-directory
                   uri)))
           (save-file
             (lambda (namespace)
               (let ((mod (modification-time? uri)))
                 (sqlite3--bind! statement-save-file uri namespace mod)
                 (sqlite3--step! statement-save-file)
                 (sqlite3--reset! statement-save-file))))
           (total-changes (sqlite3--total-changes database))
           (save-implementation
             (lambda (#!key name)
               (sqlite3--bind! statement-save-implementation
                               ns
                               (read-text name)
                               (ts--node-start-byte (ts--query-capture-node name))
                               (ts--node-end-byte (ts--query-capture-node name)))
               (sqlite3--step! statement-save-implementation)
               (sqlite3--reset! statement-save-implementation)))
           (save-invocation
             (lambda (#!key name)
               (sqlite3--bind! statement-save-invocation
                               ns
                               (read-text name)
                               (ts--node-start-byte (ts--query-capture-node name))
                               (ts--node-end-byte (ts--query-capture-node name)))
               (sqlite3--step! statement-save-invocation)
               (sqlite3--reset! statement-save-invocation)))
           (save
             (lambda (#!key name type insert-text body kind local)
               (let ((stmt (if (null? local) statement-save-binding statement-save-local)))
                 (sqlite3--bind! stmt
                                 ns
                                 (read-text name)
                                 type
                                 insert-text
                                 (ts--node-start-byte (ts--query-capture-node name))
                                 (ts--node-end-byte (ts--query-capture-node name))
                                 (ts--node-start-byte (ts--query-capture-node body))
                                 (ts--node-end-byte (ts--query-capture-node body))
                                 kind)
                 (sqlite3--step! stmt)
                 (sqlite3--reset! stmt))))
           (read-text (lambda (node)
                        (read-node content (ts--query-capture-node node))))

           (process-query-result
             (lambda x
               (let ((args
                       (reverse
                         (fold (lambda (v acc)
                                 (cons
                                   (cons (ts--query-capture-name-for-id
                                           query
                                           (ts--query-capture-index v))
                                         v)
                                   acc))
                               '() (caddr x)))))
                 (cond
                   ((assoc "skip" args) 'ignore-capture)

                   ((assoc "namespace" args)
                    (save-file (read-text (cdr (assoc "namespace" args)))))

                   (else
                     (let ((body #f)
                           (kind 1))

                       (if (or (assoc "reference.ctor" args)
                               (assoc "reference.call" args))
                           (save-invocation
                             name: (cdr (assoc "name" args))))

                       (if (or (assoc "reference.implementation" args)
                               (assoc "reference.class" args))
                           (save-implementation
                             name: (cdr (assoc "name" args))))

                       (cond
                         ((assoc "definition.class" args)
                          (set! body (cdr (assoc "definition.class" args)))
                          (set! kind completion-item-kind-class))

                         ((assoc "declaration.method" args)
                          (set! body (cdr (assoc "declaration.method" args)))
                          (set! kind completion-item-kind-method))

                         ((assoc "declaration.field" args)
                          (set! body (cdr (assoc "declaration.field" args)))
                          (set! kind completion-item-kind-field))

                         ((assoc "definition.method" args)
                          (set! body (cdr (assoc "definition.method" args)))
                          (set! kind completion-item-kind-method))

                         ((assoc "definition.field" args)
                          (set! body (cdr (assoc "definition.field" args)))
                          (set! kind completion-item-kind-field))

                         ((assoc "definition.interface" args)
                          (set! body (cdr (assoc "definition.interface" args)))
                          (set! kind completion-item-kind-interface))

                         ((assoc "definition.ctor" args)
                          (set! body (cdr (assoc "definition.ctor" args)))
                          (set! kind completion-item-kind-constructor))

                         ((assoc "definition.enum" args)
                          (set! body (cdr (assoc "definition.enum" args)))
                          (set! kind completion-item-kind-enum))

                         ((assoc "definition.annotation" args)
                          (set! body (cdr (assoc "definition.annotation" args)))
                          (set! kind completion-item-kind-property))

                         ((assoc "definition.type" args)
                          (set! body (cdr (assoc "definition.type" args)))
                          (set! kind completion-item-kind-property))

                         ((assoc "definition.variable" args)
                          (set! body (cdr (assoc "definition.variable" args)))
                          (set! kind completion-item-kind-variable))

                         ((assoc "definition.param" args)
                          (set! body (cdr (assoc "definition.param" args)))
                          (set! kind completion-item-kind-variable))

                         ((assoc "error" args)
                          (let* ((err-node (ts--query-capture-node (cdr (assoc "error" args))))
                                 (item `(("range"
                                          ("start"
                                           ("character" . ,(ts--point-col (ts--node-start-point err-node)))
                                           ("line" . ,(ts--point-row (ts--node-start-point err-node))))
                                          ("end"
                                           ("character" . ,(ts--point-col (ts--node-end-point err-node)))
                                           ("line" . ,(ts--point-row (ts--node-end-point err-node)))))
                                         ("severity" . ,diagnostic-error)
                                         ("code" . "Syntax Error")
                                         ("message" . "Abstract Syntax Tree parsing error"))))
                            (if (not (member item file-diagnostics))
                                (set! file-diagnostics (cons item file-diagnostics))))))

                       (when (and body kind)
                         (save name: (cdr (assoc "name" args))
                               type: (if (assoc "type" args)
                                         (read-text (cdr (assoc "type" args)))
                                         '())
                               insert-text: (if (assoc "signature" args)
                                                (string-append (read-text (cdr (assoc "name" args)))
                                                               (read-text (cdr (assoc "signature" args))))
                                                '())
                               local: (if (assoc "local" args) 1 '())
                               body: body
                               kind: kind)))))))))

    (when (not (string? uri))
      (print-log type: 2
                 "INVALID URI: " uri (table-search (lambda (k v) (eq? v doc)) documents) )
      (table-for-each (lambda (k v) (print-log type: 2 "available: " k v))
                      documents)
      (error (string-append "invalid uri provided to make-bindings: " (object->string uri))))

    (save-file ns)

    (sqlite3--bind! statement-query-rowid uri)
    (sqlite3--query statement-query-rowid (lambda (x) (set! ns (car x))))
    (sqlite3--reset! statement-query-rowid)

    (sqlite3--exec database "BEGIN")
    (with-exception-catcher
      (lambda (ex)
        (log-exception! ex)
        (window/showMessage
          type: 2
          "exception while fetching bindings of file: " uri
          (if (didSave)
              "in order to restore bindings, save the file."
              ""))
        (sqlite3--exec database "ROLLBACK"))
      (lambda ()
        (ts-query-execute! tree query process-query-result (cons start-byte (ts--node-end-byte tree)))
        (sqlite3--exec database "COMMIT")))
    (ts--query-free! query)

    (when (and diagnostics?
               (publish-diagnostics))
      ;; iterate provided tree (start-node) and collect ts--node-missing?
      (table-set! diagnostics uri
                  (append
                    (fold (lambda (it acc)
                            (if (not (member it file-diagnostics))
                                (cons it acc)
                                acc))
                    '() (make-missing-diagnostics tree))
                    file-diagnostics))
      (debounce-diagnostics uri))

    #!void))

(define (collect-diagnostics! uri node)
  (letrec ((file-diagnostics (table-ref diagnostics uri))
           (query (make-query
                    (table-ref documents uri)
                    ;; FIXME:
                    ;; Apparently, we are querying errors while changing
                    ;; the content so that we prefix diagnostics already
                    ;; invalidated before returning.
                    ;; By ignoring the root-node of the tree, we can avoid this.
                    ;;   This is obviously a fix, but I am not aware if this is
                    ;;   due to the amount of queries applies within a change
                    ;;   (speed) or an issue at invalidation.
                    ;;
                    ;; should be read and compiled as:
                    ;;   `\n(ERROR) @error`
                    ;; instead.
                    "\n(_ (ERROR) @error)\n"))

           (process-query-result
             (lambda x
               (let ((args
                       (reverse
                         (fold (lambda (v acc)
                                 (cons
                                   (cons (ts--query-capture-name-for-id
                                           query
                                           (ts--query-capture-index v))
                                         v)
                                   acc))
                               '() (caddr x)))))
                 (if (assoc "error" args)
                     (let* ((err-node (ts--query-capture-node (cdr (assoc "error" args))))
                            (item `(("range"
                                     ("start"
                                      ("character" . ,(ts--point-col (ts--node-start-point err-node)))
                                      ("line" . ,(ts--point-row (ts--node-start-point err-node))))
                                     ("end"
                                      ("character" . ,(ts--point-col (ts--node-end-point err-node)))
                                      ("line" . ,(ts--point-row (ts--node-end-point err-node)))))
                                    ("severity" . ,diagnostic-error)
                                    ("code" . "Syntax Error")
                                    ("message" . "Abstract Syntax Tree parsing error"))))
                       (if (not (member item file-diagnostics))
                           (set! file-diagnostics (cons item file-diagnostics)))))))))

    (with-exception-catcher
      (lambda (ex)
        (log-exception! ex)
        (window/showMessage type: 2
          "exception while querying for errors in: " uri))
      (lambda ()
        (ts-query-execute! node query process-query-result (cons (ts--node-start-byte node) (ts--node-end-byte node)))))
    (ts--query-free! query)

    ;; iterate provided tree (start-node) and collect ts--node-missing?
    (table-set! diagnostics uri
                (append
                  (fold (lambda (it acc)
                          (if (not (member it file-diagnostics))
                              (cons it acc)
                              acc))
                        '() (make-missing-diagnostics node))
                  file-diagnostics))
    (debounce-diagnostics uri)
    #!void))
;}}}

