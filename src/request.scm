;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This is an internal class.
;; When zamba receives a json, the message is passed to a worker-thread.
;; This thread will always parse the message.
;; It will also handle tasks, which do not require to visit the runtime data.
;; Otherwise he would be occupied. In this case, the work will get delegated.
;; This enables the worker to act fast and cancel delegated work, if asked.

(define worker-queue-lock (make-mutex 'work))
(define worker-queue '())

(define server-requests '())

(define (handle-request json)
  (letrec ((remove-itself-from-queue
             (lambda (id)
               (mutex-lock! worker-queue-lock)
               (set! worker-queue (remove worker-queue (assoc id worker-queue)))
               (mutex-unlock! worker-queue-lock)))

           (respond-to
             (lambda (id method . result)
               (let ((reply `(("jsonrpc" . "2.0")
                              ("id"      . ,id)
                              ("result"  . ,(car result)))))
               (print-log type: 4 "responding with result" reply)
               (thread-send output-thread reply))))

           (respond-with-error
             (lambda (id method err msg . data)
               (let ((reply `(("jsonrpc"  . "2.0")
                              ("id"       . ,id)
                              ("error"
                               ("code"    . ,err)
                               ("message" . ,msg )
                               ("data"    . ,data)))))
               (print-log type: 4 "responding with error" reply)
               (thread-send output-thread reply)
               (if (< (status-code) server-initialized)
                   (exit (status-code))))))

           (reply-with-error!
             (lambda (msg)
               (let ((err (make-lsp-error unkown-error-code
                                          msg)))
                 (respond-with-error
                   id method
                   (lsp-error-code err)
                   (lsp-error-msg err)))))

           (j-id         (assoc "id" json))
           (j-method (assoc "method" json))
           (j-params (assoc "params" json))
           (j-result (assoc "result" json)))

    (print-log "message-id: " j-id
               "method: " j-method
               "params: " j-params
               "result: " j-result)
    (cond
      ;; REQUEST (dispatched)
      ((and j-id j-method)
       (letrec ((id     (cdr j-id))
                (method (cdr j-method))
                (params (if j-params (cdr j-params) #f))
                (t (make-thread
                     (lambda ()
                       (with-exception-catcher
                         (lambda (ex)
                           (log-exception! ex " unimplemented method?: " method)
                           (window/showMessage
                             type: 2
                             "failed executing request number: "
                             (number->string id)
                             ". Called: " method)
                           (remove-itself-from-queue id)
                           (reply-with-error!
                             (string-append
                               "Unexpected exception logged. "
                               "affected: " method " (id: " (number->string id) ")."))
                           #f)
                         (lambda ()
                           (let ((result (apply (eval (string->symbol method)) (list params))))
                             (print-log type: 4 "handled: " method)
                             (remove-itself-from-queue id)
                             (cond ((lsp-error? result)    (respond-with-error id method
                                                                               (lsp-error-code result)
                                                                               (lsp-error-msg result)))
                                   ((lsp-response? result) (respond-to         id method
                                                                               (lsp-response-res result)))
                                   (else                   (raise (string-append "invalid type: " (object->string result)))))))))
                     'worker
                     zamba-worker-thread-group)))
         (case (status-code)
           ((server-shutdown-requested) (respond-with-error id method (status-code) "lsp should be quiet."))
           (else                        (mutex-lock! worker-queue-lock)
                                        (set! worker-queue (cons `(,id . ,t) worker-queue))
                                        (mutex-unlock! worker-queue-lock)
                                        (thread-specific-set! t id)
                                        (thread-start! t)))))
      ;; NOTIFICATION (synchroniously)
      ((and (not j-id) j-method)
       (let ((method (cdr j-method))
             (params (if j-params (cdr j-params) #f)))
         (thread-specific-set! (current-thread) method)
         (with-exception-catcher
           (lambda (ex)
             (log-exception! ex "when executing notification: " method)
             (reply-with-error!
               (string-append
                 "Unexpected exception logged. "
                 "notification not processed: " method))
             #f)
           (lambda ()
             (print-log type: 4 "got notified/statusCode: " method (status-code))
             (cond
               ((string=? method "exit")
                ;; clean-up
                (close-port output-file)
                (if (= (status-code) server-shutdown-requested)
                  (exit server-shutdown-requested)
                  (exit server-shutdown-unexpected)))
               ((< (status-code) server-initializing) 'skip-notification)
               (else (apply (eval (string->symbol method)) (list params))))))))
      ;; REPONSE / RESULT (synchroniously)
      (else
        (let* ((id (cdr j-id))
               (cb (assoc id server-requests))
               (method ((cdr cb))))
          (thread-specific-set! (current-thread) id)
          (cond
            (j-result
             (apply (eval method) (list (cdr j-result)))
             (print-log type: 4 "handled result for zamba-request:" id method))
            (else
              (set! server-requests (remove server-requests cb))
              (print-log type: 1 "client error:" id (assoc "error" json)))))))))

(define ($/cancelRequest params)
  (mutex-lock! worker-queue-lock)
  (let ((pair (assoc (cdar params) worker-queue)))
    (when pair
      (set! worker-queue (remove worker-queue pair))
      (with-exception-catcher
        (lambda (ex)
          (log-exception! ex "when canceling request: " params)
          #f)
        (lambda () (thread-interrupt! (cdr pair))))))
  (mutex-unlock! worker-queue-lock))

;; can be used to parameterize
(define progress-prefix (make-parameter "(zamba): "))

;; will be bound to a synchronized clojure
;; which immediately propagates the 'begin'-notification when called.
;; lambda-signature 11 lines below.
(define make-progress #f)

(let* ((mutex (make-mutex 'progress))
       (__token-id (make-parameter 0))
       (id (lambda ()
             (let ((this (__token-id)))
               (__token-id (+ 1 this))
               this))))
  (set! make-progress
    ;; signature of first invocation
    (lambda (title #!optional (cancellable #t))
      (letrec ((token (id))
               (last-percentage 0)
               (reporter
                 ;; signature of second invocation
                 (lambda (kind msg #!optional (percentage #f))
                   (mutex-lock! mutex 0)
                   (let ((val `(("kind" . ,(symbol->string kind))
                                ("cancellable" . ,cancellable)
                                ("message" . ,(string-append (progress-prefix) msg)))))
                     (when (and percentage (eq? kind 'report))
                       (if (> (floor (* percentage 100)) last-percentage)
                           (set! last-percentage (floor (* percentage 100))))
                       (set! val (cons `("percentage" . ,last-percentage) val)))
                     (if (and (symbol? kind)
                              (or (eq? kind 'report)
                                  (eq? kind 'end)))
                         (thread-send
                           output-thread
                           `(("method" . "$/progress")
                             ("jsonrpc" . "2.0")
                             ("params"
                              ("token" . ,token)
                              ("value" . ,val))))))
                   (mutex-unlock! mutex))))
        (thread-send output-thread
                     `(("method" . "$/progress")
                       ("jsonrpc" . "2.0")
                       ("params"
                        ("token" . ,token)
                        ("value" .
                         (("kind" . "begin")
                          ("title" . ,(string-append (progress-prefix) title))
                          ("cancellable" . ,cancellable))))))
        reporter))))

(define send-request
  (letrec ((im (make-mutex 'requests))
           (req-id (make-parameter 0))
           (request-id
             (lambda ()
               (let ((res (+ 1 (req-id))))
                 (req-id res)
                 res))))
    (lambda (method . params)
      (mutex-lock! im)
      (let* ((wrapped-callback
               (lambda (method id)
                 (cons id
                       (lambda ()
                         (let ((it (assoc id server-requests)))
                           (set! server-requests (remove server-requests it))
                           (string->symbol method))))))
             (cb (wrapped-callback method (request-id)))
             (json `(("jsonrpc" . "2.0")
                     ("id" . ,(car cb))
                     ("method" . ,method)
                     ,@params)))
        (set! server-requests (cons cb server-requests))
        (mutex-unlock! im)
        (thread-send output-thread json)))))
