;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (make-query doc patterns)
  ;; FIXME: Free memory, after erroring
  (let* ((error* (make-unsigned-int32*))
         (query-error* (make-ts--query-error))
         (query (ts--query-new (cdr (assoc (lsp-document-language doc) known-languages))
                               patterns
                               (string-length patterns)
                               error*
                               query-error*))
         (abort (when (not (= 0 (ts--query-error-get query-error*)))
                  (ts--query-free! query)
                  (error (string-append
                           "could not create the query. error offset: "
                           (number->string (ts--resolve-uint-32 error*))
                           " error code: "
                           (case (ts--query-error-get query-error*)
                             ((0) (error "should never arrive here"))
                             ((1) "query syntax invalid")
                             ((2) "invalid node type")
                             ((3) "invalid field")
                             ((4) "invalid capture")
                             ((5) "invalid query structure")
                             ((6) "invalid language")))))))
    (case query
      ((#f) (error "query was not created. Check arguments to ts-query-execute!"))
      (else query))))

;; thunk => (match-id pattern-index (list captures))
;; optional: a pair with the start and end bytes where the query should be executed on.
(define (ts-query-execute! root-node query thunk #!optional (outline #f))
  (letrec* ((cursor (ts--query-cursor-new)) ;; FIXME: reuse or share this struct
            (iter (lambda (captures i)
                   (if (< i 0)
                     captures
                     (iter (cons (ts--query-match-capture-node-at match i) captures)
                           (- i 1)))))

           (walk-match-cursor
             (lambda (#!optional (second-attempt #f))
               (if (ts--query-cursor-next-match! cursor match)
                 (begin
                   (with-exception-catcher
                     (lambda (ex)
                       (log-exception! ex "from within ts-query-execute!")
                       (if second-attempt (free)
                           (walk-match-cursor #t))
                       #f)
                     (lambda ()
                       (thunk
                         (ts--query-match-id match)
                         (ts--query-match-pattern-index match)
                         (iter '() (- (ts--query-match-capture-count match) 1)))))
                   (walk-match-cursor second-attempt)))))

           (free (lambda () ;; free error*, query-error* (ts--query-free! query)
                   (ts--query-cursor-free! cursor)))

           (match (make-ts--query-match)))

    ;(ts--query-cursor-match-limit-set! cursor (match-limit))

    (if outline
      (ts--query-cursor-limit-execution-range!
        cursor
        (car outline)
        (cdr outline)))
    (ts--query-cursor-execute! cursor query root-node)

    (if (ts--query-cursor-did-exceed-limit? cursor)
      (print-log type: 2
                 (string-append "EXCEEDED LIMIT when querying the tree: " (object->string root-node))))

    (walk-match-cursor)
    (free)))

(define (ts--query-collect! document query-string current-node)
  (let ((res '())
        (query (make-query document query-string)))
    (ts-query-execute! current-node query
                       (lambda x
                         (set! res
                           (cons (fold
                                   (lambda (cap acc)
                                     (cons (ts--query-capture-name-for-id query (ts--query-capture-index cap))
                                           (cons (ts--query-capture-node cap)
                                                 acc)))
                                   '() (caddr x))
                                 res))))
    res))

;; FIXME: Free memory.
(define (ts-query-capture-execute! doc query #!optional (outline #f))
  (letrec* ((walk-query-cursor
              (lambda (captures)
                (if (ts--query-cursor-next-capture! cursor match capture-index)
                  (walk-query-cursor (cons (ts--query-capture-node  (ts--query-match-capture match)) captures))
                  captures)))

            (cursor (ts--query-cursor-new))
            (root (ts--tree-root-node (lsp-document-tree doc)))
            (match (make-ts--query-match))
            (capture-index (make-unsigned-int32*)))

    (ts--query-cursor-match-limit-set! cursor (match-limit))
    (if outline
      (ts--query-limit-execution-range!
        cursor
        (car outline)
        (cdr outline)))
    (ts--query-cursor-execute! cursor query root)
    (let ((res (walk-query-cursor '())))
      (ts--query-cursor-free! cursor)
      res)))
