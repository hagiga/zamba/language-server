;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; COMMON:
(include "query.scm")

;;;;;;;;;;;;;;;;;;;;;
; before processing ;
;;;;;;;;;;;;;;;;;;;;;

;; this needs to be adapted to the method
;; ts_parser_parse, using the TSInput struct.
(define (ts-parse-with-string ts-lang doc-content)
  (let* ((ts-parser (ts--parser-new))
         (void (ts--parser-language-set! ts-parser (cdr (assoc ts-lang known-languages))))
         ;; exception handling? force a error for now
         (tree (ts--parser-parse-string-with-encoding ts-parser
                                                      #f
                                                      doc-content
                                                      (string-length doc-content)
                                                      0))) ; 0 -> utf-8, 1 -> utf-16
    (ts--parser-free! ts-parser)
    tree))

;(define parser-input (make--ts-input 0)) ;; 0 => UTF-8 encoding

(define (ts-parse language content #!optional (tree #f))
  (cond
    ((assoc language known-languages)
     (let* ((ts-parser (ts--parser-new))
            (_1 (ts--parser-language-set! ts-parser (cdr (assoc language known-languages))))
            (_2 (set! ts--parser-current-payload content))
            (tree (ts--parser-parse
                    ts-parser
                    tree)))
       (ts--parser-free! ts-parser)
       (set! ts--parser-current-payload #f)
       tree))
      (else
        #f)))
