# libraries

There are four+ external libraries:

 - sqlite3
 - tree-sitter + parsers (linked in as shared libraries)
 - gambit-ts-binding
 - gamrepl

All other directories within are part of the language server.
These bindings are build against the created parser, which is why they need to get build.
Refer to the Makefile in the project directory.
Complementary there are directories..:

 gambit-sqlite3-binding -> bindings for the ffi.
 gambit-ts-binding -> bindings for the ffi.
 gamrepl -> connect a repl to a running zamba-instance.
 lang/*/ -> bindings to tree-sitter-parsers and objects which are depended.
