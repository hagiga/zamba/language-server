# essential

- [X] implement low-computation mode
  - [ ] Note: When making bindings, one could drop a tag-file, if the mode is activated and text-sync is 0.
- [X] implement TCP connection
- [X] implement incremental text document syncronization
- [X] implement full-text document synchronization
      Almost done: get range-length and reuse the incremental functions to reuse the existing vector.
- [X] workspace/didChangeConfiguration -> harden initialize
- [ ] when opening a workspace/file from an directory where access privileges are restricted,
      an error prohibits further processing.
      This is postponed, until parsing with tree-sitter is done.

- [X] Verify if all end-positions can get dropped => No, they can't. They are required to satisfy responses. The body-functionality is not used - this could get dropped. The body can probably be resolved on demand by the AST.
- [X] it should be possible to reuse the overall structure of walk-tree with other languages.
- [X] differentiate between ranges: dependent vs relevant nodes (anonymous and named) => Done with a incremental search functionality.
- [X] make gambit-repl parameterize the port (start-up flag)
- [X] describe used threading strategy
- [ ] the build shall be able to be configured, so that the hosted REPL can be excluded. This shall be possible in order to create a more trustworthy binary for users.
- [ ] when calling multiple-times a method within time x, activate tracing?
- [ ] rework logging: when start-up-argument = higher level, then add to default type.
- [X] db only to hold possible import candidates. => Done with a dedicated table, who keep track of accessible documents. Also, locals are only queried, when the file is opened.
- [X] FIXME: NOT A URI in structs.scm => cyclic dependency was introduced: if not tree, then read file - if not tree, then there is no content either.. Fixed!
- [X] Changes on a name are not included => this needs to be fixed by going to the linestart (+ linend) for the sql-statements (byte position), Done.
- [ ] make zamba a singleton
- [ ] use a proper build system (CMake)
- [X] document: invocations, references have to be stored as global. => within bindings. Documentation merge follows.
- [X] FIXME: build. Again.
- [ ] dont hardcode directories.
- [X] wrap stderr, so that errors are shown in the ui
- [ ] make the configuration file portable (CR-LF support and such).
- [ ] enable working with zamba via docker, so portability issues are killed.
- [X] when a request arrives, set the thread-specific-bit! to the message ID, so that it can get referenced in the logs.
- [ ] make a document/faq to implement a new language, so that the particularities are documented
- [ ] check if `(make-bindings` can rely on the content of the lsp-doc, dropping its third argument.

# tree sitter

- [X] Create use-case-diagram
- [X] replace all occurences of walk-for-bindings
- [X] make all ts-object unique and free them

# java-binding:

- [X] Put uri in an own table.

## completions:
capturing name, parent and interfaces of a class could be combined, since all are tinting the class.
I should revisit this by the invocations.
Include every import for "autocompletion", told by linkdd @proglangdesign.

# sqlite
- [X] parse a long string appropriate; including newlines, splitting at semicolon, etc pp ... => Done, reworked json implementation.
